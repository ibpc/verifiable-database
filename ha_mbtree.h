/*
  Copyright (C) BABEC. All rights reserved.
  Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
  and Privacy Computing. All rights reserved.
  
  SPDX-License-Identifier: Apache-2.0
*/



/** @file ha_mbtree.h

    @brief
  The ha_mbtree engine is a stubbed storage engine for mbtree purposes only;
  it does nothing at this point. Its purpose is to provide a source
  code illustration of how to begin writing new storage engines; see also
  /storage/mbtree/ha_mbtree.cc.

    @note
  Please read ha_mbtree.cc before reading this file.
  Reminder: The mbtree storage engine implements all methods that are *required*
  to be implemented. For a full list of all methods that you can implement, see
  handler.h.

   @see
  /sql/handler.h and /storage/mbtree/ha_mbtree.cc
*/

#include "my_global.h"                   /* ulonglong */
#include "thr_lock.h"                    /* THR_LOCK, THR_LOCK_DATA */
#include "handler.h"                     /* handler */
#include "my_base.h"                     /* ha_rows */
#include "bplus_tree.h"
#include "predefined.h"
#include "sql_class.h"           // MYSQL_HANDLERTON_INTERFACE_VERSION

#define MAX_RECORD_SIZE 100


/** @brief
    Mbtree_share is a class shared between all processes
    this mbtree implements the minimum you may need
*/
class Mbtree_share : public Handler_share {
public:
    THR_LOCK lock;
    Mbtree_share();

    ~Mbtree_share() {
        thr_lock_delete(&lock);
    }
};

/** @brief
 * definition of storage engine
*/
class ha_mbtree : public handler {
    THR_LOCK_DATA lock;      ///< MySQL lock
    Mbtree_share *share;    ///< Shared lock info
    Mbtree_share *get_share(); ///< Get the share
    char* table_name;

    uint buf_size = 0;  ///< buffer size, used to copy record
    uint8_t *record_buffer = NULL;  ///< buffer , used to copy record

    BplusTree *bpt; ///< MBT pointer

    /**
     * @brief pack line data into buffer
     * mysql row->mbtree row，result is in record_buffer
     * @param buf buffer pointer, to store data after package
     * @return data length after package，if fails return-1。
     */
    int pack_row(uint8_t *buf);
    /**
     * @brief copy part of record key to target
     *
     * @param to_key pointer to target key，to store key copied
     * @param from_record pointer to source record，including key part to copy
     * @param key_info key information，including length and key part
     * @return key part length
     */
    int key_copy(uchar *to_key, const uchar *from_record, KEY *key_info);
    /**
     * @brief get key from record
     * @param key pointer to key
     * @param record raw record pointer
     * @return key length
     */
    int pack_key(uchar *key, const uchar *record);
    /**
     * @brief copy Record raw_record to buf
     * @param buf buffer
     * @param raw_record raw record pointer
     * @param typ record type
     */
    void copy_from_raw_record(uchar* buf, uchar* raw_record, int typ);
    /**
     * @brief copy record in buf to Record raw_record
     * @param buf buffer
     * @param typ record type
     * @return raw_record pointer
     */
    uchar* encode_record_buffer(uchar* buf, int typ);

    bool has_varchar(int typ);

  

public:
    ha_mbtree(handlerton *hton, TABLE_SHARE *table_arg);

    ~ha_mbtree() {
    }

    File data_file;

    /** @brief
     * storage engine name to display
   */
    const char *table_type() const { return "MBTREE"; }

    /** @brief
      index name to display
     */
    const char *index_type(uint inx) { return "MBTREE"; }

    /** @brief
      file extension，.mb
     */
    const char **bas_ext() const;

    /** @brief
      This is a list of flags that indicate what functionality the storage engine
      implements. The current table flags are documented in handler.h
    */
    ulonglong table_flags() const {
        /*
          We are saying that this engine is just statement capable to have
          an engine that can only handle statement-based logging. This is
          used in testing.
        */
        return HA_BINLOG_STMT_CAPABLE;
    }

    /** @brief
      This is a bitmap of flags that indicates how the storage engine
      implements indexes. The current index flags are documented in
      handler.h. If you do not implement indexes, just return zero here.

        @details
      part is the key part to check. First key part is 0.
      If all_parts is set, MySQL wants to know the flags for the combined
      index, up to and including 'part'.
    */
    ulong index_flags(uint inx, uint part, bool all_parts) const {
        return 0;
    }

    /** @brief
      unireg.cc will call max_supported_record_length(), max_supported_keys(),
      max_supported_key_parts(), uint max_supported_key_length()
      to make sure that the storage engine can handle the data it is about to
      send. Return *real* limits of your storage engine here; MySQL will do
      min(your_limits, MySQL_limits) automatically.
     */
    uint max_supported_record_length() const { return MAX_RECORD_SIZE ; }

    /** @brief
      unireg.cc will call this to make sure that the storage engine can handle
      the data it is about to send. Return *real* limits of your storage engine
      here; MySQL will do min(your_limits, MySQL_limits) automatically.

        @details
      There is no need to implement ..._key_... methods if your engine doesn't
      support indexes.
     */
    uint max_supported_keys() const { return 1; }

    /** @brief
      unireg.cc will call this to make sure that the storage engine can handle
      the data it is about to send. Return *real* limits of your storage engine
      here; MySQL will do min(your_limits, MySQL_limits) automatically.

        @details
      There is no need to implement ..._key_... methods if your engine doesn't
      support indexes.
     */
    uint max_supported_key_parts() const { return 1; }

    /** @brief
      unireg.cc will call this to make sure that the storage engine can handle
      the data it is about to send. Return *real* limits of your storage engine
      here; MySQL will do min(your_limits, MySQL_limits) automatically.

        @details
      There is no need to implement ..._key_... methods if your engine doesn't
      support indexes.
     */
    uint max_supported_key_length() const { return sizeof(int); }

    /** @brief
      Called in test_quick_select to determine if indexes should be used.
    */
    virtual double scan_time() { return (double) (stats.records + stats.deleted) / 20.0 + 10; }

    /** @brief
      This method will never be called if you do not implement indexes.
    */
    virtual double read_time(uint, uint, ha_rows rows) { return (double) rows / 20.0 + 1; }

    /*
      Everything below are methods that we implement in ha_mbtree.cc.

      Most of these methods are not obligatory, skip them and
      MySQL will treat them as not implemented
    */
    /** @brief
      We implement this in ha_mbtree.cc; it's a required method.
    */
    int open(const char *name, int mode, uint test_if_locked);    // required

    /** @brief
      We implement this in ha_mbtree.cc; it's a required method.
    */
    int close(void);                                              // required

    /** @brief
      We implement this in ha_mbtree.cc. It's not an obligatory method;
      skip it and and MySQL will treat it as not implemented.
    */
    int write_row(uchar *buf);

    /** @brief
      We implement this in ha_mbtree.cc. It's not an obligatory method;
      skip it and and MySQL will treat it as not implemented.
    */
    int update_row(const uchar *old_data, uchar *new_data);

    /** @brief
      We implement this in ha_mbtree.cc. It's not an obligatory method;
      skip it and and MySQL will treat it as not implemented.
    */
    int delete_row(const uchar *buf);

    /** @brief
      We implement this in ha_mbtree.cc. It's not an obligatory method;
      skip it and and MySQL will treat it as not implemented.
    */
//    int index_read_map(uchar *buf, const uchar *key,
//                       key_part_map keypart_map, enum ha_rkey_function find_flag);

//    int index_read(uchar * buf, const uchar * key, uint key_len, ha_rkey_function find_flag);

    /** @brief
      We implement this in ha_mbtree.cc. It's not an obligatory method;
      skip it and and MySQL will treat it as not implemented.
    */
    int index_next(uchar *buf);

    /** @brief
      We implement this in ha_mbtree.cc. It's not an obligatory method;
      skip it and and MySQL will treat it as not implemented.
    */
    int index_prev(uchar *buf);

    /** @brief
      We implement this in ha_mbtree.cc. It's not an obligatory method;
      skip it and and MySQL will treat it as not implemented.
    */
    int index_first(uchar *buf);

    /** @brief
      We implement this in ha_mbtree.cc. It's not an obligatory method;
      skip it and and MySQL will treat it as not implemented.
    */
    int index_last(uchar *buf);

    /** @brief
      Unlike index_init(), rnd_init() can be called two consecutive times
      without rnd_end() in between (it only makes sense if scan=1). In this
      case, the second call should prepare for the new table scan (e.g if
      rnd_init() allocates the cursor, the second call should position the
      cursor to the start of the table; no need to deallocate and allocate
      it again. This is a required method.
    */
    int rnd_init(bool scan);                                      //required
    int rnd_end();

    int rnd_next(uchar *buf);                                     ///< required
    int rnd_pos(uchar *buf, uchar *pos);                          ///< required
    void position(const uchar *record);                           ///< required
    int info(uint);                                               ///< required
    int extra(enum ha_extra_function operation);

    int external_lock(THD *thd, int lock_type);                   ///< required
    int delete_all_rows(void);

    int truncate();

    ha_rows records_in_range(uint inx, key_range *min_key,
                             key_range *max_key);

    int delete_table(const char *from);

    int rename_table(const char *from, const char *to);

    int create(const char *name, TABLE *form,
               HA_CREATE_INFO *create_info);                      ///< required

    THR_LOCK_DATA **store_lock(THD *thd, THR_LOCK_DATA **to,
                               enum thr_lock_type lock_type);     ///< required
};
