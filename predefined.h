/*
  Copyright (C) BABEC. All rights reserved.
  Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
  and Privacy Computing. All rights reserved.
  
  SPDX-License-Identifier: Apache-2.0
*/

/*
 * @file predefined.h predifined.cc
 * @brief define all auxiliary functions of this project
 *
 * Maintainer: haojk
 * Email: jk.hao@pku.edu.cn
 *
 * Dependencies:
 *  - openssl
 */

#ifndef PREDEFINED_H
#define PREDEFINED_H

#include <stdio.h>
#include <string.h>
#include <string>
#include <vector>
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <unistd.h>
#include <sys/stat.h>
#include <map>

#define INVALID_TYPE 0
#define INT_TYPE 1
#define INT_SIZE 4
#define STR20_TYPE 2
#define STR20_SIZE 20

#define LEAF_TYPE 1
#define INDEX_TYPE 2
typedef unsigned char uchar;

class MyException {
public:
    virtual const char* what() const throw() =0;
};


class PredefinedException: public MyException {
public:
    std::string err_msg;
    PredefinedException(std::string err_msg) {
        this->err_msg = "[predefined] error: " + err_msg;
    }
    virtual const char* what() const throw() {
        return this->err_msg.c_str();
    }
};

std::string sha256(const std::string str);

std::string int_to_string(int x);

int uchars_to_int(uchar* ptr);

void int_to_uchars(int x, uchar* ptr);

std::string uchars_to_hex(uchar* ptr, int len);

uchar* hex_to_uchars(std::string str);

std::vector<std::string> string_split(const std::string& str, char delimiter);

bool is_file_exist(std::string name);

bool is_dir_exist(std::string path);

///recursively create directory，if not permission problem，won't fail（can't recursively create directory temporarily）
bool create_dir(std::string name);

///recursively create file，if not permission problem，won't fail（can't recursively create file temporarily）
bool create_file(std::string name);

bool copy_file(std::string src, std::string dst);

bool is_files_equal(std::string filePath1, std::string filePath2);

int get_type_size(int typ);

int get_types_size(int typs);

int get_var_length(uchar* buf, int fields_typ);

std::map<std::string, std::string> parse_toml_config(const std::string& filePath);


/**
 * @brief resolve path，return composed vector by every directory in the path 
 * @param path definite path or relative path
 * @return paths composed vector
 */
std::vector<std::string> parse_path(std::string path);

struct Table {
    std::string name;
    int version;
    std::string db_file_dir_path;
    std::string mkp_files_dir_path;
};

void save_tables_to_txt(const std::string filename, std::vector<Table> open_tables);

std::vector<Table> load_tables_from_txt(const std::string filename);

/**
 * @brief Manage imported and opened tables
 */
class TableManager {
public:
    std::string config_file_path; ///< file path configuration
    std::vector<Table> imported_tables; ///< imported tables
    std::map<int, void*> opened_trees; ///< oepned tables，key is the location of table in imported_tables，value is the pointer to B plus tree

    TableManager(std::string config_file_path) {
        this->config_file_path = config_file_path;
        if(!is_file_exist(config_file_path)) {
            create_file(config_file_path);
        }
        this->imported_tables = load_tables_from_txt(config_file_path);
    }

    int is_imported(std::string table_name, int version) {
        int i;
        for (i = 0; i < imported_tables.size(); i++) {
            Table table = imported_tables[i];
            if (table.name == table_name &&
                table.version == version) {
                return i;
            }
        }
        return -1;
    }

    bool is_opened(int table_index) {
        if (opened_trees.count(table_index) == 0) {
            return false;
        }
        return true;
    }

    bool import_table(Table table) {
        if(is_imported(table.name, table.version) != -1) {
            return false;
        }
        imported_tables.push_back(table);
        return true;
    }

    bool open_table(std::string table_name, int version, void* tree) {
        int table_index = is_imported(table_name, version);
        if (table_index == -1) {
            return false;
        }
        if (is_opened(table_index)) {
            return false;
        }
        opened_trees[table_index] = tree;
        return true;
    }

    void save_config() {
        save_tables_to_txt(config_file_path, imported_tables);
    }

    void* get_opened_tree(int table_index) {
        if (!is_opened(table_index)) {
            return NULL;
        }
        return opened_trees[table_index];
    }
};




class Hash {
public:
    uchar *hash;
    bool in_node; ///< whether is in the node, to determint whether to free memory
    /**
     * @brief construction funciton
     *
     * @param in_node whether is in the node, to determint whether to free memory
     */
    Hash(bool in_node) {
        this->in_node = in_node;
        if (!this->in_node) {
            //if not in the node, should allocate memory as a temporary variable
            hash = new uchar[SHA256_DIGEST_LENGTH];
        } else {
            //if in the node, needn't allocate memory , is a pointer to part of the node
            hash = NULL;
        }
    }

    /**
     * @brief deconstruction function
     */
    ~Hash() {
        if (!in_node) {
            delete[] hash;
        }
    }

    static Hash& sha256(const std::string input) {
        unsigned char hash[EVP_MAX_MD_SIZE];
        EVP_MD_CTX *mdctx;
        const EVP_MD *md;
        unsigned int md_len;

        md = EVP_sha256();
        mdctx = EVP_MD_CTX_new();
        EVP_DigestInit_ex(mdctx, md, NULL);
        EVP_DigestUpdate(mdctx, input.c_str(), input.size());
        EVP_DigestFinal_ex(mdctx, hash, &md_len);
        EVP_MD_CTX_free(mdctx);

        Hash *output = new Hash(false);
        memcpy(output->hash, hash, SHA256_DIGEST_LENGTH);
        return *output;
    }

    std::string to_hex() {
        return uchars_to_hex(hash, SHA256_DIGEST_LENGTH);
    }

    static Hash& from_hex(std::string hex_str) {
        Hash *hash = new Hash(false);
        delete hash->hash;
        hash->hash = hex_to_uchars(hex_str);
        return *hash;
    }

    static Hash& generate_empty_hash() {
        Hash *hash = new Hash(false);
        memset(hash->hash, 0, SHA256_DIGEST_LENGTH);
        return *hash;
    }
    bool operator == (const Hash& other) const {
        if (memcmp(hash, other.hash, SHA256_DIGEST_LENGTH) == 0) {
            return true;
        }
        return false;
    }
    bool operator != (const Hash& other) const {
        if (memcmp(hash, other.hash, SHA256_DIGEST_LENGTH) != 0) {
            return true;
        }
        return false;
    }
    Hash& operator= (const Hash& other) {
        if (this != &other) {
            memcmp(hash, other.hash, SHA256_DIGEST_LENGTH);
        }
        return *this;
    }
};

class MerkleProof {
public:
    std::vector<std::vector<std::string> > hashlists;
    std::vector<std::string> hashes;
    std::string key;
    std::string record;

    static MerkleProof* from_string(std::string raw_mkp) {
        MerkleProof *mkp = new MerkleProof();
        std::vector<std::string> lines = string_split(raw_mkp, '\n');

        if (lines.size() > 0)
            mkp->key = lines[0];
        else
            return NULL;
        if(lines.size() > 1)
            mkp->record = lines[1];
        else
            return NULL;
        if (lines.size() % 2 == 0) {
            return NULL;
        }
        for(int i=2; i<lines.size(); i++) {
            if (i % 2 == 0) {
                mkp->hashes.push_back(lines[i]);
            } else {
                std::vector<std::string> hashlist = string_split(lines[i], ' ');
                mkp->hashlists.push_back(hashlist);
            }
        }
        return mkp;
    }

    std::string to_string() {
        std::string mkp;
        mkp += key + "\n" + record + "\n";
        for (int i=0; i<hashes.size()-1; i++) {
            mkp += hashes[i] + "\n";
            for (int j=0; j<hashlists[i].size(); j++) {
                mkp += hashlists[i][j] + " ";
            }
            mkp[mkp.size()-1] = '\n';
        }
        mkp += hashes[hashes.size()-1] + "\n";
        return mkp;
    }

    /**
     * @brief chech merkle proof
     *  key                |7
     *  value              |000000070b0c0d0e
     *  hash(value)        |f5d79c9a37b97ef3f2c7d7a2fdc2c182eb1f37ec1711db4224d7b4dd99808dc0
     *  leaf node          |c4da4c8d5260cb24554e27b3aa1a64dc3790dc01c5d5a5bf5c97878d63dbf247 465cf30f49e2e3cb145666e61173f1e82e28c0edd7a38b9320883879c88881d1 f5d79c9a37b97ef3f2c7d7a2fdc2c182eb1f37ec1711db4224d7b4dd99808dc0 36618a44666c97f23c43e6770a9cab33034c4cba1a6a78c1c4f969178b90d91a 902ddfcdcb47025aabb9adc6b450ee08d568493121ac54ad27b36360bab361e0 d632c6f525e37d3e44c03ba14ca7a48d15aa2c1ebd38576f58c9e857ddec95fc
     *  hash(leaf node)    |4ce5c5a16e6c4231fa65eb17be69ff740c783c5ac059357a60b58e2b1e322416
     *  internal node      |4ce5c5a16e6c4231fa65eb17be69ff740c783c5ac059357a60b58e2b1e322416 b55767a797c11dbb3df1946d4c1fde15f0627a78ef4e5387bc6a856075d18260 5c0a98760f1f42f7ddcb4d12b1572d0413abb5c0b3d1f916ea82d65d61e7c21a
     *  hash(internal node)|abe6bbc72129d575633c99ba5393b065c6f553e718664ad69870037659cded3b
     * @return success or fail
     */
    bool verify() {
        for (int i=0; i<hashes.size(); i++) {
            //check whether the hash is correct
            std::string hash_str;
            if (i == 0) {
                Hash& hash = Hash::sha256(record);
                hash_str = hash.to_hex();
                delete &hash;
            } else {
                std::string raw_hashlist = hashlists[i-1][0];
                for (int j=1; j<hashlists[i-1].size(); j++) {
                    raw_hashlist += " " + hashlists[i-1][j];
                }
                Hash& hash = Hash::sha256(raw_hashlist);
                hash_str = hash.to_hex();
                delete &hash;
            }
            if (hashes[i] != hash_str)
                return false;
            if (i == hashes.size()-1)
                break;
            //check whether hash is in the list
            bool found = false;
            for (int j=0; j < hashlists[i].size(); j++) {
                if(hashlists[i][j] == hashes[i]) {
                    found = true;
                    break;
                }
            }
            if (!found)
                return false;
        }
        return true;
    }
};

#endif /* end of PREDEFINED_H */
