/*
  Copyright (C) BABEC. All rights reserved.
  Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
  and Privacy Computing. All rights reserved.
  
  SPDX-License-Identifier: Apache-2.0
*/
#include <fstream>
#include <iostream>
#include "predefined.h"

std::string sha256(const std::string str) {
    unsigned char hash[EVP_MAX_MD_SIZE];
    EVP_MD_CTX *mdctx;
    const EVP_MD *md;
    unsigned int md_len;

    md = EVP_sha256();
    mdctx = EVP_MD_CTX_new();
    EVP_DigestInit_ex(mdctx, md, NULL);
    EVP_DigestUpdate(mdctx, str.c_str(), str.size());
    EVP_DigestFinal_ex(mdctx, hash, &md_len);
    EVP_MD_CTX_free(mdctx);

    std::stringstream ss;
    for(int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
        ss << std::hex << std::setw(2) << std::setfill('0') << (int)hash[i];
    }
    return ss.str();
}

std::string int_to_string(int x) {
    char buf[20];
    sprintf(buf, "%d", x);
    std::string str(buf);
    return str;
}

int uchars_to_int(uchar* ptr) {
    int x = 0;
    for (int i=0; i<4; i++) {
        x *= 256;
        x += *(ptr+i);
    }
    return x;
}

void int_to_uchars(int x, uchar* ptr) {
    for (int i=3; i>=0; i--) {
        *(ptr+i) = x % 256;
        x /= 256;
    }
}

std::string uchars_to_hex(uchar* ptr, int len) {
    std::string output;
    char hex[17] = "0123456789abcdef";
    for (int i = 0; i < len; ++i) {
        output += hex[(ptr[i] >> 4) & 0x0F];
        output += hex[ptr[i] & 0x0F];
    }
    return output;
}

uchar* hex_to_uchars(std::string str) {
    if(str.length() % 2 != 0) {
        throw PredefinedException("string_to_chars: string length is not even");
    }
    uchar* result = new uchar[str.length()/2];
    for (size_t i = 0; i < str.length(); i += 2) {
        std::string byteString = str.substr(i, 2);
        uchar byte = (uchar) strtol(byteString.c_str(), NULL, 16);
        result[i/2] = byte;
    }
    return result;
}


std::vector<std::string> string_split(const std::string& str, char delimiter) {
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(str);
    while (std::getline(tokenStream, token, delimiter)) {
        tokens.push_back(token);
    }
    return tokens;
}


// store opened_tables as txt
void save_tables_to_txt(const std::string filename, std::vector<Table> opened_tables) {
    std::ofstream file(filename.c_str());
    if (file.is_open()) {
        for (int i=0; i<opened_tables.size(); i++) {
            Table table = opened_tables[i];
            file << "[" << table.name << "]" << std::endl;
            file << "version=" << table.version << std::endl;
            file << "db_file_dir_path=" << table.db_file_dir_path << std::endl;
            file << "mkp_files_dir_path=" << table.mkp_files_dir_path << std::endl;
            file << std::endl;
        }
        file.close();
        std::cout << "Tables saved to TXT file: " << filename << std::endl;
    } else {
        std::cerr << "Unable to open file: " << filename << std::endl;
    }
}

// read data from txt to opened_tables
std::vector<Table> load_tables_from_txt(const std::string filename) {
    std::vector<Table> opened_tables;
    std::ifstream file(filename.c_str());
    if (file.is_open()) {
        std::string line;
        Table table;
        while (std::getline(file, line)) {
            if (line.empty()) {
                if (!table.name.empty()) {
                    opened_tables.push_back(table);
                    table = Table();
                }
            } else if (line[0] == '[') {
                table.name = line.substr(1, line.size() - 2);
            } else {
                size_t pos = line.find('=');
                std::string key = line.substr(0, pos);
                std::string value = line.substr(pos + 1);
                if (key == "version") {
                    table.version = std::atoi(value.c_str());
                } else if (key == "db_file_dir_path") {
                    table.db_file_dir_path = value;
                } else if (key == "mkp_files_dir_path") {
                    table.mkp_files_dir_path = value;
                }
            }
        }

        if (!table.name.empty()) {
            opened_tables.push_back(table);
        }

        file.close();
        std::cout << "Tables loaded from TXT file: " << filename << std::endl;
    } else {
        std::cerr << "Unable to open file: " << filename << std::endl;
    }
    return opened_tables;
}

bool is_file_exist(std::string name) {
    std::ifstream f(name.c_str());
    return f.good();
}

bool is_dir_exist(std::string path) {
    if (access(path.c_str(), F_OK) == 0) {
        return true;  // path exists
    } else {
        return false; // path doesn't exist
    }
}


bool create_dir(std::string path) {
    if (is_dir_exist(path)) {
        return true;
    } else {
        int flag = mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        if (flag == 0) {
            return true;
        } else {
            return false;
        }
    }
}

bool create_file(std::string name) {
    if (is_file_exist(name)) {
        std::cout << "File '" << name << "' already exists." << std::endl;
        return false;
    } else {
        std::ofstream file(name.c_str(), std::ios::out | std::ios::binary);
        if (file.is_open()) {
            file.close();
            // Set file limits for UNIX/Linux system
            chmod(name.c_str(), S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
            std::cout << "File '" << name << "' created successfully." << std::endl;
            return true;
        } else {
            std::cout << "Failed to create file '" << name << "'." << std::endl;
            return false;
        }
    }
}


std::vector<std::string> parse_path(std::string path) {
    std::vector<std::string> result = string_split(path, '/');
    if(result.size() != 0 && result[0] == "")
        result[0] = '/';
    return result;
}

bool copy_file(const std::string sourcePath, const std::string destinationPath) {
    if(!is_file_exist(sourcePath)) {
        std::cerr << "Source file does not exist: " << sourcePath << std::endl;
        return false;
    }
    if(!is_file_exist(destinationPath)) {
        create_file(destinationPath);
    }

    std::ifstream sourceFile(sourcePath.c_str(), std::ios::binary);
    std::ofstream destinationFile(destinationPath.c_str(), std::ios::binary);

    if (!sourceFile || !destinationFile) {
        return false; // copy file fail
    }

    destinationFile << sourceFile.rdbuf();

    return true; // copy file success
}

bool is_files_equal(std::string filePath1, std::string filePath2) {
    std::ifstream file1(filePath1.c_str(), std::ios::binary);
    std::ifstream file2(filePath2.c_str(), std::ios::binary);

    if (!file1 || !file2) {
        return false; // open file fail
    }

    char ch1, ch2;
    while ((ch1 = file1.get()) != EOF && (ch2 = file2.get()) != EOF) {
        if (ch1 != ch2) {
            return false; // file content doesn't fit
        }
    }
    if((ch2 = file2.get()) != EOF) {
        return false; // file2 doesn't reach EOF
    }
    // If both files reaches EOF, they are exactly the same
    return file1.eof() && file2.eof();
}

int get_type_size(int typ) {
    if (typ == INT_TYPE) {
        return INT_SIZE;
    } else if (typ == STR20_TYPE) {
        return STR20_SIZE;
    } else {
        throw PredefinedException("get_type_size: invalid type");
    }
}

int get_types_size(int typs) {
    int size = 0;
    while(typs != 0) {
        size += get_type_size(typs % 8);
        typs /= 8;
    }
    return size;
}

int get_var_length(uchar* buf, int fields_typ) {
    int i = 0;
    int rev_fields_typ = 0;
    while(fields_typ != 0) {
        rev_fields_typ = rev_fields_typ * 8 + fields_typ % 8;
        fields_typ /= 8;
    }
    while(rev_fields_typ != 0) {
        if (rev_fields_typ % 8 == STR20_TYPE) {
            i += buf[i] + 1;
        } else if (rev_fields_typ % 8 == INT_TYPE) {
            i += 4;
        } else {
            throw PredefinedException("get_var_length: invalid type");
        }
        rev_fields_typ /= 8;
    }
    return i;
}

std::map<std::string, std::string> parse_toml_config(const std::string& filePath) {
    std::map<std::string, std::string> config;

    std::ifstream file(filePath.c_str());
    if (!file) {
        std::cerr << "Failed to open Toml file: " << filePath << std::endl;
        return config;
    }

    std::string line;
    std::string section;
    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::string key;
        std::string value;
        if (line[line.size()-1] == '\r')
            line = line.substr(0, line.size()-1);
        if (line.size() == 0) {
            continue;
        }
        if (line[0] == '[' && line[line.size() - 1] == ']') {
            // Remove square brackets from section name
            section = line.substr(1, line.size() - 2);
            continue;
        }

        std::vector<std::string> kv = string_split(line, '=');
        if (kv.size() != 2) {
            std::cerr << "Failed to open Toml file: " << filePath << std::endl;
            std::cerr << kv[0] << std::endl;
            throw PredefinedException("parse_toml_config: invalid config file");
        }
        key = kv[0];
        value = kv[1];

        // Store key-value pair in the config map with section prefix
        if(value[0] == '\"' && value[value.size() - 1] == '\"') {
            value = value.substr(1, value.size() - 2);
        }
        std::string fullKey = section + "." + key;
        config[fullKey] = value;

    }

    return config;
}