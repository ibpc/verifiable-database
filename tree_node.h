/*
  Copyright (C) BABEC. All rights reserved.
  Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
  and Privacy Computing. All rights reserved.
  
  SPDX-License-Identifier: Apache-2.0
*/

/*
 * @file tree_node.h tree_node.cc
 * @brief defines record, treenode struct and treenode method
 *
 * Maintainer: haojk
 * Email: jk.hao@pku.edu.cn
 *
 * Dependencies:
 *  - predefined.h
 *  - mbt_key.h
 */


#ifndef TREE_NODE_H
#define TREE_NODE_H

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <string>
#include <string.h>
#include <map>
#include <sstream>
#include <iomanip>
#include <openssl/sha.h>
#include <openssl/evp.h>

#ifdef in_dev
    #include <dbug.h>
#endif

#include <sys/stat.h>

#include "predefined.h"
#include "mbt_key.h"

#define PAGE_SIZE 16384
// 6*4+32+8=64 < 80
#define NODE_PREFIX_SIZE 80
#define INTKEY 0
#define SIMPLEKEY 1

typedef unsigned char uchar;

class TreeNodeException: public MyException {
public:
    std::string err_msg;
    TreeNodeException(std::string err_msg) {
        this->err_msg = "[tree_node] error: " + err_msg;
    }
    virtual const char* what() const throw() {
        return this->err_msg.c_str();
    }
};

/**
 * @brief record metadata
 *
 * @variable
 * 1. key type
 * 2. key length
 * 3. record length
 * 4. field types in a record
 * 5. index node or not
 * 6. maximum records in a node
 */
struct RecordMetadata {
    RecordMetadata() {}
    /*
     * @brief construction function
     *
     * throght key type and record fields type，calculate key and record length，finish the construction
     * @param key_type key type
     * @param fields_type view it as a octal digit ，every digit corresponds to a field type
     */
    RecordMetadata(int typ, int key_type, int fields_type, int node_type) {
        this->typ = typ;
        this->key_type = key_type;
        this->key_size = get_type_size(key_type);
        this->fields_type = fields_type;
        this->raw_record_size = get_types_size(fields_type);
        this->node_type = node_type;
        this->max_record_num = (PAGE_SIZE - NODE_PREFIX_SIZE) /
                (key_size+raw_record_size+SHA256_DIGEST_LENGTH);
    }
    int typ;
    int key_type;
    int key_size;
    int raw_record_size;
    int fields_type;
    int node_type;
    int max_record_num;
};

/**
 * @brief this class represents a line
 *
 * @method
 *  1.load and write record to specified memory location
 *  2.compute record hash
 *  3.compare records by index 
 *
 * @variable
 *  1.index key
 *  2.record not decoded
 */
class Record {
public:
    static std::map<int, RecordMetadata> metadatas; ///<map of all record metadatas

    MbtKey key; ///<record key
    uchar *raw_record; ///<raw string of the record
    bool in_node; ///<whether record is in the node
    RecordMetadata *metadata; ///<record metadata

    /**
     * @brief construction function
     *
     * @param in_node whether record is in the node，if it is in the node，destruction function wouldn't free raw_record and key.raw_key
     * @param typ record type
     */
    Record(bool in_node, int typ) {
        this->in_node = in_node;
        if (!this->in_node) {
            this->raw_record = new uchar[metadatas[typ].raw_record_size];
            this->key.raw_key = new uchar[metadatas[typ].key_size];
        } else {
            this->raw_record = NULL;
            this->key.raw_key = NULL;
        }
        this->metadata = &metadatas[typ];
    }

    /**
     * @brief destruction function，if record not in node，free raw_record and key.raw_key
     */
     ~Record() {
        if (!this->in_node) {
            delete[] this->raw_record;
            delete[] this->key.raw_key;
        }
     }

    /**
     * @brief get record length（key+raw_record)
     * @param typ record type
     * @return record length
     */
    static int get_record_size(int typ) {
        return metadatas[typ].key_size + metadatas[typ].raw_record_size;
    }
    static int get_key_size(int typ) {
        return metadatas[typ].key_size;
    }
    static int get_raw_record_size(int typ) {
        return metadatas[typ].raw_record_size;
    }

    /**
     * @brief compute hash of record
     * @param typ record type
     * @return a 32B hash
     */
    Hash& hash() {
        std::string input = uchars_to_hex(raw_record, metadata->raw_record_size);
        return Hash::sha256(input);
    };

    /**
     * @brief encode raw_record into hexadecimal string
     * @return hexadecimal string
     */
    std::string encode_to_hex() {
        return uchars_to_hex(raw_record, metadata->raw_record_size);
    }

    /**
     * @brief determine two records are equal（key and raw_record）
     * @param other other record
     * @param typ record type
     * @return equal returns true， otherwise false
     */
    bool equal(const Record& other) const {
        if (memcmp(this->key.raw_key, other.key.raw_key,
                   metadata->key_size) != 0) {
            return false;
        }
        if (memcmp(this->raw_record, other.raw_record,
                   metadata->raw_record_size) != 0) {
            return false;
        }
        return true;
    }
    /**
     * @brief deep copy of record（key and raw_record）,other->this
     * @param other target path
     * @param typ record type
     */
    void copy(const Record& other) {
        if (this != &other) {
            memcpy(key.raw_key, other.key.raw_key, metadata->key_size);
            memcpy(raw_record, other.raw_record, metadata->raw_record_size);
        }
    }
    /**
     * @brief compare record by index
     * @param other record to compare
     * @return compare results
     */
    bool lt(Record& other)  {
        return key.lt(other.key, metadata->key_type);
    }
    bool gt(Record& other)  {
        return key.gt(other.key, metadata->key_type);
    }
    bool eq(Record& other)  {
        return key.eq(other.key, metadata->key_type);
    }
    bool neq(Record& other)  {
        return key.neq(other.key, metadata->key_type);
    }
    /**
     * @brief read raw_record as aint
     * @return return position of child node
     */
    int get_pos() const {
        int pos = uchars_to_int(raw_record);
        return pos;
    }
    /**
     * @brief store a int into raw_record
     * @param pos child node position
     */
    void set_pos(int pos) {
        if(raw_record == NULL) {
            throw TreeNodeException("raw_record is NULL");
        }
        int_to_uchars(pos, raw_record);
    }
    /**
     * @brief check whether record exists
     * if key and raw_record are NULL the record doesn't exist
     * @return if record exists it returns false, otherwise true
     */
     bool is_empty() const {
        return key.raw_key == NULL && raw_record == NULL;
    }

    /**
     * @brief record to string
     * return type，int,str,int,str
     * 10\nhaojk\n20\nabcdefg
     * @return use \n to delimit fields
     */
    std::string to_string() {
        std::vector<std::string> fields;
        std::string result;
        int typ = metadata->fields_type;
        std::vector<int> field_types;
        while (typ) {
            field_types.push_back(typ % 8);
            typ /= 8;
        }
        int j=0;
        for (int i=field_types.size()-1; i>=0; i--) {
            if (field_types[i] == INT_TYPE) {
                int x = 0;
                for(int k=INT_SIZE-1; k>=0; k--) {
                    x *= 256;
                    x += raw_record[j+k];
                }
                fields.push_back(int_to_string(x));
                j += 4;
            } else if (field_types[i] == STR20_TYPE) {

                fields.push_back("abcde");
                j += 20;
            } else {
                throw TreeNodeException("unknow fields_type");
            }
            typ /= 8;
        }
        for (int i=0; i<fields.size(); i++) {
            result += fields[i] + "\n";
        }
        return result;
    }
};


/**
 * @brief a mbt treenode
 *
 * leaf node and index node, differs in raw_record
 * leaf node raw_record可 can be interpreted as on or more segments by fields_type
 * index node raw_record is int，fields_type is 1(int)
 *
 * memory layout
 * |<-------------------------------------60B------------------------------------------------>|
 * |typ|parent|current|next|prev|n|record1(key,raw_record)|Hash|000000000000000000000000000000|
 * |record1(key,raw_record)|hash1|record2(key,raw_record)|hash2|..............................|
 * |..........................................................................................|
 *
 * @method
 *  1.curd of data in node
 *  2.get xth record in node
 *  3.node merge and split
 *
 * @variable
 *  1.index node or leaf node
 *  2.position of current node, parent node,previous node and next ndoe
 *  3.number of records in node
 *  4.record hash
 *  5.record list
 */
class TreeNode {
public:
    int node_type;
    int parent;
    int current;
    int next;
    int prev;
    int n;
    uchar hash[SHA256_DIGEST_LENGTH];
    // record metadata，if use create_new_node，needn't initialize manually，if through map need
    RecordMetadata *metadata;

    /**
     * @brief construct an empty node in an allocated memory page
     * @param node a successive memory page(16KB)
     * @param parent parent node position
     * @param current current node position
     * @param next next node position
     * @param prev previous node position
     */
    static void create_new_node(TreeNode *node, int typ, int parent,
                                int current, int next, int prev);

    /**
     * @brief deconstruction function
     */
     ~TreeNode() {
         delete &hash;
     }

    /**
     * @brief get maximum node capacity
     * @param typ
     * @return maximum node capacity
     */
    static int get_max_child_num(int typ);

    /**
     * @brief find position of which is greater than or equal to key
     * @param key to find
     * @return offset of the found key
     */
    int lower_find(const MbtKey &key) const;
    /**
     * @brief find position of which is greater than key
     * @param key to find
     * @return offset of the found key
     */
    int upper_find(const MbtKey &key) const;

    /**
     * @brief get the offset of the raw_key in the node of a position
     * use head pointer and the offset can get the raw_key of the record
     * @param pos position of the record in the node
     * @return offset by byte
     */
    int compute_key_offset(int pos) const;

    /**
     * @brief get the offset of the raw_record in the node of a position
     * use head pointer and the offset can get the raw_record of the record
     * @param pos position of the record in the node
     * @return offset by byte
     */
    int compute_record_offset(int pos) const;

    /**
     * @brief get the offset of the hash in the node of a position
     * use head pointer and the offset can get the hash of the record
     * @param pos position of the record in the node
     * @return offset by byte
     */
    int compute_hash_offset(int pos) const;

    /**
     * @brief reset value of the record
     * @param record record to write
     * @param pos the position of the record to write
     */
    void set_child(Record &record, int pos);
    /**
     * @brief get value of the record
     * @param pos postion of the record to get
     * @note Record need to be free
     * @return record get
     */
    Record& get_child(int pos) const;
    /**
     * @brief get hash of the record
     * @param pos record number to get
     * @return hash to return
     */
    Hash& get_hash(int pos);
    /**
     * @brief set hash
     * @param hash hash to be set
     * @param pos position of the hash
     */
     void set_hash(Hash& hash, int pos);
    /**
     * @brief search whether the corresponding key can be found(only the first one fits)
     * @param key search key
     * @param record return parameter and the position of the record found
     * @return if can find return true，otherwise false
     */
    bool find(const MbtKey &key, Record& record) const;
    /**
     * @brief insert record into the node
     * @param record record to be inserted
     */
    void insert(Record &record);


    /**
     * @brief get index of the node
     * @return index of the first record
     */
    MbtKey get_node_key();

    /**
     * @brief node spilt
     * @param next_node split the second half to next_node
     */
    void split(TreeNode &next_node);
    /**
     * @brief node merge
     * @param first_node put records in second_node after first_node
     * @param second_node clean after merge
     */
    static void merge(TreeNode &first_node, TreeNode &second_node);

    /**
     * @brief update hash
     * @return return root hash
     */
    Hash& update_hash();

};



#endif //TREE_NODE_H
