# 可验证数据库

## 概述

链下可验证数据库通过Merkle B+树实现链下SQL数据存储和长安链的链上验证。
链下可验证数据库支持以独立的存储引擎模式或MySQL插件模式运行。


## 相关文档

- [使用文档](https://docs.chainmaker.org.cn/dev/%E5%8F%AF%E9%AA%8C%E8%AF%81%E6%95%B0%E6%8D%AE%E5%BA%93%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3.html)：介绍链下可验证数据库及配套合约的部署和使用方式，便于用户快速上手使用。

- [技术文档](https://docs.chainmaker.org.cn/tech/%E5%8F%AF%E9%AA%8C%E8%AF%81%E6%95%B0%E6%8D%AE%E5%BA%93%E6%8A%80%E6%9C%AF%E6%96%87%E6%A1%A3.html)：介绍链下可验证数据库的设计思路，便于开发者深入了解底层原理。

- [开发文档](https://docs.chainmaker.org.cn/dev/%E5%8F%AF%E9%AA%8C%E8%AF%81%E6%95%B0%E6%8D%AE%E5%BA%93%E5%BC%80%E5%8F%91%E6%96%87%E6%A1%A3.html)：介绍链下可验证数据库的代码结构以及源码编译和测试方法，便于开发者快速了解存储引擎的具体实现方式，并能基于本项目二次开发。

## 兼容版本

长安链v2.3.1

## 目录结构

```
│  bplus_tree.h         # 数据结构
|  bplus_tree.cc        # 数据结构
│  CMakeLists.txt       # CMackLists
│  file_manager.cc      # 与文件系统交互
│  file_manager.h       # 与文件系统交互
│  ha_mbtree.cc         # MySQL插件接口
│  ha_mbtree.h          # MySQL插件接口
│  mbt_key.cc           # 索引类型
│  mbt_key.h            # 索引类型
│  predefined.cc        # 基础类和函数
│  predefined.h         # 基础类和函数
│  README.md            # 项目说明
│  tree_node.h          # 树节点类
├─contracts             # 智能合约目录
├─docs                  # 文档目录
└─tests                 # 测试文件目录
```