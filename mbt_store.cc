/*
  Copyright (C) BABEC. All rights reserved.
  Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
  and Privacy Computing. All rights reserved.
  
  SPDX-License-Identifier: Apache-2.0
*/

/*
 * @file mbt_store.cc
 * @brief command line control of mbt storage engine
 *
 * Maintainer: haojk
 * Email: jk.hao@pku.edu.cn
 *
 * Dependencies:
 *  - bplus_tree.h
*/

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <map>

#include "predefined.h"
#include "bplus_tree.h"
#include "mbt_key.h"

std::string data_dir_path = "/data/chainmaker_mbt/mbt_store_data/";
std::string mkp_file_dir_path = "/data/chainmaker_mbt/merkle_proofs";
std::string search_result_file_dir_path = "/data/chainmaker_mbt/search_results";

std::string config_path = data_dir_path + "config.txt";
std::string db_file_dir_path = data_dir_path + "mbt_db_files";


//std::vector<Table> imported_tables;
//std::map<int, BplusTree*> opened_trees;

TableManager table_manager(config_path);

int print_help() {
    std::cout << "1. Create table" << std::endl;
    std::cout << "2. Import table" << std::endl;
    std::cout << "3. Insert data" << std::endl;
    std::cout << "4. Query data" << std::endl;
    std::cout << "5. Verify Merkle proof" << std::endl;
    std::cout << "6. Duplicate table" << std::endl;
    std::cout << "7. Exit" << std::endl;
    std::cout << "Enter your choice: ";
}
/**
 * @brief create table
 * @param input {table name，field name，field type number}
 */
void create_table(std::vector<std::string> inputs) {
    if(inputs.size() != 3) {
        std::cerr << "Error: invalid inputs" << std::endl;
        return;
    }
    Table table;
    table.name = inputs[0];
    table.version = 0;
    table.db_file_dir_path = db_file_dir_path;
    table.mkp_files_dir_path = mkp_file_dir_path;
    if(!table_manager.import_table(table)) {
        std::cerr << "Error: table already exists" << std::endl;
        return;
    }
    std::string table_path = db_file_dir_path + "/"
                             + inputs[0] + "-"
                             + int_to_string(table.version) + ".mb";
    int field_types = std::atoi(inputs[2].c_str());
    int index_type = field_types;
    while (index_type / 8 != 0) {
        index_type /= 8;
    }
    BplusTree* bpt = new BplusTree(
            &table_path[0],
            index_type,
            field_types,
            true);
    //must open successfully
    table_manager.open_table(table.name, table.version, bpt);
    std::cout << "Table created at " << table_path << std::endl;
}


/**
 * @brief open table
 * @param inputs {table_name, version, file_path(if not imported)}
 */
void open_table(std::vector<std::string> inputs) {
    if (inputs.size() != 3) {
        std::cerr << "Error: invalid inputs" << std::endl;
        return;
    }
    if (inputs[2] == "NULL") {
        //if imported
        int table_index = table_manager.is_imported(inputs[0], std::atoi(inputs[1].c_str()));
        if (table_index == -1) {
            std::cerr << "Error: table not imported" << std::endl;
            return;
        } else {
            if (table_manager.is_opened(table_index)) {
                std::cout << "Table already opened" << std::endl;
            } else {
                std::string table_path = db_file_dir_path + "/"
                                         + inputs[0] + "-"
                                         + inputs[1] + ".mb";
                BplusTree *bpt;
                try {
                    bpt = new BplusTree(
                            &table_path[0],
                            -1,
                            -1,
                            false);
                } catch (MyException &e) {
                    std::cerr << e.what() << std::endl;
                    return;
                }
                table_manager.open_table(inputs[0], std::atoi(inputs[1].c_str()), bpt);
                std::cout << "Table opened" << std::endl;
                return;
            }
        }
    } else {
        //if not imported
        std::string table_path;
        //copy file to db_file_dir_path
        table_path = db_file_dir_path + "/"
                     + inputs[0] + "-"
                     + inputs[1] + ".mb";
        copy_file(inputs[2], table_path);
        //import table
        Table table;
        table.name = inputs[0];
        table.version = std::atoi(inputs[1].c_str());
        table.db_file_dir_path = db_file_dir_path;
        table.mkp_files_dir_path = mkp_file_dir_path;
        table_manager.import_table(table);
        //open table
        BplusTree *bpt;
        try {
            bpt = new BplusTree(
                    &table_path[0],
                    -1,
                    -1,
                    false);
        } catch (MyException &e) {
            std::cerr << e.what() << std::endl;
            return;
        }
        std::string root_hash = uchars_to_hex(bpt->metadata->root_hash, SHA256_DIGEST_LENGTH);
        std::string table_mkp_path = mkp_file_dir_path + "/"
                                     + inputs[0] + "-"
                                     + inputs[1] + ".hash";
        std::cout << "Table root hash file: " << table_mkp_path << std::endl;
        std::ofstream table_hash_file(table_mkp_path.c_str());
        table_hash_file << root_hash << std::endl;
        table_hash_file.close();
        table_manager.open_table(inputs[0], std::atoi(inputs[1].c_str()), bpt);
        std::cout << "Table imported and opened" << std::endl;
        return;
    }
}

/**
 * @brief range search
 * @param inputs {table_name, version, left_key, right_key}
 */
void query(std::vector<std::string> inputs) {
    int table_index = table_manager.is_imported(inputs[0], std::atoi(inputs[1].c_str()));
    if (table_index == -1) {
        std::cerr << "Error: table not imported" << std::endl;
        return;
    } else {
        if(!table_manager.is_opened(table_index)) {
            std::cerr << "Error: table not opened at " << table_index << std::endl;
            return;
        }
    }
    std::string res_file_name = inputs[0] + "-" + inputs[1] + "-" +
                                      inputs[2] + "-" + inputs[3];

    std::string range_mkp_file_path = mkp_file_dir_path + "/" + res_file_name + ".rmkp";
    std::string subtree_mkp_file_path = mkp_file_dir_path + "/" + res_file_name + ".stmkp";
    std::string search_result_file_path = search_result_file_dir_path + "/" + res_file_name + ".res";
    if (!is_file_exist(range_mkp_file_path) || !is_file_exist(search_result_file_path)) {
        BplusTree* bpt = (BplusTree*) table_manager.get_opened_tree(table_index);
        MbtKey *left = NULL, *right = NULL;
        if (inputs[2] != "NULL") {
            MbtKey tmp_left = MbtKey::from_int(std::atoi(inputs[2].c_str()));
            left = &tmp_left;
        }
        if (inputs[3] != "NULL") {
            MbtKey tmp_right = MbtKey::from_int(std::atoi(inputs[3].c_str()));
            right = &tmp_right;
        }
        bpt->init_search(left, right, true);
        std::string range_mkp = bpt->iter->generate_range_merkle_proof();
        std::string subtree_mkp = bpt->iter->generate_subtree_merkle_proof();
        std::cout << "Search result: " << std::endl;
        std::string search_result;
        if(bpt->iter->state == EMPTY_ITER) {
            std::cout << "[result] empty iter" << std::endl;
        }
        bpt->iter->prev();
        while(bpt->iter->next() != NULL) {
            int key = bpt->iter->current_record->key.to_int();
            std::string raw_record =
                    uchars_to_hex(bpt->iter->current_record->raw_record,
                                     bpt->iter->current_record->metadata->raw_record_size);
            search_result += int_to_string(key) + " | " + raw_record + "\n";
            search_result += bpt->iter->current_record->to_string();
            std::cout << "[result]" << int_to_string(key) + " | " + raw_record << std::endl;
        }
        std::ofstream range_mkp_file(range_mkp_file_path.c_str());
        if (range_mkp_file.is_open()) {
            range_mkp_file << range_mkp;
            range_mkp_file.close();
        } else {
            std::cerr << "Failed to open file." << std::endl;
        }
        std::ofstream subtree_mkp_file(subtree_mkp_file_path.c_str());
        if (subtree_mkp_file.is_open()) {
            subtree_mkp_file << subtree_mkp;
            subtree_mkp_file.close();
        } else {
            std::cerr << "Failed to open file." << std::endl;
        }
        std::ofstream search_result_file(search_result_file_path.c_str());
        if (search_result_file.is_open()) {
            search_result_file << search_result;
            search_result_file.close();
        } else {
            std::cerr << "Failed to open file." << std::endl;
        }
    }

    std::cout << "Range Merkle Proof saved at: " << range_mkp_file_path << std::endl;
    std::cout << "Search result saved at: " << search_result_file_path << std::endl;
}


int main() {
    while(true) {
        print_help();
        int choice;
        std::cin >> choice;
        if (choice == 1) {
            std::vector<std::string> inputs;

            std::string table_name;
            std::cout << "Enter table name:__";
            std::cin >> table_name;
            inputs.push_back(table_name);

            std::string field_names;
            std::cout << "Enter field names (separated by space):__";
            std::cin.ignore();
            std::getline(std::cin, field_names);
            inputs.push_back(field_names);

            std::string field_types;
            std::cout << "Enter field types number:__";
            std::getline(std::cin, field_types);
            inputs.push_back(field_types);

            create_table(inputs);
        } else if(choice == 2) {
            std::vector<std::string> inputs;
            std::string table_name;
            std::string version;
            std::string table_path;
            std::cout << "Enter table name:__";
            std::cin >> table_name;
            inputs.push_back(table_name);
            std::cout << "Enter table version:__";
            std::cin >> version;
            inputs.push_back(version);
            std::cout << "Enter table path (if imported enter NULL):__";
            std::cin >> table_path;
            inputs.push_back(table_path);
            open_table(inputs);
        } else if(choice == 3) {

        } else if(choice == 4) {
            std::vector<std::string> inputs;
            std::string table_name, version, left_value, right_value;

            std::cout << "Enter table name:__";
            std::cin >> table_name;
            inputs.push_back(table_name);
            std::cout << "Enter table version:__";
            std::cin >> version;
            inputs.push_back(version);
            std::cout << "Enter query left value:__";
            std::cin >> left_value;
            inputs.push_back(left_value);
            std::cout << "Enter query right value:__";
            std::cin >> right_value;
            inputs.push_back(right_value);

            query(inputs);

        } else if(choice == 7) {
            std::map<int, void*>::iterator it;
            for (it = table_manager.opened_trees.begin(); it != table_manager.opened_trees.end(); ++it) {
                std::cout << "Close table " <<
                    table_manager.imported_tables[it->first].name << "-" <<
                    table_manager.imported_tables[it->first].version << std::endl;
                delete it->second;
            }
            table_manager.save_config();
            std::cout << "Exit..." << std::endl;
            break;
        } else {
            std::cerr << "Invalid choice" << std::endl;
        }
    }
    return 0;
}