/*
  Copyright (C) BABEC. All rights reserved.
  Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
  and Privacy Computing. All rights reserved.
  
  SPDX-License-Identifier: Apache-2.0
*/

/*
 * @file bplus_tree.h
 * @brief define B+ tree related class and method
 *
 * include B+tree class and B+tree iterator class
 *
 * Maintainer: haojk
 * Email: jk.hao@pku.edu.cn
 *
 * Dependencies:
 *  - predefined.h
 *  - tree_node.h
 *  - file_manager.h
 *  - mbt_key.h
*/

#ifndef BPLUS_TREE_H
#define BPLUS_TREE_H

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <string>
#include <openssl/sha.h>
#include <sys/stat.h>
#ifdef in_dev
    #include <dbug.h>
#endif

#include "tree_node.h"
#include "file_manager.h"

/* offsets */
#define OFFSET_META 0
#define OFFSET_ROOT 1
#define OFFSET_FIRST_LEAF 2
#define OFFSET_BLOCK OFFSET_META + sizeof(meta_t)
#define META_ID 1234
#define ROOT_ID 1000
#define INTER_ID 1111
#define LEAF_ID 2222
#define UNUSED_ID 9999
#define INITED 0x1234

#define METADATA_POSITION 0
#define ROOT_FILE_PAGES_MANAGER_POSITION 1
#define NOT_EXIST_NODE_POS -1

class BplusTreeException: public MyException {
public:
    std::string err_msg;
    BplusTreeException(std::string err_msg) {
        this->err_msg = "[bplus_tree] error: " + err_msg;
    }
    virtual const char* what() const throw() {
        return this->err_msg.c_str();
    }
};

class Iterator;

/**
 * @brief MBT main class， implements main methods of MBT
 *
 * @method
 *  1.CURD
 *  2.merkle proof generation and check
 *
 * @variable
 *  1.root node pointer
 *  2.leaf node type and index node type
 *  3.current search iterator
 */
class BplusTree: public FilePagesManager, public FileMapper {
public:
    int internal_node_type; ///<index node type
    int leaf_node_type; ///<leaf node type
    TreeNode *root_node; ///<root pointer
    Iterator* iter;///<current search iterator

    /**
     * @brief construction function
     * @param file_path database file path
     * @param index_type index type
     * @param field_types record field type
     * @param force_empty whether empty file
     */
    BplusTree(char *file_path, int index_type, int field_types, bool force_empty = false);
    ///destruction function，clean record metadate type
    ~BplusTree();
    /**
     * @brief set indexctype and field type
     * @param index_types index type
     * @param field_types field type
     */
    void set_record_type(int index_types, int field_types);

    /**
     * @brief use b+tree index to range query
     * @param left left bound index
     * @param right right bound index
     * @param values return，position of record found
     * @param max values maximum
     * @param next return，finished or not，if =false keep seaching，=true end search
     * @return values, number of results
     */

    /**
     * @brief insert
     * @param key index of inserting record
     * @param value record to insert
     * @return if not only one indexes true，if only one index itself is false，otherwise true
     */
    bool insert(MbtKey& key, uchar* value);


    /**
     * @brief construct MBT from empty file
     */
    void init_from_empty();
    /**
     * @brief search which leaf node has index greater than or equal to target
     * if no equal find the first greater
     * if all less return last node
     * if all greater return first node
     * @param search_key index to query
     * @return leaf node number found
     */
    int search_child(const MbtKey& search_key);

    /**
     * @brief get first leaf node pointer
     * @return first leaf node pointer
     */
     TreeNode* get_first_leaf();

     /**
      * @brief get last leaf node pointer
      * @return last leaf node pointer
      */
    TreeNode* get_last_leaf();

    /**
     * @brief initialize a iterator by query index
     * only one iterator, no parallel
     * @param left_key leaf bound index
     * @param right_key right bound index
     * @param need_mkp whether generate merkle proof
     */
    void init_search(MbtKey* left_key, MbtKey* right_key, bool need_mkp);


    /**
     * @brief update mbtree hash
     * recursively update
     * @param root_pos mbtree or subtree root position
     * @return hash after update
     */
    Hash& update_hash(int root_pos);

    /**
     * @brief read a node from file
     * file_mapper map upper level interface
     * @param node_pos node locatiob
     * @return ndoe pointer
     */
    TreeNode* map(int node_pos) {
        TreeNode* node = (TreeNode*)FileMapper::map(node_pos);
        if (node->node_type == LEAF_TYPE)
            node->metadata = &Record::metadatas[this->leaf_node_type];
        else if (node->node_type == INDEX_TYPE)
            node->metadata = &Record::metadatas[this->internal_node_type];
        return node;
    }

    /**
     * @brief generate merkle proof
     * merkle proof format
     * key: 100
     * record: 0x123456 (raw_record hex)
     * 0x123456...... (record hash)
     * 0x123456...... 0x123456..... 0x...... 0x....... (each level hash list，n level)
     * 0x123456..... (node hash，n level）
     * 0x...... 0x...... (each level hash list，n-1 level)
     * ........
     * 0x123456...... (root_hash)
     * @param node_pos leaf node position of record
     * @param record_pos record position in node
     * @return merkle proof，string 
     */
    std::string generate_merkle_proof(int node_pos, int record_pos);


    static bool verify_range_merkle_proof(std::string& range_mkp, Hash& root_hash);

    static bool verify_subtree_merkle_proof(std::string& range_mkp, Hash& root_hash);

};

const int EMPTY_TREE = -1; // empty
const int IN_ITER = 0;  // 1*2*3
const int EMPTY_ITER = 1; // **
const int BEFORE_FIRST = 2; // **123
const int AFTER_LAST = 3; // 123**

/**
 * @brief a MBT iterator
 *
 * traverse MBT leaf node，partly or all
 *
 * @method
 *  1.construct iterator by key range，or a global one
 *  2.traverse leaf node，next、prev、first、last
 *
 * @variable
 *  1.MBT pointer
 *  2.leaf node pointer
 *  3.leaf node position
 *  4.leaf node record
 *  5.leaf and right bound
 *  6.iterator status
 *  7.whether generate merkle proof
 */
class  Iterator {
public:
    BplusTree *tree;
    /**
     * track iterator location
     * [left-1, right+1]
     * -1 and +1 forrange merkle proof
     * -1 BEFORE_FIRST，+1 AFTER_LAST
     */
    TreeNode *current_node;
    int current_record_pos;
    Record* current_record;

    MbtKey* left, *right;
    bool need_mkp;

    /**
     * merkle proof file path
     * merkle proof:left_key right_key first line
     * 4 merkle proof(mkps)
     * key range: left_key, right_key
     * mkps[0]:
     * ...
     * ......
     * 
     * mkps[1]:
     * mkps[2]:
     * mkps[3]:
     */
    std::string mkps_file_path="./mkps.txt";

    int state;

    /**
     * @brief construct iterator，traverse MBT
     * @param tree MBT object pointer
     */
    Iterator(BplusTree* tree);

    /**
     * @brief construct range iterator
     *
     * if left right empty，则为全局迭代器
     * if left empty，right not empty，search for less than right
     * if left not empty，right empty，search for greater than left
     * if left and right not empty，search for greater than left and less than right
     *
     * iterator at first record，if exists，get first record reference
     * 12345, [2,4]
     *  x--
     * if iterator empty，move to first greater than left_key
     * 12678, [3,5]
     *   x
     * if iterator empty，left_key greater than all records，move to last record
     * 12345 , [6,9]
     *      x
     * if iterator empty，left_key less than all records，move to first record
     * 34567, [1,2]
     * x
     *
     *
     * @param tree MBT object pointer
     * @param left range query left bound
     * @param right range query right bound
     * @param need_mkp whether need merkle proof
     */
    Iterator(BplusTree* tree, MbtKey* left, MbtKey* right, bool need_mkp);

    /**
     * @brief destruction function
     */
     ~Iterator() {
         delete left;
         delete right;
     }

    /**
     * @brief move iterator，get next record reference，if  exceeds iterator range return NULL record reference
     * 12345
     *  ---
     * 1->return null，iterator is BEFORE_FIRST，current* is 1
     * 234->return record，iterator is IN_ITER，current* is corresponding position
     * 5->return null，iterator is AFTER_LAST，current* is 5
     * @return next record reference
     */
    Record* next();

    /**
     * @brief move iterator，get previous record reference，if  exceeds iterator range return NULL record reference
     * 
     * @return previous record reference
     */
     Record* prev();

     /**
      * @brief get first record reference
      * iterator location construction function
      */
     void first();

    /**
     * @brief get last valid record and after
     * if no valid record last==NULL，after_last==NULL
     * if the last record is valid，after_last==NULL
     * @return [last_node_pos, last_record_pos,
     *  after_last_node_pos, after_last_record_pos]
     */
     int* get_last_and_after_last_record();

    /**
    * @brief get first valid record and before
    * if no valid record first==NULL，before_first==NULL
    * if the first record is valid，before_first==NULL
    * @return [before_first_node_pos, before_first_record_pos,
    *   first_node_pos, first_record_pos]
    */
     int* get_before_first_and_first_record();



    /**
     * @brief generaterange merkle proof
     * query result not empty，generate mkps 01234 merkle proof
     * query result empty，generate mkps 03 merkle proof，12 empty
     * if 0 or 3 exceeds boundary，or boundary not set ，generate mkps 12 merkle proof，03 empty
     * if tree empty，null
     *
     * 4 merkle proof
     * mkps[0]->before left_key merkle proof
     * mkps[1]->left_key，merkle proof
     * mkps[2]->right_key，merkle proof
     * mkps[3]->after right_key merkle proof
     *
     * @return a string has left and right key, 4 merkle proof，write into mkps_file_path
     */
    std::string generate_range_merkle_proof();

    /**
     * @brief generate subtree merkle proof
     *             node1
     *         /    \    \
     *      node2 node3 node4
     *      / \   / \   / \
     *     1   2 3   4 5   6
     * [3,4] subtree merkle proof，return node1,node3 node hash and hashlist
     * write merkle proof file
     *
     * @return hash、hashlist subtree merkle proof
     */
    std::string generate_subtree_merkle_proof();
};



#endif //BPLUS_TREE_H
