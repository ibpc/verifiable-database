/*
  Copyright (C) BABEC. All rights reserved.
  Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
  and Privacy Computing. All rights reserved.
  
  SPDX-License-Identifier: Apache-2.0
*/

/*
 * @file file_manager.h file_manager.cc
 *
 * implements file page allocation management and file page read-write management class
 *
 * Maintainer: haojk
 * Email: jk.hao@pku.edu.cn
 *
 * @dependencies
 *  - openssl
 */
#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <string>
#include <openssl/sha.h>
#include <sys/stat.h>
#include <map>

#include "tree_node.h"

/* offsets */
#define OFFSET_META 0
#define OFFSET_ROOT 1
#define OFFSET_FIRST_LEAF 2
#define OFFSET_BLOCK OFFSET_META + sizeof(meta_t)
//#define PAGE_SIZE 1024*16 //macos
#define META_ID 1234
#define ROOT_ID 1000
#define INTER_ID 1111
#define LEAF_ID 2222
#define UNUSED_ID 9999
#define INITED 0x1234

#define METADATA_POSITION 0
#define ROOT_FILE_PAGES_MANAGER_POSITION 1
#define ROOT_NODE_POSITION 2
#define NOT_EXIST_NODE_POS -1

typedef unsigned char uchar;

/**
 * @brief database file metadata structure
 *
 * metadata stores database file structure data and MBT structure data，can manage database file and MBT
 */
struct Metadata {
    int inited; ///<whether database file initialized ，if so META_ID

    int value_size; ///<record size
    int key_size; ///<index key size
    int index_type; ///<index type
    int field_types; ///<field type in record
    int file_size; ///<file size

    int height; ///<tree height
    int root_node_pos; ///<root location，default1
    int first_leaf_node_pos; ///<first leaf node location
    int last_leaf_node_pos; ///<last leaf node location

    uchar root_hash[SHA256_DIGEST_LENGTH]; ///root hash
};

class FileManagerException : public MyException {
public:
    std::string err_msg;
    FileManagerException(std::string err_msg) {
        this->err_msg = "[file_manager] error: " + err_msg;
    }
    virtual const char* what() const throw() {
        return this->err_msg.c_str();
    }
};

/**
 * @brief database file page read-write manager
 *
 * this class uses system call mmap、munmap、msync、ftrunc functions to read and write pages
 *
 * @method
 *  1.file page to memory projection and free
 *  2.write page
 *  3.database file change of size
 *
 * @variable
 *  1.database file handler and file path
 *  2.database file metadata pointer
 *  3.cached page
 */
class FileMapper {
public:
    int fd; ///<database file handler
    char file_path[512]; ///<databse file path
    Metadata *metadata; ///<database file metadata
    std::map<int, void*> pages; ///<database file page cache

    /**
     * @brief destruction function
     *
     * synchronize metadata block and free all cached pages
     */
    ~FileMapper() {
        if(metadata != NULL) {
            sync(metadata);
            unmap(metadata);
        }
        std::map<int,void*>::iterator it;
        for (it = pages.begin(); it != pages.end(); it++) {
            if(msync(it->second, PAGE_SIZE, MS_SYNC) == -1) {
                std::string err_msg = "msync failed, error " + int_to_string(errno);
                throw FileManagerException(err_msg);
            }
            if(munmap(it->second, PAGE_SIZE) == -1) {
                std::string err_msg = "munmap failed, error  " + int_to_string(errno);
                throw FileManagerException(err_msg);
            }
        }
    }
    /**
     * @brief set database file path，open file，set file handler
     * @param file_path database file path
     */
    void set_file_path_and_fd(char* file_path) {
        fd = open(file_path, O_RDWR | O_CREAT, 0644);
        memcpy(this->file_path, file_path, strlen(file_path));
    }
    /**
     * @brief set metadata pointer
     * @param metadata metadata pointer
     */
    void set_metadata(Metadata* metadata) {
        this->metadata = metadata;
    }
    /**
     * @brief determine whether metadata page is empty
     *
     * if the first bit of page is META_ID or NULL，or database file invalid
     *
     * @param page metadata page pointer
     * @return if empty true，otherwise false
     */
    static bool is_metadata_exist(void* page) {
        if ((int)(size_t) page != META_ID) {
            return true;
        }
        return false;
    }
    /**
     * @brief load page in specified location to memory
     * @param page_number xth page in database file
     * @return page pointer after loading to memory
     */
    virtual void* map(int page_number)
    {
        //if page in memory, return
        if (pages.count(page_number) != 0) {
            return pages[page_number];
        }
        void *block = mmap(NULL, PAGE_SIZE, PROT_READ | PROT_WRITE,
                           MAP_SHARED, fd, page_number*PAGE_SIZE);
        if ((long long int) block == -1) {
            std::string err_msg = "mmap failed, get no page, error " + int_to_string(errno);
//            DBUG_PRINT("error", ("FileManagerException %s", err_msg.c_str()));
            throw FileManagerException(err_msg);
        }
        pages[page_number] = block;
        return block;
    }
    /**
     * @brief free page in memory
     * @param block head pointer to page
     */
    void unmap(void *block)
    {
        std::map<int,void*>::iterator it;
        for (it = pages.begin(); it != pages.end(); it++) {
            if(it->second == block) {
                pages.erase(it->first);
                break;
            }
        }
        if (it == pages.end()) {
            std::string err_msg = "unmap failed, not map that page, empty page pointer";
            throw FileManagerException(err_msg);
        }
        if(munmap(block, PAGE_SIZE) == -1) {
            std::string err_msg = "munmap failed, error";
            throw FileManagerException(err_msg);
        }
    }
    /**
     * @brief wrtie page in memory back to database file
     * @param block head pointer to page
     */
    void sync(void *block) const
    {
        if(msync(block, PAGE_SIZE, MS_SYNC) == -1) {
            std::string err_msg = " msync failed, error " + int_to_string(errno);
            throw FileManagerException(err_msg);
        }
    }
    /**
     * @brief file resize
     * @param new_size new file size
     */
    void trunc(int new_size) {
        int state = ftruncate(fd, new_size);
        if (state == -1) {
            std::string err_msg = "ftruncate failed, error " + int_to_string(errno);
            throw FileManagerException(err_msg);
        }
    }
    /**
     * @brief use file path to get file size
     * @param file_path file path
     * @return file size in bytes
     */
    static int get_file_size_by_path(char *file_path) {
        struct stat st;
        stat(file_path, &st);
        return st.st_size;
    }
};

/**
 * @brief database file page allocator manager
 *
 * this class implements a bitmap to manage pages, bitmap is also written in pages into database file
 *
 * @method
 *  1.allocate page
 *  2.free page
 *  3.whether a page is allocated
 *  4.other auxiliary functions
 *
 * @variable
 *  1.head pointer to page allocator manager
 */
class FilePagesManager {
public:
    // can manage PAGE_SIZE*8 page，1024*8*4096B=32MB database file
    uchar *file_pages_manager_root; ///head pointer to page allocator manager

    /**
     * @brief set head pointer to page allocator manager
     * @param file_pages_manager_root head pointer to page allocator manager
     */
    void set_file_pages_manager_root(uchar* file_pages_manager_root);

    /**
     * @brief allocate an empty page
     *
     * find an empty page in database file and allocate
     * first page metadata，second page bitmap，allocation begins from 3rd
     * first two bits must be 1
     *
     * @return page number allocated
     */
    int get_a_page();
    /**
     * @brief free a page
     * @param pos page freed
     * @return if page empty return alse，if allocated free and returntrue
     */
    bool reback_a_page(int pos);
    /**
     * @brief find highest 0 in a byte
     *
     * 0->0b00000000->0
     * 123->0b1111011->5
     *
     * @param x byte
     * @return highest 0 position
     */
    static int get_zero_pos_in_byte(unsigned char x);
    /**
     * @brief generate a mask，set a bit to 1
     *
     * 0->0b10000000->128
     * 4->0b00010000->16
     *
     * @param x position 
     * @return mask
     */
    static unsigned char get_bit_exist_mask_in_pos(int x);
    /**
     * @brief whether xth position in bit map occupied
     * @param x position
     * @return if occupied return true，otherwise return false
     */
    bool exist_pos(int x);
    /**
     * @brief set xth position in bit map value
     * @param x location number
     * @param b true set 1，false set 0
     */
    void set_pos(int x, bool b);
};




#endif //FILE_MANAGER_H
