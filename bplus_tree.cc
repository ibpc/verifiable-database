/*
  Copyright (C) BABEC. All rights reserved.
  Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
  and Privacy Computing. All rights reserved.
  
  SPDX-License-Identifier: Apache-2.0
*/

#include "bplus_tree.h"
#include <iostream>

BplusTree::BplusTree(char *file_path, int index_type, int field_types, bool force_empty) {
    set_file_path_and_fd(file_path);
    metadata = NULL;
    if (!force_empty) {
        void* raw_metadata = FileMapper::map(METADATA_POSITION);
        if (!is_metadata_exist(metadata) ||
            get_file_size_by_path(file_path) < 2*PAGE_SIZE) {
            throw BplusTreeException("file is not a valid b+tree file");
        }
        set_record_type(
                ((Metadata*)raw_metadata)->index_type,
                ((Metadata*)raw_metadata)->field_types);
        unmap(raw_metadata);
    }
    if (force_empty) {
        set_record_type(index_type, field_types);
        init_from_empty();
    }
    Metadata* metadata = (Metadata*) FileMapper::map(METADATA_POSITION);
    if (force_empty) {
        //如果强制清空，那么就需要重新设置元数据中的索引类型和字段类型
        metadata->index_type = index_type;
        metadata->field_types = field_types;
    }
    sync(metadata);
    set_metadata(metadata);
    uchar* file_pages_manager_root = (uchar*) FileMapper::map(ROOT_FILE_PAGES_MANAGER_POSITION);
    set_file_pages_manager_root(file_pages_manager_root);
    iter = NULL;
}


BplusTree::~BplusTree() {
    Record::metadatas.erase(internal_node_type);
    Record::metadatas.erase(leaf_node_type);
    sync(metadata);
}

void BplusTree::set_record_type(int index_types, int field_types) {
    internal_node_type = Record::metadatas.size();
    Record::metadatas[internal_node_type] = RecordMetadata(internal_node_type, index_types, INT_TYPE, INDEX_TYPE);

    leaf_node_type = Record::metadatas.size();
    Record::metadatas[leaf_node_type] = RecordMetadata(leaf_node_type, index_types, field_types, LEAF_TYPE);
}


void BplusTree::init_from_empty()
{
    trunc(PAGE_SIZE*10);

    // init default meta
    Metadata* metadata = (Metadata*) FileMapper::map(METADATA_POSITION);
    memset(metadata, 0, sizeof(Metadata));

    metadata->inited = META_ID;
    metadata->value_size = Record::metadatas[leaf_node_type].raw_record_size;
    metadata->key_size = Record::metadatas[leaf_node_type].key_size;
    metadata->height = 0;
    metadata->file_size = PAGE_SIZE*10;
    metadata->root_node_pos = NOT_EXIST_NODE_POS;
    metadata->first_leaf_node_pos = NOT_EXIST_NODE_POS;
    metadata->last_leaf_node_pos = NOT_EXIST_NODE_POS;

    uchar* file_pages_manager_root =
            (uchar*) FileMapper::map(ROOT_FILE_PAGES_MANAGER_POSITION);
    set_file_pages_manager_root(file_pages_manager_root);
    set_pos(ROOT_FILE_PAGES_MANAGER_POSITION, true);
    set_pos(METADATA_POSITION, true);

    sync(metadata);
    sync(file_pages_manager_root);
}


bool BplusTree::insert(MbtKey& key, uchar* value) {
    Record *record = new Record(true, leaf_node_type);
    record->key = key;
    record->raw_record = value;

    if (metadata->root_node_pos == NOT_EXIST_NODE_POS) {
        int root_pos = get_a_page();
        TreeNode *root = map(root_pos);
        TreeNode::create_new_node(root, internal_node_type,
                                     NOT_EXIST_NODE_POS,root_pos,
                                     NOT_EXIST_NODE_POS,NOT_EXIST_NODE_POS);
        //creat leaf
        int leaf_pos = get_a_page();
        TreeNode *leaf = map(leaf_pos);
        TreeNode::create_new_node(leaf, leaf_node_type,
                                     root_pos,leaf_pos,
                                     NOT_EXIST_NODE_POS,NOT_EXIST_NODE_POS);
        metadata->root_node_pos = root_pos;
        metadata->first_leaf_node_pos = leaf_pos;
        metadata->last_leaf_node_pos = leaf_pos;
        leaf->insert(*record);
        Record *index = new Record(false, internal_node_type);
        index->key.copy(key, index->metadata->key_type);
        index->set_pos(leaf_pos);
        root->insert(*index);
        delete index;
        sync(root);
        sync(leaf);
    } else {
        int leaf_pos = search_child(key);
        TreeNode *leaf = map(leaf_pos);
        bool is_found = leaf->find(key, *record);
        if (is_found)
            return false;
        int leaf_max_child_number = TreeNode::get_max_child_num(leaf_node_type);
        int internal_max_child_number = TreeNode::get_max_child_num(internal_node_type);
        if (leaf->n == leaf_max_child_number) {
            //If size of the node exceeds, split
            int next_leaf_pos = get_a_page();
            TreeNode *next_leaf = map(next_leaf_pos);
            TreeNode::create_new_node(next_leaf,leaf->metadata->typ,
                                         leaf->parent,next_leaf_pos,leaf->next,
                                         leaf->current);
            next_leaf->node_type = LEAF_TYPE;
            leaf->next = next_leaf_pos;
            leaf->split(*next_leaf);
            metadata->last_leaf_node_pos = next_leaf_pos;
            //determine where the record should be inserted
            if (key.lt(next_leaf->get_node_key(), Record::metadatas[leaf_node_type].key_type)) {
                leaf->insert(*record);
            } else {
                next_leaf->insert(*record);
            }
            //split new node and add index in the parent
            TreeNode *parent = map(leaf->parent);
            Record *index = new Record(false, internal_node_type);
            MbtKey new_key = next_leaf->get_node_key();
            index->key.copy(new_key, index->metadata->key_type);
            index->set_pos(next_leaf_pos);
            parent->insert(*index);
            delete index;
            sync(parent);
            sync(next_leaf);
        } else {
            leaf->insert(*record);
        }
        sync(leaf);
    }
    delete record;
    sync(metadata);
    return true;
}

int BplusTree::search_child(const MbtKey& key)  {
    if (metadata->root_node_pos == NOT_EXIST_NODE_POS) {
        return 0;
    }
    TreeNode *root = map(metadata->root_node_pos);
    int child_offset = root->upper_find(key);
    if (child_offset != 0)
        child_offset -= 1;
    Record& child_record = root->get_child(child_offset);
    TreeNode *child = map(child_record.get_pos());
    while (child->node_type == INDEX_TYPE) {
        //Muliti-layer index are not included here
        child_offset = child->upper_find(key);
        delete &child_record;
        child_record = child->get_child(child_offset);
        child = map(child_record.get_pos());
        if (child_offset != 0)
            child_offset -= 1;
    }
    int child_pos = child->current;
    delete &child_record;
    return child_pos;
}

TreeNode* BplusTree::get_first_leaf() {
    int first_leaf_pos = metadata->first_leaf_node_pos;
    if (first_leaf_pos == NOT_EXIST_NODE_POS) {
        throw BplusTreeException("first leaf node not exist");
    }
    TreeNode *first_leaf = map(first_leaf_pos);
    return first_leaf;
}

TreeNode* BplusTree::get_last_leaf() {
    int last_leaf_pos = metadata->last_leaf_node_pos;
    if (last_leaf_pos == NOT_EXIST_NODE_POS) {
        throw BplusTreeException("last leaf node not exist");
    }
    TreeNode *last_leaf = map(last_leaf_pos);
    return last_leaf;
}

void BplusTree::init_search(MbtKey* left_key, MbtKey* right_key, bool need_mkp) {
    iter = new Iterator(this, left_key, right_key, need_mkp);
}

Iterator::Iterator(BplusTree *tree) {
    this->tree = tree;
    current_node = tree->get_first_leaf();
    if (current_node == NULL) {
        //If the tree is empty,state is empty
        state = EMPTY_ITER;
        return;
    }
    current_record_pos = 0;
    current_record = &(current_node->get_child(current_record_pos));
    left = NULL;
    right = NULL;
    state = IN_ITER;
}

Iterator::Iterator(BplusTree *tree, MbtKey* left, MbtKey* right, bool need_mkp) {
    this->tree = tree;
    this->need_mkp = need_mkp;
    if (this->tree->metadata->first_leaf_node_pos == NOT_EXIST_NODE_POS) {
        //If the tree is empty, state is empty
        state = EMPTY_TREE;
        this->current_node = NULL;
        this->current_record_pos = -1;
        this->current_record = NULL;
        return;
    }
    int current_node_pos;
    if(left == NULL) {
        //If left is empty, move to the first record in the first treenode
        current_node_pos = tree->metadata->first_leaf_node_pos;
        current_node = tree->map(current_node_pos);
        current_record_pos = 0;
        current_record = &(current_node->get_child(current_record_pos));
        this->left = NULL;
    } else {
        //If left is not empty ,set state as range iteration, and set left bound first.
        this->left = new MbtKey;
        this->left->copy(*left, Record::metadatas[tree->leaf_node_type].key_type);
        current_node_pos = tree->search_child(*left);
        current_node = tree->map(current_node_pos);
        current_record_pos = current_node->lower_find(*left);
        if (current_record_pos == current_node->n) {
            //If all records are less than left, state is empty
            current_record = NULL;
            state = EMPTY_ITER;
            if(right == NULL) {
                this->right = NULL;
            } else {
                this->right = new MbtKey;
                this->right->copy(*right, Record::metadatas[tree->leaf_node_type].key_type);
            }
            return;
        }
        current_record = &(current_node->get_child(current_record_pos));
    }
    if(right == NULL) {
        //If left is empty , don't set right bound
        this->right = NULL;
    } else {
        //If left is not empty ,set right bound
        this->right = new MbtKey;
        this->right->copy(*right, Record::metadatas[tree->leaf_node_type].key_type);
        if (current_record->key.gt(*right, Record::metadatas[tree->leaf_node_type].key_type)) {
            //if the first record are greater than right, state is empty
            state = EMPTY_ITER;
            return;
        }
    }
    state = IN_ITER;
}

Record* Iterator::next() {
    //if empty or tail,return NULL
    if (state == EMPTY_TREE || state == EMPTY_ITER || state == AFTER_LAST) {
        return NULL;
    }
    if (current_record_pos == current_node->n-1) {
        //if this node is traversed, need jump to the next node
        if(current_node->next != NOT_EXIST_NODE_POS) {
            //if next node exists,jump to the next node
            current_node = tree->map(current_node->next);
            current_record_pos = 0;
            Record *record = &(current_node->get_child(0));
            current_record = record;
            if (right != NULL && record->key.gt(*right, Record::metadatas[tree->leaf_node_type].key_type)) {
                //if range has maximum limit，and the first record of next node is greater than right，it means we reaches the tail，end
                state = AFTER_LAST;
                //the iterator reaches the next place
                return NULL;
            } else {
                return record;
            }
        } else {
            //if next node doesn't exist，it means the tail
            state = AFTER_LAST;
            current_record = NULL;
            current_record_pos += 1;
            return NULL;
        }
    } else {
        //if this node still has records, go to the next record
        current_record_pos += 1;
        Record *record = &(current_node->get_child(current_record_pos));
        current_record = record;
        if (right != NULL && record->key.gt(*right, Record::metadatas[tree->leaf_node_type].key_type)) {
            //if range has maximum limit，and the first record of next node is greater than right，it means we reaches the tail，end
            state = AFTER_LAST;
            return NULL;
        } else {
            return record;
        }
    }
}


Record* Iterator::prev() {
    //if the tree is empty or is the head, return null
    if (state == EMPTY_TREE || state == EMPTY_ITER || state == BEFORE_FIRST) {
        return NULL;
    }
    if (current_record_pos == 0) {
        //if this node eis traversed, jump to the previous node
        if(current_node->prev != NOT_EXIST_NODE_POS) {
            //if previous node exists, jump to the previous node
            current_node = tree->map(current_node->prev);
            current_record_pos = 0;
            Record *record = &(current_node->get_child(0));
            current_record = record;
            if (left != NULL && record->key.lt(*left, Record::metadatas[tree->leaf_node_type].key_type)) {
                //if range has minimum limit，and the first record of next node is less than left，it means we reaches the head，end
                state = BEFORE_FIRST;
                return NULL;
            } else {
                return record;
            }
        } else {
            //if the previous node doen't exist, it means we reaches the head
            state = BEFORE_FIRST;
            current_record = NULL;
            current_record_pos -= 1;
            return NULL;
        }
    } else {
        //if this node has record, go to the previous record
        current_record_pos -= 1;
        Record *record = &(current_node->get_child(current_record_pos));
        current_record = record;
        if (left != NULL && record->key.lt(*left, Record::metadatas[tree->leaf_node_type].key_type)) {
            //if range has minimum limit，and the first record of next node is less than left，it means we reaches the head，end
            state = BEFORE_FIRST;
            return NULL;
        } else {
            return record;
        }
    }
}


int* Iterator::get_last_and_after_last_record() {
    int* records = new int[4];
    if (state == EMPTY_TREE) {
        records[0] = records[1] = records[2] = records[3] = -1;
        return records;
    }
    if(right == NULL) {
        records[2] = -1;
        records[3] = -1;
        records[0] = tree->metadata->last_leaf_node_pos;
        TreeNode* node = tree->map(records[0]);
        records[1] = node->n-1;
        return records;
    }
    int node_pos = tree->search_child(*right);
    TreeNode* node = tree->map(node_pos);
    int record_pos =node->upper_find(*right);
    if (record_pos == node->n) {
        //if all greater than right，no value is bigger than right bound
        if(node->current == tree->metadata->last_leaf_node_pos) {
            //if lasy node， so last is the last record of this node
            //after last not exists
            records[2] = -1;
            records[3] = -1;
            records[0] = node->current;
            records[1] = record_pos - 1;
        } else {
            //if not last node，so last is the first record of next node
            records[0] = node->current;
            records[1] = record_pos - 1;
            TreeNode* next_node = tree->map(node->next);
            records[2] = next_node->current;
            records[3] = 0;
        }
    } else {
        records[2] = node->current;
        records[3] = record_pos;
        if(record_pos == 0) {
            //if after last is the first record of a node，so last should be in the previous node
            if(node->prev == NOT_EXIST_NODE_POS) {
                //if previous node not exists， last not exists
                records[0] = -1;
                records[1] = -1;
            } else {
                //if previous node exists， so last is the last record of previous node
                TreeNode* prev_node = tree->map(node->prev);
                records[0] = prev_node->current;
                records[1] = prev_node->n-1;
            }
        } else {
            //if after last isn't the first record of a node, so last is previous record of record_pos
            records[0] = node->current;
            records[1] = record_pos - 1;
        }
    }
    return records;
}

int* Iterator::get_before_first_and_first_record() {
    int* records = new int[4];
    if (state == EMPTY_TREE) {
        records[0] = records[1] = records[2] = records[3] = -1;
        return records;
    }
    if(left == NULL) {
        records[0] = -1;
        records[1] = -1;
        records[2] = tree->metadata->first_leaf_node_pos;
        records[3] = 0;
        return records;
    }
    int node_pos = tree->search_child(*left);
    TreeNode* node = tree->map(node_pos);
    int record_pos = node->lower_find(*left);
    if (record_pos == 0) {
        // if find the first record of a node
        Record &record = node->get_child(record_pos);
        if (record.key.lt(*left, Record::metadatas[tree->leaf_node_type].key_type)) {
            //if the first is less than left，it is before_first（mbt first record）
            records[0] = node->current;
            records[1] = record_pos;
            if (node->n > 1) {
                //more than one record in node,first is the second
                records[2] = node->current;
                records[3] = 1;
            } else {
                //only one record in node， first is the first record of next node
                if (node->next == NOT_EXIST_NODE_POS) {
                    records[2] = -1;
                    records[3] = -1;
                } else {
                    TreeNode *next_node = tree->map(node->next);
                    records[2] = next_node->current;
                    records[3] = 0;
                }
            }
        } else {
            //if the first is greater than or equal to left，it is first
            records[2] = node->current;
            records[3] = record_pos;
            if (node->prev == NOT_EXIST_NODE_POS) {
                //if previous node not exists，left doesn't have smaller record ，before first not exists
                records[0] = -1;
                records[1] = -1;
            } else {
                //if previous node exists，left has smaller record
                TreeNode *prev_node = tree->map(node->prev);
                records[0] = prev_node->current;
                records[1] = prev_node->n - 1;
            }
        }
    } else if(record_pos == node->n) {
        //if all greater than left， before first is the last record of this node
        //first is the next record of left
        records[0] = node->current;
        records[1] = node->n - 1;
        if (node->current == tree->metadata->last_leaf_node_pos) {
            //if last ndoe，first not exists
            records[2] = -1;
            records[3] = -1;
        } else {
            //iff not last node， first is the first record of next node
            TreeNode *next_node = tree->map(node->next);
            records[2] = next_node->current;
            records[3] = 0;
        }
    }
    else {
        //get previous record
        records[0] = node->current;
        records[1] = record_pos -1;
        records[2] = node->current;
        records[3] = record_pos;
    }
    return records;
}

std::string BplusTree::generate_merkle_proof(int node_pos, int record_pos) {
    std::string proof;
    TreeNode* node = map(node_pos);
    Record& record = node->get_child(record_pos);
    proof += int_to_string(record.key.to_int()) + "\n";
    proof += record.encode_to_hex() + "\n";
    Hash *hash = &(node->get_hash(record_pos));
    proof += hash->to_hex() + "\n";
    delete hash;
    for(int i=0; i<node->n; i++) {
        hash = &(node->get_hash(i));
        proof += hash->to_hex() + " ";
        delete hash;
    }
    proof[proof.size()-1] = '\n';
    hash = new Hash(true);
    hash->hash = node->hash;
    proof += hash->to_hex() + "\n";
    delete hash;
    int parent_node_pos = node->parent;
    while(parent_node_pos != NOT_EXIST_NODE_POS) {
        TreeNode* parent_node = map(node->parent);
        for(int i=0; i<parent_node->n; i++) {
            hash = &(parent_node->get_hash(i));
            proof += hash->to_hex() + " ";
            delete hash;
        }
        proof[proof.length()-1] = '\n';
        Hash* hash = new Hash(true);
        hash->hash = parent_node->hash;
        proof += hash->to_hex() + "\n";
        delete hash;
        parent_node_pos = parent_node->parent;
    }
    return proof;
}

std::string Iterator::generate_range_merkle_proof() {
    std::string result;
    std::string mkps[4];
    if (left != NULL) {
        result += int_to_string(left->to_int()) + "\n";
    } else {
        result += "NULL\n";
    }
    if (right != NULL) {
        result += int_to_string(right->to_int()) + "\n";
    } else {
        result += "NULL\n";
    }
    result += "\n";

    int* before_first_and_first = get_before_first_and_first_record();
    int* last_and_after_last = get_last_and_after_last_record();
    if (before_first_and_first[0] != -1) {
        mkps[0] += tree->generate_merkle_proof(before_first_and_first[0], before_first_and_first[1]);
    } else {
        mkps[0] += "NULL\n";
    }
    if (before_first_and_first[2] != -1) {
        mkps[1] += tree->generate_merkle_proof(before_first_and_first[2], before_first_and_first[3]);
    } else {
        mkps[1] += "NULL\n";
    }
    if (last_and_after_last[0] != -1) {
        mkps[2] += tree->generate_merkle_proof(last_and_after_last[0], last_and_after_last[1]);
    } else {
        mkps[2] += "NULL\n";
    }
    if (last_and_after_last[2] != -1) {
        mkps[3] += tree->generate_merkle_proof(last_and_after_last[2], last_and_after_last[3]);
    } else {
        mkps[3] += "NULL\n";
    }
    delete [] before_first_and_first;
    delete [] last_and_after_last;
    for(int i=0; i<4; i++) {
        result += mkps[i] + '\n';
    }
    return result;
}


std::string Iterator::generate_subtree_merkle_proof() {
    if (tree->metadata->root_node_pos == NOT_EXIST_NODE_POS) {
        return 0;
    }
    std::vector<TreeNode*> subtree_nodes;
    TreeNode *root = tree->map(tree->metadata->root_node_pos);
    subtree_nodes.push_back(root);
    int left_offset, right_offset;
    if (left == NULL) {
        left_offset = 0;
    } else {
        left_offset = root->upper_find(*left);
        if (left_offset != 0) {
            left_offset -= 1;
        }
    }
    if (right == NULL) {
        right_offset = root->n - 1;
    } else {
        right_offset = root->upper_find(*right);
        if (right_offset != left_offset) {
            right_offset -= 1;
        }
    }
    if (left_offset > right_offset) {
        return "NULL\n";
    }
    for (int i=left_offset; i<=right_offset; i++) {
        Record& child_record = root->get_child(i);
        TreeNode *child = tree->map(child_record.get_pos());
        subtree_nodes.push_back(child);
    }
    int level_pos = 1;

    while (subtree_nodes[subtree_nodes.size()-1]->node_type == INDEX_TYPE) {
        //Multi-layer index node are not included here
        int i = level_pos;
        level_pos = subtree_nodes.size();
        for(; i<subtree_nodes.size(); i++) {
            TreeNode *node = subtree_nodes[i];
            if (left == NULL) {
                left_offset = 0;
            } else {
                left_offset = root->lower_find(*left);
            }
            if (right == NULL) {
                right_offset = root->n - 1;
            } else {
                right_offset = root->upper_find(*right);
                if (right_offset != 0) {
                    right_offset -= 1;
                }
            }
            if (left_offset > right_offset) {
                return "NULL\n";
            }
            for (int i=left_offset; i<=right_offset; i++) {
                Record& child_record = root->get_child(i);
                TreeNode *child = tree->map(child_record.get_pos());
                subtree_nodes.push_back(child);
            }
        }
    }

    std::string stmkp;
    for (int i=0; i<subtree_nodes.size(); i++) {
        stmkp += uchars_to_hex(subtree_nodes[i]->hash,
                                  SHA256_DIGEST_LENGTH) + "\n";
        for (int j=0; j<subtree_nodes[i]->n; j++) {
            stmkp += subtree_nodes[i]->get_hash(j).to_hex() + " ";
        }
        stmkp[stmkp.length()-1] = '\n';
    }
    return stmkp;
}


/**
 * @brief update mbtree hash
 * recursively update
 * @param root_pos mbtree or subtree root
 * @return
 */
Hash& BplusTree::update_hash(int root_pos) {
    if(root_pos == NOT_EXIST_NODE_POS) {
        return Hash::generate_empty_hash();
    }
    TreeNode *root = map(root_pos);
    if (root->node_type == INDEX_TYPE) {
        // If index node，recursively update children hash，update hash list
        for (int i=0; i<root->n; i++) {
            int child_pos = root->get_child(i).get_pos();
            root->set_hash(update_hash(child_pos), i);
        }
    } else {
        //If leaf node，update hash list
        for (int i=0; i<root->n; i++) {
            Record &record = root->get_child(i);
            root->set_hash(record.hash(), i);
        }
    }
    Hash& root_hash = root->update_hash();
    if (root_pos == metadata->root_node_pos) {
        // If mbtree root，update mbtree hash
        memcpy(metadata->root_hash, root_hash.hash, SHA256_DIGEST_LENGTH);
    }
    return root_hash;
}

bool BplusTree::verify_range_merkle_proof(std::string& range_mkp, Hash& root_hash) {
    std::vector<std::string> result;
    std::string delimiter = "\n\n"; // 2 "\n" as delimiter
    size_t pos = 0;
    size_t prevPos = 0;

    while ((pos = range_mkp.find(delimiter, prevPos)) != std::string::npos) {
        std::string token = range_mkp.substr(prevPos, pos - prevPos);
        result.push_back(token);
        prevPos = pos + delimiter.length();
    }

    // deal with string after the last delimiter
    std::string lastToken = range_mkp.substr(prevPos);
    result.push_back(lastToken);

    std::vector<std::string> keys = string_split(result[0], '\n');
    int left_key, right_key;
    bool left_key_exist = false;
    bool right_key_exist = false;
    if(keys[0] != "NULL") {
        left_key = std::atoi(keys[0].c_str());
        left_key_exist = true;
    }
    if(keys[1] != "NULL") {
        right_key = std::atoi(keys[1].c_str());
        right_key_exist = true;
    }
    MerkleProof *mkps[4];
    for(int i=0; i<4; i++) {
        mkps[i] = NULL;
    }
    if(result[1] != "NULL") {
        mkps[0] = MerkleProof::from_string(result[1]);
    }
    if(result[2] != "NULL") {
        mkps[1] = MerkleProof::from_string(result[2]);
    }
    if(result[3] != "NULL") {
        mkps[2] = MerkleProof::from_string(result[3]);
    }
    if(result[4] != "NULL") {
        mkps[3] = MerkleProof::from_string(result[4]);
    }
    for(int i=0; i<4; i++) {
        if(mkps[i] == NULL) {
            continue;
        }
        if(!mkps[i]->verify())
            return false;
        if(root_hash.to_hex() != mkps[i]->hashes[mkps[i]->hashes.size()-1]) {
            return false;
        }
        delete mkps[i];
    }
    //need check key
    return true;
}

bool BplusTree::verify_subtree_merkle_proof(std::string& range_mkp, Hash& root_hash) {
    std::vector<std::string> raw_hashes = string_split(range_mkp, '\n');
    if(raw_hashes.size()%2 != 0)
        return false;
    if (raw_hashes[0] != root_hash.to_hex()) {
        return false;
    }
    for (int i=0; i<raw_hashes.size()/2; i++) {
        Hash* hash = &Hash::sha256(raw_hashes[i*2+1]);
        if(raw_hashes[i*2] != hash->to_hex()) {
            return false;
        }
    }
    return true;
}




