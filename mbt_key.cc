/*
  Copyright (C) BABEC. All rights reserved.
  Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
  and Privacy Computing. All rights reserved.
  
  SPDX-License-Identifier: Apache-2.0
*/

#include "mbt_key.h"


int MbtKey::to_int() const {
    return uchars_to_int(raw_key);
}

int MbtKey::cmp(const MbtKey &key, int typ) const {
    int res;
    if(typ == INT_TYPE) {
        int x = to_int();
        int y = key.to_int();
        if (x < y)
            res = -1;
        else if (x > y)
            res = 1;
        else
            res = 0;
    } else if(typ == STR20_TYPE) {
        //TODO
        throw MbtKeyException("str20 type");
    } else {
        throw MbtKeyException("invalid type");
    }
    return res;
}

bool MbtKey::lt(const MbtKey &key, int typ) const {
    if (cmp(key, typ) < 0)
        return true;
    return false;
}

bool MbtKey::lte(const MbtKey &key, int typ) const {
    if (cmp(key, typ) <=0)
        return true;
    return false;
}

bool MbtKey::gt(const MbtKey &key, int typ) const {
    if (cmp(key, typ) > 0)
        return true;
    return false;
}

bool MbtKey::gte(const MbtKey &key, int typ) const {
    if (cmp(key, typ) >= 0)
        return true;
    return false;
}

bool MbtKey::eq(const MbtKey &key, int typ) const {
    if (cmp(key, typ) == 0)
        return true;
    return false;
}

bool MbtKey::neq(const MbtKey &key, int typ) const {
    if (cmp(key, typ) != 0)
        return true;
    return false;
}

void MbtKey::copy(MbtKey &key, int typ) {
    if(typ == INT_TYPE) {
        if (raw_key == NULL) {
            raw_key = new uchar[INT_SIZE];
        }
        memcpy(raw_key, key.raw_key, INT_SIZE);
    } else if(typ == STR20_TYPE) {
        //TODO
        throw MbtKeyException("str20 type");
    } else {
        throw MbtKeyException("invalid type");
    }
}

MbtKey MbtKey::from_int(int x) {
    MbtKey key;
    key.raw_key = new uchar[INT_SIZE];
    int_to_uchars(x, key.raw_key);
    return key;
}
