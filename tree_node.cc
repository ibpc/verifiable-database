/*
  Copyright (C) BABEC. All rights reserved.
  Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
  and Privacy Computing. All rights reserved.
  
  SPDX-License-Identifier: Apache-2.0
*/

#include <iostream>
#include <algorithm>
#include <map>

#include "tree_node.h"

std::map<int, RecordMetadata> Record::metadatas;


void TreeNode::create_new_node(TreeNode *node, int typ, int parent,
                                    int current, int next, int prev) {
    memset((void*) node, 0, PAGE_SIZE);

    node->parent = parent;
    node->current = current;
    node->next = next;
    node->prev = prev;
    memset(node->hash, 0, SHA256_DIGEST_LENGTH);
    node->metadata = &Record::metadatas[typ];
    node->node_type = node->metadata->node_type;
}


int TreeNode::get_max_child_num(int typ) {
    return Record::metadatas[typ].max_record_num;
}


int TreeNode::lower_find(const MbtKey &key) const {
    int pos = 0;
    for (; pos<n; pos++) {
        Record& record = get_child(pos);
        if (key.lte(record.key, record.metadata->key_type)) {
            delete &record;
            break;
        }
        delete &record;
    }
    return pos;
}


int TreeNode::upper_find(const MbtKey &key) const {
    int pos = 0;
    for (; pos<n; pos++) {
        Record& record = get_child(pos);
//        DBUG_PRINT("info", ("TreeNode::upper_find: current key %d, key %d",
//                record.key.to_int(), key.to_int()));
        if (key.lt(record.key, record.metadata->key_type)) {
            delete &record;
            break;
        }
        delete &record;
    }
    return pos;
}

void TreeNode::set_child(Record &record, int pos) {
    Record& old_record = get_child(pos);
    old_record.copy(record);
    delete &old_record;
    Hash& hash = record.hash();
    set_hash(hash, pos);
}

int TreeNode::compute_key_offset(int pos) const {
    return NODE_PREFIX_SIZE +
           pos*(SHA256_DIGEST_LENGTH
                +metadata->raw_record_size
                +metadata->key_size);
}

int TreeNode::compute_record_offset(int pos) const {
    return NODE_PREFIX_SIZE +
           pos*(SHA256_DIGEST_LENGTH
                +metadata->raw_record_size
                +metadata->key_size) +
              metadata->key_size;
}

int TreeNode::compute_hash_offset(int pos) const {
    return NODE_PREFIX_SIZE +
           (pos+1)*(metadata->raw_record_size
                    +metadata->key_size) +
           pos*SHA256_DIGEST_LENGTH;
}


Record& TreeNode::get_child(int pos) const {
    Record* record = new Record(true, metadata->typ);
    record->key.raw_key = ((uchar*) this + compute_key_offset(pos));
    record->raw_record = ((uchar*) this + compute_record_offset(pos));
    return *record;
}


void TreeNode::set_hash(Hash &new_hash, int pos) {
    Hash& hash = get_hash(pos);
    memcpy(hash.hash, new_hash.hash, SHA256_DIGEST_LENGTH);
    delete &hash;
}


Hash & TreeNode::get_hash(int pos) {
    Hash *hash = new Hash(true);
    hash->hash = ((uchar*) this + compute_hash_offset(pos));
    return *hash;
}


MbtKey TreeNode::get_node_key() {
    MbtKey key;
    key.raw_key = (uchar*) this + NODE_PREFIX_SIZE;
    return key;
}

bool TreeNode::find(const MbtKey &key, Record& record) const {
    int pos = lower_find(key);
    if (pos >= n)
        return false;
    Record& new_record = get_child(pos);
    if (key.eq(new_record.key, metadata->key_type)) {
        record.key = new_record.key;
        record.raw_record = new_record.raw_record;
        delete &new_record;
        return true;
    }
    delete &new_record;
    return false;
}


void TreeNode::insert(Record &record) {
    int pos = 0;
    if (n != 0) {
        //if not empty node, shoulde determine where to insert
        MbtKey node_key = get_node_key();
        if (node_key.lte(record.key, metadata->key_type)) {
            pos = lower_find(record.key);
        }
        //determine whether the inserting position is valid
        if (pos == -1) {
            throw TreeNodeException("insert pos == -1");
        }
        if (pos > n) {
            throw TreeNodeException("insert pos > n");
        }
        if (n > metadata->max_record_num) {
            throw TreeNodeException("insert node is full");
        }
        //puut backward the records after the inserted record 
        uchar* start = ((uchar*) this + NODE_PREFIX_SIZE +
                pos*(SHA256_DIGEST_LENGTH
                     +metadata->raw_record_size
                     +metadata->key_size));
        uchar* end = ((uchar*) this + NODE_PREFIX_SIZE +
                      n*(SHA256_DIGEST_LENGTH
                           +metadata->raw_record_size
                           +metadata->key_size));
        uchar* new_end = ((uchar*) this + NODE_PREFIX_SIZE +
                             (n+1)*(SHA256_DIGEST_LENGTH
                                +metadata->raw_record_size
                                +metadata->key_size));
        std::copy_backward(start, end, new_end);
    } //if empty node insert in position 0
    set_child(record, pos);
    n++;
}


void TreeNode::split(TreeNode &next_node) {

    if (n < 2) {
        //Only node with more than two records can split
        std::string err_msg = "splited child number < 2";
        throw TreeNodeException(err_msg);
    }
    //split position is the middle
    int split_pos = n / 2;
    //copy the records after split tonext_node
    uchar* start = ((uchar*) this + compute_key_offset(split_pos));
    uchar* end = ((uchar*) this + compute_key_offset(n));
    uchar* start_next = (uchar*) (&next_node) + NODE_PREFIX_SIZE;

    std::copy(start, end, start_next);
    //update the number of records in the node
    next_node.n = n - n / 2;
    n = n / 2;

}


Hash& TreeNode::update_hash() {
    Hash &child_hash = get_hash(0);
    std::string input = child_hash.to_hex();
    delete &child_hash;
    for(int i=1; i<n; i++) {
        Hash &child_hash = get_hash(i);
        input += " " + child_hash.to_hex();
        delete &child_hash;
    }
    Hash& new_hash = Hash::sha256(input);
    memcpy(hash, new_hash.hash, SHA256_DIGEST_LENGTH);
    return new_hash;
}

