/*
  Copyright (C) BABEC. All rights reserved.
  Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
  and Privacy Computing. All rights reserved.

  SPDX-License-Identifier: Apache-2.0
*/

/*
 * @file VerifiableDB.go
 * @brief smart contract with MBTREE storage engine
 *
 * This smart contract supports ChainMaker v2.3.1
 *
 * Dependencies:
 *  - chainmaker-sdk-go/v2
 */

package main

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"

	"chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sandbox"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

type FactContract struct {
}

// Proof
type Proof struct {
	SearchId  string `json:"SearchId"`
	ProofId   string `json:"ProofId"`
	Hash      string `json:"Hash"`
	RawString string `json:"RawString"`
}

// Create Proof
func NewProof(SearchId string, ProofId string, Hash string, RawString string) *Proof {
	Proof := &Proof{
		SearchId:  SearchId,
		ProofId:   ProofId,
		Hash:      Hash,
		RawString: RawString,
	}
	return Proof
}

// Search Result
type Result struct {
	SearchId string `json:"SearchId"`
	ResultId string `json:"ResultId"`
	K        string `json:"K"`
	SK       string `json:"SK"`
	Data     string `json:"Data"`
	Valid    bool   `json:"Valid"`
}

// Create Search Result
func NewResult(SearchId string, ResultId string, K string, SK string, Data string, Valid bool) *Result {
	Result := &Result{
		SearchId: SearchId,
		ResultId: ResultId,
		K:        K,
		SK:       SK,
		Data:     Data,
		Valid:    Valid,
	}
	return Result
}

// Search Information
type SearchInfo struct {
	SearchId   int    `json:"SearchId"`
	Time       string `json:"Time"`
	Table      string `json:"Table"`
	SK         string `json:"SK"`
	EK         string `json:"EK"`
	SSK        string `json:"SSK"`
	SEK        string `json:"SEK"`
	Status     string `json:"Status"`
	Merkle     string `json:"Merkle"`
	ProofNum   int    `json:"ProofNum"`
	ResultNum  int    `json:"ResultNum"`
	ProofLayer int    `json:"ProofLayer"`
	LeftKey    string `json:"LeftKey"`
	LeftValue  string `json:"LeftValue"`
	StartKey   string `json:"StartKey"`
	StartValue string `json:"StartValue"`
	EndKey     string `json:"EndKey"`
	EndValue   string `json:"EndValue"`
	RightKey   string `json:"RightKey"`
	RightValue string `json:"RightValue"`
}

// Create Search Information
func NewSearchInfo(SearchId int, Time string, Table string, SK string, EK string, SSK string, SEK string,
	Status string, Merkle string, ProofNum int, ResultNum int, ProofLayer int, LeftKey string, LeftValue string,
	StartKey string, StartValue string, EndKey string, EndValue string, RightKey string, RightValue string) *SearchInfo {
	SearchInfo := &SearchInfo{
		SearchId:   SearchId,
		Time:       Time,
		Table:      Table,
		SK:         SK,
		EK:         EK,
		SSK:        SSK,
		SEK:        SEK,
		Status:     Status,
		Merkle:     Merkle,
		ProofNum:   ProofNum,
		ResultNum:  ResultNum,
		ProofLayer: ProofLayer,
		LeftKey:    LeftKey,
		LeftValue:  LeftValue,
		StartKey:   StartKey,
		StartValue: StartValue,
		EndKey:     EndKey,
		EndValue:   EndValue,
		RightKey:   RightKey,
		RightValue: RightValue,
	}
	return SearchInfo
}

// Time-Table Hash
type TTHash struct {
	Time  string `json:"Time"`
	Table string `json:"Table"`
	Hash  string `json:"Hash"`
}

// Create Time-Table Hash
func NewTTHash(Time string, Table string, Hash string) *TTHash {
	TTHash := &TTHash{
		Time:  Time,
		Table: Table,
		Hash:  Hash,
	}
	return TTHash
}

// Search Identifier
type Search_Id struct {
	Name     string `json:"Name"`
	SearchId string `json:"SearchId"`
}

// Create Search Identifier
func NewSearch_Id(Name string, SearchId string) *Search_Id {
	Search_Id := &Search_Id{
		Name:     Name,
		SearchId: SearchId,
	}
	return Search_Id
}

// Deploy Contract
func (f *FactContract) InitContract() protogo.Response {
	// Construct New Search Identifier
	search_id := NewSearch_Id("SearchId", strconv.Itoa(1))

	// Serialize
	factBytes, err := json.Marshal(search_id)
	if err != nil {
		return sdk.Error(fmt.Sprintf("marshal searchid failed, err: %s", err))
	}
	// Store  Search Identifier
	err = sdk.Instance.PutStateByte("SearchIds", search_id.Name, factBytes)
	if err != nil {
		return sdk.Error("fail to save SearchIds bytes")
	}
	return sdk.Success([]byte("Init contract success"))
}

// Upgrade Contract
func (f *FactContract) UpgradeContract() protogo.Response {
	return sdk.Success([]byte("Upgrade contract success"))
}

// Contract Interface
func (f *FactContract) InvokeContract(method string) protogo.Response {
	switch method {
	case "update_offchain_status":
		return f.update_offchain_status()
	case "get_hash_by_time_table":
		return f.get_hash_by_time_table()
	case "search_from_offchain":
		return f.search_from_offchain()
	case "upload_from_offchain":
		return f.upload_from_offchain()
	case "update_range_proof":
		return f.update_range_proof()
	case "update_merkle_proof":
		return f.update_merkle_proof()
	case "update_result":
		return f.update_result()
	case "result_and_range_proof_check":
		return f.result_and_range_proof_check()
	case "merkle_proof_check":
		return f.merkle_proof_check()
	case "get_status_and_result":
		return f.get_status_and_result()
	default:
		return sdk.Error("invalid method")
	}
}

// Update TTHash
func (f *FactContract) update_offchain_status() protogo.Response {
	params := sdk.Instance.GetArgs()

	// Get Parameters
	Time := string(params["Time"])
	Table := string(params["Table"])
	Hash := string(params["Hash"])
	// Construct TTHash
	TTHash := NewTTHash(Time, Table, Hash)

	// Serialize
	factBytes, err := json.Marshal(TTHash)
	if err != nil {
		return sdk.Error(fmt.Sprintf("marshal TTHash failed, err: %s", err))
	}

	// Emit Event
	sdk.Instance.EmitEvent("new_status_update", []string{TTHash.Time, TTHash.Table, TTHash.Hash})

	// Store TTHash
	err = sdk.Instance.PutStateByte("TTHashes", TTHash.Time+TTHash.Table, factBytes)
	if err != nil {
		return sdk.Error("fail to save fact bytes")
	}

	// Write Log
	sdk.Instance.Infof("[save] Time=" + TTHash.Time)
	sdk.Instance.Infof("[save] Table=" + TTHash.Table)
	sdk.Instance.Infof("[save] Hash=" + TTHash.Hash)

	// Return the result
	return sdk.Success([]byte(TTHash.Time + " " + TTHash.Table + " " + TTHash.Hash))
}

// Get Hash by Time and Table
func (f *FactContract) get_hash_by_time_table() protogo.Response {
	// Get Parameters
	Time := string(sdk.Instance.GetArgs()["Time"])
	Table := string(sdk.Instance.GetArgs()["Table"])
	// Get Hash by Time and Table
	result, err := sdk.Instance.GetStateByte("TTHashes", Time+Table)
	if err != nil {
		return sdk.Error("failed to call get_state")
	}

	// Unserialize
	var tthash TTHash
	if err = json.Unmarshal(result, &tthash); err != nil {
		return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
	}

	// Write Log
	sdk.Instance.Infof("[get_hash_by_time_table] Time=" + tthash.Time)
	sdk.Instance.Infof("[get_hash_by_time_table] Table=" + tthash.Table)
	sdk.Instance.Infof("[get_hash_by_time_table] Hash=" + tthash.Hash)

	// Return Result
	return sdk.Success([]byte(tthash.Hash))
}

// Search From User
func (f *FactContract) search_from_offchain() protogo.Response {
	params := sdk.Instance.GetArgs()

	// Get Parameters
	Time := string(params["Time"])
	Table := string(params["Table"])
	SK := string(params["SK"])
	EK := string(params["EK"])
	SSK := string(params["SSK"])
	SEK := string(params["SEK"])
	Status := "Not Uploaded"
	Merkle := "Not Uploaded"
	ProofNum := 0
	ResultNum := 0
	ProofLayer := 0
	LeftKey := ""
	LeftValue := ""
	StartKey := ""
	StartValue := ""
	EndKey := ""
	EndValue := ""
	RightKey := ""
	RightValue := ""

	// Get Search Identifier
	result, err := sdk.Instance.GetStateByte("SearchIds", "SearchId")
	if err != nil {
		return sdk.Error("failed to call get_state")
	}

	// Unserialize
	var searchid Search_Id
	if err = json.Unmarshal(result, &searchid); err != nil {
		return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
	}

	search_id, err := strconv.Atoi(searchid.SearchId)

	// Construct Search Information
	searchinfo := NewSearchInfo(search_id, Time, Table, SK, EK, SSK, SEK, Status, Merkle, ProofNum, ResultNum,
		ProofLayer, LeftKey, LeftValue, StartKey, StartValue, EndKey, EndValue, RightKey, RightValue)

	// Serialize
	factBytes, err := json.Marshal(searchinfo)
	if err != nil {
		return sdk.Error(fmt.Sprintf("marshal SearchInfo failed, err: %s", err))
	}

	// Emit Search Event
	sdk.Instance.EmitEvent("BeginSearch", []string{strconv.Itoa(searchinfo.SearchId), searchinfo.Time,
		searchinfo.Table, searchinfo.SK, searchinfo.EK, searchinfo.SSK, searchinfo.SEK})

	// Store Search Information
	err = sdk.Instance.PutStateByte("SearchInfos", strconv.Itoa(searchinfo.SearchId), factBytes)
	if err != nil {
		return sdk.Error("fail to save SearchInfos bytes")
	}

	// Write Log
	sdk.Instance.Infof("[search] SearchId=" + strconv.Itoa(searchinfo.SearchId))
	sdk.Instance.Infof("[search] Time=" + searchinfo.Time)
	sdk.Instance.Infof("[search] Table=" + searchinfo.Table)
	sdk.Instance.Infof("[search] SK=" + searchinfo.SK)
	sdk.Instance.Infof("[search] EK=" + searchinfo.EK)
	sdk.Instance.Infof("[search] SSK=" + searchinfo.SSK)
	sdk.Instance.Infof("[search] SEK=" + searchinfo.SEK)
	//Update Search Identifier
	search_id = search_id + 1

	// Construct Search Identifier
	si := NewSearch_Id("SearchId", strconv.Itoa(search_id))

	// Searilize
	fB, err := json.Marshal(si)
	if err != nil {
		return sdk.Error(fmt.Sprintf("marshal searchid failed, err: %s", err))
	}

	// Store Search Identifier
	err = sdk.Instance.PutStateByte("SearchIds", si.Name, fB)
	if err != nil {
		return sdk.Error("fail to save SearchIds bytes")
	}

	// Return Result
	return sdk.Success([]byte(strconv.Itoa(search_id - 1)))
}

// Upload Search Information by Database
func (f *FactContract) upload_from_offchain() protogo.Response {
	params := sdk.Instance.GetArgs()

	// Get Parameters
	SearchId := string(params["SearchId"])
	ResultNum := string(params["ResultNum"])
	ProofLayer := string(params["ProofLayer"])
	LeftKey := string(params["LeftKey"])
	LeftValue := string(params["LeftValue"])
	StartKey := string(params["StartKey"])
	StartValue := string(params["StartValue"])
	EndKey := string(params["EndKey"])
	EndValue := string(params["EndValue"])
	RightKey := string(params["RightKey"])
	RightValue := string(params["RightValue"])

	// Get Search Information by Search Identifier
	result, err := sdk.Instance.GetStateByte("SearchInfos", SearchId)
	if err != nil {
		return sdk.Error("failed to call get_state")
	}

	// Unserialized
	var searchinfo SearchInfo
	if err = json.Unmarshal(result, &searchinfo); err != nil {
		return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
	}

	// Upload the Result of Search
	searchinfo.Status = "Uploaded But Not Checked"
	searchinfo.ResultNum, err = strconv.Atoi(ResultNum)
	searchinfo.ProofLayer, err = strconv.Atoi(ProofLayer)
	searchinfo.LeftKey = LeftKey
	searchinfo.LeftValue = LeftValue
	searchinfo.StartKey = StartKey
	searchinfo.StartValue = StartValue
	searchinfo.EndKey = EndKey
	searchinfo.EndValue = EndValue
	searchinfo.RightKey = RightKey
	searchinfo.RightValue = RightValue

	// Searialize
	factBytes, err := json.Marshal(searchinfo)
	if err != nil {
		return sdk.Error(fmt.Sprintf("marshal SearchInfo failed, err: %s", err))
	}
	rn := strconv.Itoa(searchinfo.ResultNum)
	pl := strconv.Itoa(searchinfo.ProofLayer)
	// Emit End Search Event
	sdk.Instance.EmitEvent("EndSearch", []string{strconv.Itoa(searchinfo.SearchId), searchinfo.Time,
		searchinfo.Table, searchinfo.SK, searchinfo.EK, searchinfo.SSK, searchinfo.SEK, searchinfo.Status,
		rn, pl, searchinfo.LeftKey, searchinfo.LeftValue, searchinfo.StartKey, searchinfo.StartValue,
		searchinfo.EndKey, searchinfo.EndValue, searchinfo.RightKey, searchinfo.RightValue})

	// Store Search Information
	err = sdk.Instance.PutStateByte("SearchInfos", strconv.Itoa(searchinfo.SearchId), factBytes)
	if err != nil {
		return sdk.Error("fail to save SearchInfos bytes")
	}

	// Return Result
	return sdk.Success([]byte("Result and Proof of SearchId " + SearchId + " Uploaded Successfully"))
}

// Update Range Proof
func (f *FactContract) update_range_proof() protogo.Response {
	params := sdk.Instance.GetArgs()

	SearchId := string(params["SearchId"])
	ProofId := string(params["ProofId"])
	Hash := string(params["Hash"])
	RawString := string(params["RawString"])

	// Construct Proof
	proof := NewProof(SearchId, ProofId, Hash, RawString)

	// Serialize
	factBytes, err := json.Marshal(proof)
	if err != nil {
		return sdk.Error(fmt.Sprintf("marshal proof failed, err: %s", err))
	}

	// Emit UploadProof Event
	sdk.Instance.EmitEvent("UploadProof", []string{SearchId, ProofId, proof.Hash, proof.RawString})

	// Store Range Proof
	err = sdk.Instance.PutStateByte("RangeProofs", SearchId+"-"+ProofId, factBytes)
	if err != nil {
		return sdk.Error("fail to save Proofs bytes")
	}

	// Return Result
	return sdk.Success([]byte("Uploaded Range Proof " + ProofId + " of SearchId " + SearchId + " Successfully"))
}

// Update Merkle Proof
func (f *FactContract) update_merkle_proof() protogo.Response {
	params := sdk.Instance.GetArgs()

	SearchId := string(params["SearchId"])
	ProofId := string(params["ProofId"])
	Hash := string(params["Hash"])
	RawString := string(params["RawString"])

	// Construct Proof
	proof := NewProof(SearchId, ProofId, Hash, RawString)

	// Serialize
	factBytes, err := json.Marshal(proof)
	if err != nil {
		return sdk.Error(fmt.Sprintf("marshal proof failed, err: %s", err))
	}

	// Emit UploadProof Event
	sdk.Instance.EmitEvent("UploadProof", []string{SearchId, ProofId, proof.Hash, proof.RawString})

	// Store Merkle Proof
	err = sdk.Instance.PutStateByte("MerkleProofs", SearchId+"-"+ProofId, factBytes)
	if err != nil {
		return sdk.Error("fail to save Proofs bytes")
	}

	// Return Result
	return sdk.Success([]byte("Uploaded Merkle Proof " + ProofId + " of SearchId " + SearchId + " Successfully"))
}

// Update Search Result
func (f *FactContract) update_result() protogo.Response {
	params := sdk.Instance.GetArgs()

	K := string(params["K"])
	SK := string(params["SK"])
	Data := string(params["Data"])
	ResultId := string(params["ResultId"])
	SearchId := string(params["SearchId"])
	Valid := true

	// Construct Result
	result := NewResult(SearchId, ResultId, K, SK, Data, Valid)

	// Serialize
	factBytes, err := json.Marshal(result)
	if err != nil {
		return sdk.Error(fmt.Sprintf("marshal result failed, err: %s", err))
	}

	// Emit UploadResult Event
	sdk.Instance.EmitEvent("UploadResult", []string{SearchId, ResultId, K, SK, Data})

	// Store Result
	err = sdk.Instance.PutStateByte("Results", SearchId+"-"+ResultId, factBytes)
	if err != nil {
		return sdk.Error("fail to save Result bytes")
	}

	// Return Result
	return sdk.Success([]byte("Uploaded Result " + ResultId + " of SearchId " + SearchId + " Successfully"))
}

// Check search results and range proof
func (f *FactContract) result_and_range_proof_check() protogo.Response {
	params := sdk.Instance.GetArgs()

	// Get Parameter
	SI := string(params["SearchId"])
	// Search For Result
	sr, err := sdk.Instance.GetStateByte("SearchInfos", SI)
	if err != nil {
		return sdk.Error("failed to call get_state")
	}

	// Unserialize
	var searchinfo SearchInfo
	if err = json.Unmarshal(sr, &searchinfo); err != nil {
		return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
	}

	// Get TTHash
	th, err := sdk.Instance.GetStateByte("TTHashes", searchinfo.Time+searchinfo.Table)
	if err != nil {
		return sdk.Error("failed to call get_state")
	}

	// Unserialize
	var tthash TTHash
	if err = json.Unmarshal(th, &tthash); err != nil {
		return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
	}

	roothash := tthash.Hash
	rn := searchinfo.ResultNum
	sk, ek, ssk, sek := 0, 0, 0, 0
	skf, ekf, sskf, sekf := false, false, false, false
	if searchinfo.SK == "NULL" {
		skf = true
	} else {
		sk, err = strconv.Atoi(searchinfo.SK)
	}
	if searchinfo.EK == "NULL" {
		ekf = true
	} else {
		ek, err = strconv.Atoi(searchinfo.EK)
	}
	if searchinfo.SSK == "NULL" {
		sskf = true
	} else {
		ssk, err = strconv.Atoi(searchinfo.SSK)
	}
	if searchinfo.SEK == "NULL" {
		sekf = true
	} else {
		sek, err = strconv.Atoi(searchinfo.SEK)
	}
	layer := searchinfo.ProofLayer
	var result Result
	var proof Proof
	pd := 0
	resultcount := rn
	for r := 0; r < rn; r++ {
		unmar, err := sdk.Instance.GetStateByte("Results", SI+"-"+strconv.Itoa(r))
		if err != nil {
			return sdk.Error("failed to call get_state, maybe not uploaded")
		}

		// Unserialize
		if err = json.Unmarshal(unmar, &result); err != nil {
			return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
		}
		rk, err := strconv.Atoi(result.K)
		rsk, err := strconv.Atoi(result.SK)
		if skf && ekf {
		} else if (ekf && (rk < sk)) || (skf && (rk > ek)) || ((rk > ek) || (rk < sk)) {
			result.Valid = false
		}

		if sskf && sekf {
		} else if (sekf && (rsk < ssk)) || (sskf && (rsk > sek)) || ((rsk > sek) || (rsk < ssk)) {
			result.Valid = false
		}

		// Serialize
		factBytes, err := json.Marshal(result)
		if err != nil {
			return sdk.Error(fmt.Sprintf("marshal result failed, err: %s", err))
		}
		// Store Result
		err = sdk.Instance.PutStateByte("Results", SI+"-"+strconv.Itoa(r), factBytes)
		if err != nil {
			return sdk.Error("fail to save Result bytes")
		}
		if result.Valid == false {
			resultcount--
			continue
		}
	}

	leftflag := false
	rightflag := false
	i := 4
	if searchinfo.LeftKey == "NULL" {
		leftflag = true
		i--
	}
	if searchinfo.RightKey == "NULL" {
		rightflag = true
		i--
	}
	firstflag := i
	for ; i > 0; i-- {
		unmar, err := sdk.Instance.GetStateByte("RangeProofs", SI+"-"+strconv.Itoa(pd))
		if err != nil {
			return sdk.Error("failed to call get_state, maybe not uploaded")
		}

		// unserialize
		if err = json.Unmarshal(unmar, &proof); err != nil {
			return sdk.Error(fmt.Sprintf("unmarshal  fact failed, err: %s", err))
		}

		b := sha256.Sum256([]byte(proof.RawString))
		s := hex.EncodeToString(b[:])
		if strings.Contains(proof.RawString, proof.Hash) != true {
			searchinfo.Status = "Range Proof Check Failed"
			// Serialize
			factBytes, err := json.Marshal(searchinfo)
			if err != nil {
				return sdk.Error(fmt.Sprintf("marshal SearchInfo failed, err: %s", err))
			}
			// Store Search Information
			err = sdk.Instance.PutStateByte("SearchInfos", strconv.Itoa(searchinfo.SearchId), factBytes)
			if err != nil {
				return sdk.Error("fail to save SearchInfos bytes")
			}
			return sdk.Error("Contain_Check_Failed on " + strconv.Itoa(pd) + " of SeachId " + SI)
		}

		if i == firstflag && leftflag == true {
			if strings.HasPrefix(proof.RawString, proof.Hash) != true {
				searchinfo.Status = "Range Proof Check Failed"
				// Serialize
				factBytes, err := json.Marshal(searchinfo)
				if err != nil {
					return sdk.Error(fmt.Sprintf("marshal SearchInfo failed, err: %s", err))
				}
				// Store Search Information
				err = sdk.Instance.PutStateByte("SearchInfos", strconv.Itoa(searchinfo.SearchId), factBytes)
				if err != nil {
					return sdk.Error("fail to save SearchInfos bytes")
				}
				return sdk.Error("Contain_Check_Failed on " + strconv.Itoa(pd) + " of SeachId " + SI)
			}
		}

		if i == 1 && rightflag == true {
			if strings.HasSuffix(proof.RawString, proof.Hash) != true {
				searchinfo.Status = "Range Proof Check Failed"
				// Serialize
				factBytes, err := json.Marshal(searchinfo)
				if err != nil {
					return sdk.Error(fmt.Sprintf("marshal SearchInfo failed, err: %s", err))
				}
				// Store Search Information
				err = sdk.Instance.PutStateByte("SearchInfos", strconv.Itoa(searchinfo.SearchId), factBytes)
				if err != nil {
					return sdk.Error("fail to save SearchInfos bytes")
				}
				return sdk.Error("Contain_Check_Failed on " + strconv.Itoa(pd) + " of SeachId " + SI)
			}
		}

		for j := layer - 1; j > 0; j-- {
			pd = pd + 1
			unmar, err := sdk.Instance.GetStateByte("RangeProofs", SI+"-"+strconv.Itoa(pd))
			if err != nil {
				return sdk.Error("failed to call get_state, maybe not uploaded")
			}

			// Unserialize
			if err = json.Unmarshal(unmar, &proof); err != nil {
				return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
			}

			if strings.Contains(proof.RawString, proof.Hash) != true {
				searchinfo.Status = "Range Proof Check Failed"
				// Serialize
				factBytes, err := json.Marshal(searchinfo)
				if err != nil {
					return sdk.Error(fmt.Sprintf("marshal SearchInfo failed, err: %s", err))
				}
				// Store Search Information
				err = sdk.Instance.PutStateByte("SearchInfos", strconv.Itoa(searchinfo.SearchId), factBytes)
				if err != nil {
					return sdk.Error("fail to save SearchInfos bytes")
				}
				return sdk.Error("Contain_Check_Failed on " + strconv.Itoa(pd) + " of SeachId " + SI)
			}

			if i == firstflag && leftflag == true {
				if strings.HasPrefix(proof.RawString, proof.Hash) != true {
					searchinfo.Status = "Range Proof Check Failed"
					// Serialize
					factBytes, err := json.Marshal(searchinfo)
					if err != nil {
						return sdk.Error(fmt.Sprintf("marshal SearchInfo failed, err: %s", err))
					}
					// Store Search Information
					err = sdk.Instance.PutStateByte("SearchInfos", strconv.Itoa(searchinfo.SearchId), factBytes)
					if err != nil {
						return sdk.Error("fail to save SearchInfos bytes")
					}
					return sdk.Error("Contain_Check_Failed on " + strconv.Itoa(pd) + " of SeachId " + SI)
				}
			}

			if i == 1 && rightflag == true {
				if strings.HasSuffix(proof.RawString, proof.Hash) != true {
					searchinfo.Status = "Range Proof Check Failed"
					// Serialize
					factBytes, err := json.Marshal(searchinfo)
					if err != nil {
						return sdk.Error(fmt.Sprintf("marshal SearchInfo failed, err: %s", err))
					}
					// Store Search Information
					err = sdk.Instance.PutStateByte("SearchInfos", strconv.Itoa(searchinfo.SearchId), factBytes)
					if err != nil {
						return sdk.Error("fail to save SearchInfos bytes")
					}
					return sdk.Error("Contain_Check_Failed on " + strconv.Itoa(pd) + " of SeachId " + SI)
				}
			}

			if s != proof.Hash {
				searchinfo.Status = "Range Proof Check Failed"
				// Serialize
				factBytes, err := json.Marshal(searchinfo)
				if err != nil {
					return sdk.Error(fmt.Sprintf("marshal SearchInfo failed, err: %s", err))
				}
				// Store Search Information
				err = sdk.Instance.PutStateByte("SearchInfos", strconv.Itoa(searchinfo.SearchId), factBytes)
				if err != nil {
					return sdk.Error("fail to save SearchInfos bytes")
				}
				return sdk.Error("Hash_Check_Failed on " + strconv.Itoa(pd) + " of SeachId " + SI)
			}
			b = sha256.Sum256([]byte(proof.RawString))
			s = hex.EncodeToString(b[:])
		}

		if s != roothash {
			searchinfo.Status = "Range Proof Check Failed"
			// Serialize
			factBytes, err := json.Marshal(searchinfo)
			if err != nil {
				return sdk.Error(fmt.Sprintf("marshal SearchInfo failed, err: %s", err))
			}
			// Store Search Information
			err = sdk.Instance.PutStateByte("SearchInfos", strconv.Itoa(searchinfo.SearchId), factBytes)
			if err != nil {
				return sdk.Error("fail to save SearchInfos bytes")
			}
			return sdk.Error("RootHash_Check_Failed on " + strconv.Itoa(pd) + " of SeachId " + SI)
		}
		pd = pd + 1
	}

	searchinfo.Status = "Checked Successfully"
	// Serialize
	factBytes, err := json.Marshal(searchinfo)
	if err != nil {
		return sdk.Error(fmt.Sprintf("marshal SearchInfo failed, err: %s", err))
	}
	// Store Search Information
	err = sdk.Instance.PutStateByte("SearchInfos", strconv.Itoa(searchinfo.SearchId), factBytes)
	if err != nil {
		return sdk.Error("fail to save SearchInfos bytes")
	}
	return sdk.Success([]byte("Search " + SI + " Range Proof and Result Checked_Successfully! Totally " + strconv.Itoa(resultcount) + " results are Valid "))
}

// Check Merkle proof
func (f *FactContract) merkle_proof_check() protogo.Response {
	params := sdk.Instance.GetArgs()

	// Get Parameter
	SI := string(params["SearchId"])
	PN := string(params["ProofNum"])
	ProofNum, _ := strconv.Atoi(PN)
	ProofNum--
	// Get Search Information
	sr, err := sdk.Instance.GetStateByte("SearchInfos", SI)
	if err != nil {
		return sdk.Error("failed to call get_state")
	}

	// UnSerialize
	var searchinfo SearchInfo
	if err = json.Unmarshal(sr, &searchinfo); err != nil {
		return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
	}

	searchinfo.ProofNum = ProofNum
	rn := searchinfo.ResultNum
	first := 0
	second := 0
	var firstproof Proof
	var secondproof Proof
	var NewString string
	var pc int
	for {
		unmar, err := sdk.Instance.GetStateByte("MerkleProofs", SI+"-"+strconv.Itoa(first))
		if err != nil {
			return sdk.Error("failed to call get_state, maybe not uploaded")
		}

		// UnSerialize
		if err = json.Unmarshal(unmar, &firstproof); err != nil {
			return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
		}
		pc = 1
		NewString = ""
		RS := []rune(firstproof.RawString)
		for _, v := range RS {
			if v == ' ' {
				pc++
			}
		}
		for ; pc > 0; pc-- {
			second++
			unmar, err := sdk.Instance.GetStateByte("MerkleProofs", SI+"-"+strconv.Itoa(second))
			if err != nil {
				return sdk.Error("failed to call get_state, maybe not uploaded")
			}

			// UnSerialize
			if err = json.Unmarshal(unmar, &secondproof); err != nil {
				return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
			}
			NewString += secondproof.Hash
			if pc != 1 {
				NewString += " "
			}
		}
		if NewString != firstproof.RawString {
			searchinfo.Merkle = "Check Failed"
			sdk.Instance.EmitEvent("MerkleProofCheckFail", []string{SI})
			// Serialize
			factBytes, err := json.Marshal(searchinfo)
			if err != nil {
				return sdk.Error(fmt.Sprintf("marshal SearchInfo failed, err: %s", err))
			}
			// Store Search Information
			err = sdk.Instance.PutStateByte("SearchInfos", SI, factBytes)
			if err != nil {
				return sdk.Error("fail to save SearchInfos bytes")
			}
			return sdk.Error("Merkle_Check_Failed on " + strconv.Itoa(first) + " of SeachId " + SI)
		}
		first++
		if second == ProofNum {
			break
		}
	}
	var result Result
	unmar, err := sdk.Instance.GetStateByte("MerkleProofs", SI+"-"+strconv.Itoa(first))
	if err != nil {
		return sdk.Error("failed to call get_state, maybe not uploaded")
	}

	// UnSerialize
	if err = json.Unmarshal(unmar, &firstproof); err != nil {
		return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
	}

	for r := 0; r < rn; r++ {
		unmar, err := sdk.Instance.GetStateByte("Results", SI+"-"+strconv.Itoa(r))
		if err != nil {
			return sdk.Error("failed to call get_state, maybe not uploaded")
		}

		// UnSerialize
		if err = json.Unmarshal(unmar, &result); err != nil {
			return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
		}
		b := sha256.Sum256([]byte(result.Data))
		s := hex.EncodeToString(b[:])
		if strings.Contains(firstproof.RawString, s) {
			continue
		}
		first++
		unmar, err = sdk.Instance.GetStateByte("MerkleProofs", SI+"-"+strconv.Itoa(first))
		if err != nil {
			return sdk.Error("failed to call get_state, maybe not uploaded")
		}

		// UnSerialize
		if err = json.Unmarshal(unmar, &firstproof); err != nil {
			return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
		}
		if strings.Contains(firstproof.RawString, s) {
			continue
		} else {
			searchinfo.Merkle = "Check Failed"
			// Serialize
			factBytes, err := json.Marshal(searchinfo)
			if err != nil {
				return sdk.Error(fmt.Sprintf("marshal SearchInfo failed, err: %s", err))
			}
			// Store Search Information
			err = sdk.Instance.PutStateByte("SearchInfos", strconv.Itoa(searchinfo.SearchId), factBytes)
			if err != nil {
				return sdk.Error("fail to save SearchInfos bytes")
			}
			sdk.Instance.EmitEvent("MerkleProofCheckFail", []string{SI})
			return sdk.Success([]byte("Search " + SI + " Merkle Proof Check Failed in result " + strconv.Itoa(r)))
		}
	}
	searchinfo.Merkle = "Checked Successfully"
	// Serialize
	factBytes, err := json.Marshal(searchinfo)
	if err != nil {
		return sdk.Error(fmt.Sprintf("marshal SearchInfo failed, err: %s", err))
	}
	// Store Search Information
	err = sdk.Instance.PutStateByte("SearchInfos", strconv.Itoa(searchinfo.SearchId), factBytes)
	if err != nil {
		return sdk.Error("fail to save SearchInfos bytes")
	}
	sdk.Instance.EmitEvent("MerkleProofCheckSuccess", []string{SI})
	return sdk.Success([]byte("Search " + SI + " Merkle Proof Checked Successfully!"))
}

func (f *FactContract) get_status_and_result() protogo.Response {
	params := sdk.Instance.GetArgs()

	// Get Parameter
	SI := string(params["SearchId"])
	// Get Search Information
	sr, err := sdk.Instance.GetStateByte("SearchInfos", SI)
	if err != nil {
		return sdk.Error("failed to call get_state")
	}

	// UnSerialize
	var searchinfo SearchInfo
	var result Result
	if err = json.Unmarshal(sr, &searchinfo); err != nil {
		return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
	}

	if searchinfo.Status == "Not Uploaded" {
		return sdk.Error(fmt.Sprintf("Search Result Not Uploaded"))
	}
	if searchinfo.Status == "Range Proof Check Failed" {
		return sdk.Error(fmt.Sprintf("Proof Check Already Failed"))
	}
	if searchinfo.Status == "Result Check Failed" {
		return sdk.Error(fmt.Sprintf("Result Check Already Failed"))
	}
	if searchinfo.Status == "Checked Successfully" {
		if searchinfo.Status == "Not Uploaded" {
			return sdk.Error(fmt.Sprintf("Merkle Proof Not Uploaded"))
		}
		if searchinfo.Status == "Not Uploaded" {
			return sdk.Error(fmt.Sprintf("Search Result Not Uploaded"))
		}
		if searchinfo.Merkle == "Checked Successfully" {
			rn := searchinfo.ResultNum
			for r := 0; r < rn; r++ {
				unmar, err := sdk.Instance.GetStateByte("Results", SI+"-"+strconv.Itoa(r))
				if err != nil {
					return sdk.Error("failed to call get_state, maybe not uploaded")
				}

				// UnSerialize
				if err = json.Unmarshal(unmar, &result); err != nil {
					return sdk.Error(fmt.Sprintf("unmarshal fact failed, err: %s", err))
				}
				if result.Valid == true {
					sdk.Instance.EmitEvent("Return Result Number", []string{result.ResultId, " of Search Number", result.SearchId, " has primary key ", result.K,
						" and secondary key ", result.SK, " with raw data hex encoded ", result.Data})
				}
			}
			return sdk.Success([]byte("Search Result, Merkle Proof and Range Proof Checkded Successfully"))

		}
	}

	return sdk.Error(fmt.Sprintf("State of the search is wrong"))
}

// Main Function
func main() {
	err := sandbox.Start(new(FactContract))
	if err != nil {
		log.Fatal(err)
	}
}
