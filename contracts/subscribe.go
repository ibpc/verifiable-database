/*
  Copyright (C) BABEC. All rights reserved.
  Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
  and Privacy Computing. All rights reserved.

  SPDX-License-Identifier: Apache-2.0
*/

/*
 * @file subscribe.go
 * @brief subscribe search events from ChainMaker and execute search tasks
 *
 * This program works with ChainMaker v2.3.1 and VerifiableDB.go smart contract
 * 
 * Dependencies:
 *  - chainmaker-sdk-go/v2
*/

package main

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
	"chainmaker.org/chainmaker/sdk-go/v2/examples"
	_ "github.com/go-sql-driver/mysql"
)

// SDK config file path
// need to change before running
const (
	sdkConfigOrg1Client1Path = "../sdk_configs/sdk_config_org1_client1.yml"
)

// create client and subscribe events
func main() {
	// init client
	client, err := examples.CreateChainClientWithSDKConf(sdkConfigOrg1Client1Path)
	if err != nil {
		log.Fatalln(err)
	}
	// subscribe event
	go testSubscribeContractEvent(client)
	select {}
}

// subscribe contract event from ChainMaker
func testSubscribeContractEvent(client *sdk.ChainClient) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	//name your contract
	ctr := "VerifiableDB"
	//订阅指定合约的合约事件
	// 1. 获取所有历史+实时
	// c, err := client.SubscribeContractEvent(ctx, 0, -1, "claim005", "topic_vx")

	// 2. 获取实时
	//c, err := client.SubscribeContractEvent(ctx, -1, -1, "claim005", "topic_vx")

	// 3. 获取实时(兼容老版本)
	//c, err := client.SubscribeContractEvent(ctx, 0, 0, "claim005", "topic_vx")

	// 4. 获取历史到指定历史高度
	//c, err := client.SubscribeContractEvent(ctx, 0, 10, "claim005", "topic_vx")

	// 5. 获取历史到指定实时高度
	//c, err := client.SubscribeContractEvent(ctx, 0, 28, "claim005", "topic_vx")

	// 6. 获取实时直到指定高度
	//c, err := client.SubscribeContractEvent(ctx, -1, 25, "claim005", "topic_vx")

	// 7. 订阅所有topic
	// specify "BeginSearch" event
	// Subscribe BeginSearch From the Beginning to the End of the Block, for other usages please refer to the sdk
	c, err := client.SubscribeContractEvent(ctx, 0, -1, ctr, "BeginSearch")
	if err != nil {
		log.Fatalln(err)
	}

	// 7. 报错：起始高度高于当前区块高度，直接退出
	//c, err := client.SubscribeContractEvent(ctx, 25, 30, "claim005", "topic_vx")

	// 8. 报错：起始高度低于终止高度
	//c, err := client.SubscribeContractEvent(ctx, 25, 20, "claim005", "topic_vx")

	// 9. 报错：起始高度/终止高度低于-1
	//c, err := client.SubscribeContractEvent(ctx, -2, 20, "claim005", "topic_vx")

	//c, err := client.SubscribeContractEvent(ctx, 0, 0, "claim005", "")
	//c, err := client.SubscribeContractEvent(ctx, "64f50d594c2a739c7088f9fc6785e1934030e17b52f1a894baec61b98633a59f", "9c01b4c21d1907ab27aa23343493b3c9872777e3")




	for {
		select {
		case event, ok := <-c:
			// Get contract event
			if !ok {
				fmt.Println("chan is close!")
				return
			}
			if event == nil {
				log.Fatalln("require not nil")
			}
			contractEventInfo, ok := event.(*common.ContractEventInfo)
			if !ok {
				log.Fatalln("require true")
			}
			fmt.Printf("recv contract event [%d] => %+v\n", contractEventInfo.BlockHeight, contractEventInfo)

			// Get Search Information and Parameter
			// event data includes: SearchId, Time, Table, SK, EK
			eventdata := contractEventInfo.GetEventData()
			SearchId := eventdata[0]
			Time := eventdata[1]
			Table := eventdata[2]
			SK := eventdata[3]
			EK := eventdata[4]

			// Init proofid and resultid
			proofid := 0
			resultid := 0

			// Create Handler
			withSyncResult := true
			// Open Search Results
			resultfile, err := os.Open("/data/chainmaker_mbt/search_results/" + Table + "-" + Time + "-" + SK + "-" + EK + ".res")
			if err != nil {
				log.Println(err)
				continue
			}

			// Init reader
			rf := bufio.NewReader(resultfile)
			for {
				//Read Primary Key, Secondary Key and Base64 encoded whole data
				record, err := rf.ReadString(' ')
				fk := strings.Trim(record, " ")
				if err == io.EOF {
					break
				}
				//read one record 
				record, err = rf.ReadString('\n')
				record = strings.Trim(record, "\n")
				record = strings.Trim(record, "\r")
				record = strings.Trim(record, "|")
				record = strings.Trim(record, " ")
				_, err = rf.ReadString('\n')
				sk, err := rf.ReadString('\n')
				sk = strings.Trim(sk, "\n")
				sk = strings.Trim(sk, "\r")
				// encoding kvs
				kvs := []*common.KeyValuePair{
					{
						Key:   "SearchId",
						Value: []byte(SearchId),
					},
					{
						Key:   "ResultId",
						Value: []byte(strconv.Itoa(resultid)),
					},
					{
						Key:   "K",
						Value: []byte(fk),
					},
					{
						Key:   "SK",
						Value: []byte(sk),
					},
					{
						Key:   "Data",
						Value: []byte(record),
					},
				}
				//Update search result from database
				err = invokeUserContract(client, ctr, "update_result", "", kvs, withSyncResult)
				if err != nil {
					panic(err)
					return
				}
				resultid++
			}

			// Create Handler
			fi, err := os.Open("/data/chainmaker_mbt/merkle_proofs/" + Table + "-" + Time + "-" + SK + "-" + EK + ".rmkp")
			if err != nil {
				panic(err)
			}

			// Create Reader
			r := bufio.NewReader(fi)
			_, err = r.ReadString('\n')
			_, err = r.ReadString('\n')
			_, err = r.ReadString('\n')
			
			// Init reader level and vals
			hash := ""
			rawhash := ""
			level := 0
			curlevel := 1
			leftkey := ""
			leftvalue := ""
			rightkey := ""
			rightvalue := ""
			for {
				//Read Left Key, Left Value
				leftkey, err = r.ReadString('\n')
				leftkey = strings.Trim(leftkey, "\n")
				leftkey = strings.Trim(leftkey, "\r")
				if leftkey == "NULL" {
					leftvalue, err = r.ReadString('\n')
					leftvalue = "NULL"
					break
				}
				leftvalue, err = r.ReadString('\n')
				leftvalue = strings.Trim(leftvalue, "\n")
				leftvalue = strings.Trim(leftvalue, "\r")
				// Read Merkle Proof
				hash, err := r.ReadString('\n')
				hash = strings.Trim(hash, "\n")
				hash = strings.Trim(hash, "\r")
				rawhash, err := r.ReadString('\n')
				rawhash = strings.Trim(rawhash, "\n")
				rawhash = strings.Trim(rawhash, "\r")
				// encoding kvs
				kvs := []*common.KeyValuePair{
					{
						Key:   "SearchId",
						Value: []byte(SearchId),
					},
					{
						Key:   "ProofId",
						Value: []byte(strconv.Itoa(proofid)),
					},
					{
						Key:   "Hash",
						Value: []byte(hash),
					},
					{
						Key:   "RawString",
						Value: []byte(rawhash),
					},
				}
				//update range proof
				err = invokeUserContract(client, ctr, "update_range_proof", "", kvs, withSyncResult)
				if err != nil {
					panic(err)
					return
				}
				proofid++

				for {
					//read the rest of merkle proof
					hash, err = r.ReadString('\n')
					hash = strings.Trim(hash, "\n")
					hash = strings.Trim(hash, "\r")
					rawhash, err = r.ReadString('\n')
					rawhash = strings.Trim(rawhash, "\n")
					rawhash = strings.Trim(rawhash, "\r")
					if rawhash == "" {
						break
					}
					if err != nil && err != io.EOF {
						panic(err)
					}
					if err == io.EOF {
						break
					}
					// encoding kvs
					kvs = []*common.KeyValuePair{
						{
							Key:   "ProofId",
							Value: []byte(strconv.Itoa(proofid)),
						},
						{
							Key:   "SearchId",
							Value: []byte(SearchId),
						},
						{
							Key:   "Hash",
							Value: []byte(hash),
						},
						{
							Key:   "RawString",
							Value: []byte(rawhash),
						},
					}
					//update range proof
					err = invokeUserContract(client, ctr, "update_range_proof", "", kvs, withSyncResult)
					if err != nil {
						panic(err)
						return
					}
					proofid++
				}
				break
			}
			//Read Start Key, Start Value
			startkey, err := r.ReadString('\n')
			startkey = strings.Trim(startkey, "\n")
			startkey = strings.Trim(startkey, "\r")
			startvalue, err := r.ReadString('\n')
			startvalue = strings.Trim(startvalue, "\n")
			startvalue = strings.Trim(startvalue, "\r")

			// Read Proof
			hash, err = r.ReadString('\n')
			hash = strings.Trim(hash, "\n")
			hash = strings.Trim(hash, "\r")
			rawhash, err = r.ReadString('\n')
			rawhash = strings.Trim(rawhash, "\n")
			rawhash = strings.Trim(rawhash, "\r")
			// encoding kvs
			kvs := []*common.KeyValuePair{
				{
					Key:   "ProofId",
					Value: []byte(strconv.Itoa(proofid)),
				},
				{
					Key:   "SearchId",
					Value: []byte(SearchId),
				},
				{
					Key:   "Hash",
					Value: []byte(hash),
				},
				{
					Key:   "RawString",
					Value: []byte(rawhash),
				},
			}
			//update range proof
			err = invokeUserContract(client, ctr, "update_range_proof", "", kvs, withSyncResult)
			if err != nil {
				panic(err)
				return
			}
			proofid++

			for {
				//read the rest of range proof
				hash, err = r.ReadString('\n')
				hash = strings.Trim(hash, "\n")
				hash = strings.Trim(hash, "\r")
				rawhash, err = r.ReadString('\n')
				rawhash = strings.Trim(rawhash, "\n")
				rawhash = strings.Trim(rawhash, "\r")
				if rawhash == "" {
					level = curlevel
					break
				}
				curlevel++
				if err != nil && err != io.EOF {
					panic(err)
				}
				if err == io.EOF {
					break
				}

				// encoding kvs
				kvs = []*common.KeyValuePair{
					{
						Key:   "ProofId",
						Value: []byte(strconv.Itoa(proofid)),
					},
					{
						Key:   "SearchId",
						Value: []byte(SearchId),
					},
					{
						Key:   "Hash",
						Value: []byte(hash),
					},
					{
						Key:   "RawString",
						Value: []byte(rawhash),
					},
				}
				//update range proof
				err = invokeUserContract(client, ctr, "update_range_proof", "", kvs, withSyncResult)
				if err != nil {
					panic(err)
					return
				}
				proofid++
			}
			//Read End Key, End Value
			endkey, err := r.ReadString('\n')
			endkey = strings.Trim(endkey, "\n")
			endkey = strings.Trim(endkey, "\r")
			endvalue, err := r.ReadString('\n')
			endvalue = strings.Trim(endvalue, "\n")
			endvalue = strings.Trim(endvalue, "\r")

			// Read Merkle Proof
			hash, err = r.ReadString('\n')
			hash = strings.Trim(hash, "\n")
			hash = strings.Trim(hash, "\r")
			rawhash, err = r.ReadString('\n')
			rawhash = strings.Trim(rawhash, "\n")
			rawhash = strings.Trim(rawhash, "\r")

			// encoding kvs
			kvs = []*common.KeyValuePair{
				{
					Key:   "ProofId",
					Value: []byte(strconv.Itoa(proofid)),
				},
				{
					Key:   "SearchId",
					Value: []byte(SearchId),
				},
				{
					Key:   "Hash",
					Value: []byte(hash),
				},
				{
					Key:   "RawString",
					Value: []byte(rawhash),
				},
			}
			//updapte range proof
			err = invokeUserContract(client, ctr, "update_range_proof", "", kvs, withSyncResult)
			if err != nil {
				panic(err)
				return
			}
			proofid++

			for {
				//read the rest of proof
				hash, err = r.ReadString('\n')
				hash = strings.Trim(hash, "\n")
				hash = strings.Trim(hash, "\r")
				rawhash, err = r.ReadString('\n')
				rawhash = strings.Trim(rawhash, "\n")
				rawhash = strings.Trim(rawhash, "\r")
				if rawhash == "" {
					break
				}
				if err != nil && err != io.EOF {
					panic(err)
				}
				if err == io.EOF {
					break
				}

				// encoding kvs
				kvs = []*common.KeyValuePair{
					{
						Key:   "ProofId",
						Value: []byte(strconv.Itoa(proofid)),
					},
					{
						Key:   "SearchId",
						Value: []byte(SearchId),
					},
					{
						Key:   "Hash",
						Value: []byte(hash),
					},
					{
						Key:   "RawString",
						Value: []byte(rawhash),
					},
				}
				//update range proof
				err = invokeUserContract(client, ctr, "update_range_proof", "", kvs, withSyncResult)
				if err != nil {
					panic(err)
					return
				}
				proofid++
			}

			for {
				//Read Right Key, Right Value
				rightkey, err = r.ReadString('\n')
				rightkey = strings.Trim(rightkey, "\n")
				rightkey = strings.Trim(rightkey, "\r")
				if rightkey == "NULL" {
					rightvalue, err = r.ReadString('\n')
					rightvalue = "NULL"
					break
				}
				rightvalue, err = r.ReadString('\n')
				rightvalue = strings.Trim(rightvalue, "\n")
				rightvalue = strings.Trim(rightvalue, "\r")

				// Read Merkle Proof
				hash, err = r.ReadString('\n')
				hash = strings.Trim(hash, "\n")
				hash = strings.Trim(hash, "\r")
				rawhash, err = r.ReadString('\n')
				rawhash = strings.Trim(rawhash, "\n")
				rawhash = strings.Trim(rawhash, "\r")

				// encoding kvs
				kvs = []*common.KeyValuePair{
					{
						Key:   "ProofId",
						Value: []byte(strconv.Itoa(proofid)),
					},
					{
						Key:   "SearchId",
						Value: []byte(SearchId),
					},
					{
						Key:   "Hash",
						Value: []byte(hash),
					},
					{
						Key:   "RawString",
						Value: []byte(rawhash),
					},
				}
				//update range proof
				err = invokeUserContract(client, ctr, "update_range_proof", "", kvs, withSyncResult)
				if err != nil {
					panic(err)
					return
				}
				proofid++

				for {
					//read rest of proof
					hash, err = r.ReadString('\n')
					hash = strings.Trim(hash, "\n")
					hash = strings.Trim(hash, "\r")
					rawhash, err = r.ReadString('\n')
					rawhash = strings.Trim(rawhash, "\n")
					rawhash = strings.Trim(rawhash, "\r")
					if rawhash == "" {
						break
					}
					if err != nil && err != io.EOF {
						panic(err)
					}
					if err == io.EOF {
						break
					}

					// encoding kvs
					kvs = []*common.KeyValuePair{
						{
							Key:   "ProofId",
							Value: []byte(strconv.Itoa(proofid)),
						},
						{
							Key:   "SearchId",
							Value: []byte(SearchId),
						},
						{
							Key:   "Hash",
							Value: []byte(hash),
						},
						{
							Key:   "RawString",
							Value: []byte(rawhash),
						},
					}
					//update range proof
					err = invokeUserContract(client, ctr, "update_range_proof", "", kvs, withSyncResult)
					if err != nil {
						panic(err)
						return
					}
					proofid++
				}
				break
			}

			// encoding kvs
			kvs = []*common.KeyValuePair{
				{
					Key:   "ProofLayer",
					Value: []byte(strconv.Itoa(level)),
				},
				{
					Key:   "SearchId",
					Value: []byte(SearchId),
				},
				{
					Key:   "LeftKey",
					Value: []byte(leftkey),
				},
				{
					Key:   "LeftValue",
					Value: []byte(leftvalue),
				},
				{
					Key:   "StartKey",
					Value: []byte(startkey),
				},
				{
					Key:   "StartValue",
					Value: []byte(startvalue),
				},
				{
					Key:   "EndKey",
					Value: []byte(endkey),
				},
				{
					Key:   "EndValue",
					Value: []byte(endvalue),
				},
				{
					Key:   "RightKey",
					Value: []byte(rightkey),
				},
				{
					Key:   "RightValue",
					Value: []byte(rightvalue),
				},
				{
					Key:   "ResultNum",
					Value: []byte(strconv.Itoa(resultid)),
				},
			}

			// Start time record
			before := time.Now()

			//Upload the result of search
			err = invokeUserContract(client, ctr, "upload_from_offchain", "", kvs, withSyncResult)
			if err != nil {
				panic(err)
				return
			}

			// End time record and calc time used 
			after := time.Now()
			fmt.Println(after.Sub(before))

			kvs = []*common.KeyValuePair{
				{
					Key:   "SearchId",
					Value: []byte(SearchId),
				},
			}

			// Start time record
			before = time.Now()

			//Check the result and range proof
			err = invokeUserContract(client, ctr, "result_and_range_proof_check", "", kvs, withSyncResult)
			if err != nil {
				panic(err)
				return
			}

			// End time record and calc time used 
			after = time.Now()
			fmt.Println(after.Sub(before))

			proofid = 0
			// Create Handler
			mf, err := os.Open("/data/chainmaker_mbt/merkle_proofs/" + Table + "-" + Time + "-" + SK + "-" + EK + ".stmkp")
			if err != nil {
				panic(err)
			}
			mp := bufio.NewReader(mf)
			for {
				hash, err = mp.ReadString('\n')
				if err != nil && err != io.EOF {
					panic(err)
				}
				if err == io.EOF {
					break
				}
				//read merkle proof
				hash := strings.Trim(hash, "\n")
				hash = strings.Trim(hash, "\r")
				rawhash, err := mp.ReadString('\n')
				rawhash = strings.Trim(rawhash, "\n")
				rawhash = strings.Trim(rawhash, "\r")
				// encoding kvs
				kvs = []*common.KeyValuePair{
					{
						Key:   "ProofId",
						Value: []byte(strconv.Itoa(proofid)),
					},
					{
						Key:   "SearchId",
						Value: []byte(SearchId),
					},
					{
						Key:   "Hash",
						Value: []byte(hash),
					},
					{
						Key:   "RawString",
						Value: []byte(rawhash),
					},
				}
				//update the merkle proof
				err = invokeUserContract(client, ctr, "update_merkle_proof", "", kvs, withSyncResult)
				if err != nil {
					panic(err)
					return
				}
				proofid++
			}

			// encoding kvs
			kvs = []*common.KeyValuePair{
				{
					Key:   "SearchId",
					Value: []byte(SearchId),
				},
				{
					Key:   "ProofNum",
					Value: []byte(strconv.Itoa(proofid)),
				},
			}

			// Start time record
			before = time.Now()

			//Check the whole merkle proof
			err = invokeUserContract(client, ctr, "merkle_proof_check", "", kvs, withSyncResult)
			if err != nil {
				panic(err)
				return
			}

			// End time record and calc time used 
			after = time.Now()
			fmt.Println(after.Sub(before))

		case <-ctx.Done():
			return
		}
	}
}

// Invoke contract on ChainMaker
func invokeUserContract(client *sdk.ChainClient, contractName, method, txId string, kvs []*common.KeyValuePair, withSyncResult bool) error {
	// invoke smart contract
	resp, err := client.InvokeContract(contractName, method, txId, kvs, -1, withSyncResult)
	
	// invoke err
	if err != nil {
		return err
	}

	// response code fail
	if resp.Code != common.TxStatusCode_SUCCESS {
		return fmt.Errorf("invoke contract failed, [code:%d]/[msg:%s]\n", resp.Code, resp.Message)
	}

	// invode success, print msg
	if !withSyncResult {
		fmt.Printf("invoke contract success, resp: [code:%d]/[msg:%s]/[txId:%s]\n", resp.Code, resp.Message, resp.ContractResult.Result)
	} else {
		fmt.Printf("invoke contract success, resp: [code:%d]/[msg:%s]/[contractResult:%s]\n", resp.Code, resp.Message, resp.ContractResult)
	}

	return nil
}
