
#include <iostream>
#include <fcntl.h>
#include <errno.h>
#include <map>
#include <gtest/gtest.h>
#include <string.h>
#include <vector>

#include "../bplus_tree.h"
#include "test_utils.h"

using namespace std;

/**
 * @brief 通过插入的key直接计算出mbt的根哈希
 * 使用generate_record_with_two_int_by_number生成的record对于key的值是相同的
 * create_a_tree函数中，插入的record的位置是固定的，mbt的结构是固定的
 * @param keys mbt中插入的key
 * @param typ 记录类型，索引键是int
 * @return mbt的根哈希
 */
Hash& compute_hash_by_keys(vector<int> keys, int typ) {
    vector<Hash*> first_level_hashes;
    vector<Hash*> second_level_hashes;
    //遍历每一个节点区间
    for (int i = 0; i < keys.size()/2; i++) {
        //遍历节点区间中的元素，并生成record
        for(int j=keys[i*2]; j<=keys[i*2+1]; j++) {
            Record& record = generate_record_with_two_int_by_number(j, typ);
            Hash &hash = record.hash();
            second_level_hashes.push_back(&hash);
//            delete &record;
        }
        std::string hash_str = second_level_hashes[0]->to_hex();
        for (int j=1; j<keys[i*2+1]-keys[i*2]+1; j++) {
            hash_str += " ";
            hash_str += second_level_hashes[j]->to_hex();
        }
        Hash& hash = Hash::sha256(hash_str);
        first_level_hashes.push_back(&hash);
        second_level_hashes.clear();
    }
    std::string hash_str = first_level_hashes[0]->to_hex();
    for (int i=1; i<first_level_hashes.size(); i++) {
        hash_str += " ";
        hash_str += first_level_hashes[i]->to_hex();
    }
    return Hash::sha256(hash_str);
}

/**
 * @brief 测试bplus_tree的update_hash函数
 * @param keys 树中插入的key
 */
void bplus_tree_update_hash_test(vector<int> keys) {
    try {
        BplusTree* bpt = create_a_tree(keys);
        Hash& root_hash = compute_hash_by_keys(keys, bpt->leaf_node_type);
        Hash hash = bpt->update_hash(bpt->metadata->root_node_pos);
        ASSERT_EQ(root_hash.to_hex(), hash.to_hex());
    } catch (MyException& e) {
        cout << e.what() << endl;
        ASSERT_TRUE(false);
    }
}


TEST(Merkle_Proof, bplus_tree_update_hash_test) {
    try {
        int arr1[] = {1,10,30,40};
        vector<int> keys1(arr1, arr1 + sizeof(arr1) / sizeof(int));
        bplus_tree_update_hash_test(keys1);
        int arr2[] = {1,10,30,40,50,90};
        vector<int> keys2(arr2, arr2 + sizeof(arr2) / sizeof(int));
        bplus_tree_update_hash_test(keys2);
        int arr3[] = {1,10};
        vector<int> keys3(arr3, arr3 + sizeof(arr3) / sizeof(int));
        bplus_tree_update_hash_test(keys3);
    } catch (const std::exception &e) {
        std::cout << e.what() << std::endl;
    }
}

/**
 * @brief 测试bplus_tree的generate_merkle_proof函数
 * @param keys 树中插入的key
 */
void bplus_tree_generate_merkle_proof_test(vector<int> keys, int node_pos, int record_pos) {
    try {
        BplusTree* bpt = create_a_tree(keys);
        Hash hash = bpt->update_hash(bpt->metadata->root_node_pos);
        std::string mkp = bpt->generate_merkle_proof(node_pos, record_pos);
        MerkleProof *mkp_obj = MerkleProof::from_string(mkp);
        std::string mkp2 = mkp_obj->to_string();
        cout << mkp << endl;
        ASSERT_EQ(mkp, mkp2);
        ASSERT_TRUE(mkp_obj->verify());
    } catch (MyException& e) {
        cout << e.what() << endl;
        ASSERT_TRUE(false);
    }
}

TEST(Merkle_Proof, bplus_tree_generate_merkle_proof_test) {
    int arr1[] = {5,10,30,46,68,77};
    vector<int> keys(arr1, arr1 + sizeof(arr1) / sizeof(int));
    bplus_tree_generate_merkle_proof_test(keys, 3, 2);
    bplus_tree_generate_merkle_proof_test(keys, 4, 8);
    bplus_tree_generate_merkle_proof_test(keys, 5, 3);
}

/**
 * @brief 测试bplus_tree的generate_range_merkle_proof函数
 * @param keys 树中插入的key
 */
void bplus_tree_generate_range_merkle_proof_test(vector<int> keys, int left_key, int right_key) {
    try {
        BplusTree* bpt = create_a_tree(keys);
        Hash& root_hash = bpt->update_hash(bpt->metadata->root_node_pos);
        MbtKey *left, *right;
        if (left_key == -1)
            left = NULL;
        else {
            MbtKey tmp_key = MbtKey::from_int(left_key);
            left = new MbtKey;
            left->raw_key = tmp_key.raw_key;
        }
        if (right_key == -1)
            right = NULL;
        else {
            MbtKey tmp_key = MbtKey::from_int(right_key);
            right = new MbtKey;
            right->raw_key = tmp_key.raw_key;
        }
        bpt->init_search(left, right, true);
        std::string range_mkp = bpt->iter->generate_range_merkle_proof();
        cout << range_mkp << endl;
        ASSERT_TRUE(bpt->verify_range_merkle_proof(range_mkp, root_hash));
    } catch (MyException& e) {
        cout << e.what() << endl;
        ASSERT_TRUE(false);
    }
}

TEST(Merkle_Proof, bplus_tree_generate_range_merkle_proof_test) {
    int arr1[] = {5,10,30,46,68,77};
    vector<int> keys(arr1, arr1 + sizeof(arr1) / sizeof(int));
    bplus_tree_generate_range_merkle_proof_test(keys, -1, -1);
    bplus_tree_generate_range_merkle_proof_test(keys, 11, 15);
    bplus_tree_generate_range_merkle_proof_test(keys, 112, 245);
    bplus_tree_generate_range_merkle_proof_test(keys, 0, 4);
    bplus_tree_generate_range_merkle_proof_test(keys, 8, 56);
    bplus_tree_generate_range_merkle_proof_test(keys, 10, -1);
    bplus_tree_generate_range_merkle_proof_test(keys, -1, 50);
}


/**
 * @brief 测试bplus_tree的generate_range_merkle_proof函数
 * @param keys 树中插入的key
 */
void bplus_tree_generate_subtree_merkle_proof_test(vector<int> keys, int left_key, int right_key) {
    try {
        BplusTree* bpt = create_a_tree(keys);
        Hash& root_hash = bpt->update_hash(bpt->metadata->root_node_pos);
        MbtKey *left, *right;
        if (left_key == -1)
            left = NULL;
        else {
            MbtKey tmp_key = MbtKey::from_int(left_key);
            left = new MbtKey;
            left->raw_key = tmp_key.raw_key;
        }
        if (right_key == -1)
            right = NULL;
        else {
            MbtKey tmp_key = MbtKey::from_int(right_key);
            right = new MbtKey;
            right->raw_key = tmp_key.raw_key;
        }
        bpt->init_search(left, right, true);
        std::string range_mkp = bpt->iter->generate_subtree_merkle_proof();
        cout << range_mkp << endl;
        ASSERT_TRUE(bpt->verify_subtree_merkle_proof(range_mkp, root_hash));
    } catch (MyException& e) {
        cout << e.what() << endl;
        ASSERT_TRUE(false);
    }
}

TEST(Merkle_Proof, bplus_tree_generate_subtree_merkle_proof_test) {
    int arr1[] = {5,10,30,46,68,77};
    vector<int> keys(arr1, arr1 + sizeof(arr1) / sizeof(int));
    bplus_tree_generate_subtree_merkle_proof_test(keys, -1, -1);
    bplus_tree_generate_subtree_merkle_proof_test(keys, 35,40);
    bplus_tree_generate_subtree_merkle_proof_test(keys, 11, 15);
    bplus_tree_generate_subtree_merkle_proof_test(keys, 112, 245);
    bplus_tree_generate_subtree_merkle_proof_test(keys, 0, 4);
    bplus_tree_generate_subtree_merkle_proof_test(keys, 8, 56);
    bplus_tree_generate_subtree_merkle_proof_test(keys, 10, -1);
    bplus_tree_generate_subtree_merkle_proof_test(keys, -1, 50);
}


int main(int argc, char **argv) {
    printf("Running main() from %s\n", __FILE__);
    testing::InitGoogleTest(&argc, argv);
//    testing::FLAGS_gtest_filter = "Bitmap.simple_test";
    return RUN_ALL_TESTS();
}