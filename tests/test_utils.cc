#include <iostream>
#include <fcntl.h>
#include <errno.h>
#include <map>

#include "../bplus_tree.h"
#include "test_utils.h"

#include <dbug.h>

char get_a_char() {
    return rand() % (122-97+1) + 97;
}


Record& generate_record_with_two_int_by_number(int x, int typ) {
    Record *record = new Record(false, typ);
    record->key = MbtKey::from_int(x);
    record->raw_record = new uchar[INT_SIZE*2];
    int_to_uchars(x, record->raw_record);
    for (int i=INT_SIZE; i < INT_SIZE*2; i++) {
        record->raw_record[i] = (x+i) % 128;
    }
    return *record;
}

Record& generate_record_by_number_and_type(int x, int typ) {
    Record *record = new Record(false, typ);
    record->raw_record = new uchar[record->metadata->raw_record_size];
    //TODO:需要根据类型生成不同的key
    record->key = MbtKey::from_int(x);
    int_to_uchars(x, record->raw_record);
    for (int i=INT_SIZE; i < record->metadata->raw_record_size-1; i++) {
        record->raw_record[i] = (x+i) % 26 + 65;
    }
    record->raw_record[record->metadata->raw_record_size-1] = '\0';
    return *record;
}


Record& generate_index_record(int key, int pos, int typ) {
    Record *record = new Record(false, typ);
    record->key = MbtKey::from_int(key);
    record->set_pos(pos);
    return *record;
}


void check_record_with_two_int_by_number(Record& record, int x) {
    ASSERT_EQ(record.key.to_int(), x);
    for (int i=INT_SIZE; i < INT_SIZE*2; i++) {
        ASSERT_EQ(record.raw_record[i], (x+i) % 128);
    }
}

void check_record_by_number_and_type(Record& record, int x, int typ) {
    ASSERT_EQ(record.key.to_int(), x);
    for (int i=INT_SIZE; i < record.metadata->raw_record_size-1; i++) {
        ASSERT_EQ(record.raw_record[i], (x+i) % 26 + 65);
    }
    ASSERT_EQ(record.raw_record[record.metadata->raw_record_size-1], '\0');
}

Hash& generate_random_hash() {
    Hash *hash = new Hash(false);
    for (int i=0; i<SHA256_DIGEST_LENGTH; i++) {
        hash->hash[i] = random() % 256;
    }
    return *hash;
}


/**
 * @brief 通过直接编辑节点，构建一个b+tree
 *
 * @param keys 向树中插入的键值，两两一组组成一个区间为一个节点
 */
BplusTree* create_a_tree(std::vector<int> keys) {
    remove("./db.mb");
    BplusTree* bpt = new BplusTree("./db.mb", INT_TYPE, INT_TYPE*8+INT_TYPE, true);
    int root_node_pos = bpt->get_a_page();
    TreeNode* root_node = bpt->map(root_node_pos);
    TreeNode::create_new_node(root_node, bpt->internal_node_type, NOT_EXIST_NODE_POS,
                              root_node_pos, NOT_EXIST_NODE_POS, NOT_EXIST_NODE_POS);
    root_node->node_type = INDEX_TYPE;
    bpt->metadata = (Metadata*) bpt->FileMapper::map(METADATA_POSITION);
    bpt->metadata->root_node_pos = root_node_pos;
    bpt->metadata->first_leaf_node_pos = NOT_EXIST_NODE_POS;
    bpt->metadata->last_leaf_node_pos = NOT_EXIST_NODE_POS;

    TreeNode* leaf_node = NULL, *next_leaf_node = NULL;
    int leaf_node_pos, next_leaf_node_pos;
    //将keys中的键值插入到树中
    for (int i=0; i<keys.size()/2; i++) {
        next_leaf_node_pos = bpt->get_a_page();
        if (bpt->metadata->first_leaf_node_pos == NOT_EXIST_NODE_POS) {
            bpt->metadata->first_leaf_node_pos = next_leaf_node_pos;
        }
        bpt->metadata->last_leaf_node_pos = next_leaf_node_pos;

        TreeNode* next_leaf_node = bpt->map(next_leaf_node_pos);
        next_leaf_node->node_type = LEAF_TYPE;
        if (leaf_node == NULL) {
            TreeNode::create_new_node(next_leaf_node, bpt->leaf_node_type, root_node_pos, next_leaf_node_pos, NOT_EXIST_NODE_POS, NOT_EXIST_NODE_POS);
        }
        else {
            TreeNode::create_new_node(next_leaf_node, bpt->leaf_node_type, root_node_pos, next_leaf_node_pos, NOT_EXIST_NODE_POS, leaf_node_pos);
            leaf_node->next = next_leaf_node_pos;
        }
        leaf_node = next_leaf_node;
        leaf_node_pos = next_leaf_node_pos;
        for(int j=keys[i*2]; j<=keys[i*2+1]; j++) {
            Record& record = generate_record_with_two_int_by_number(j, bpt->leaf_node_type);
            leaf_node->insert(record);
            delete &record;
        }
        bpt->sync(leaf_node);
        Record* record = new Record(false, bpt->internal_node_type);
        MbtKey key = leaf_node->get_node_key();
        record->key.copy(key, record->metadata->key_type);
        record->set_pos(leaf_node_pos);
        root_node->insert(*record);
        delete record;
    }
    bpt->sync(root_node);
    return bpt;
}


BplusTree* create_a_tree_by_type(std::vector<int> keys, int index_type, int field_type) {
    remove("./db.mb");
    BplusTree* bpt = new BplusTree("./db.mb", index_type, field_type, true);
    int root_node_pos = bpt->get_a_page();
    TreeNode* root_node = bpt->map(root_node_pos);
    TreeNode::create_new_node(root_node, bpt->internal_node_type, NOT_EXIST_NODE_POS,
                              root_node_pos, NOT_EXIST_NODE_POS, NOT_EXIST_NODE_POS);
    root_node->node_type = INDEX_TYPE;
    bpt->metadata = (Metadata*) bpt->FileMapper::map(METADATA_POSITION);
    bpt->metadata->root_node_pos = root_node_pos;
    bpt->metadata->first_leaf_node_pos = NOT_EXIST_NODE_POS;
    bpt->metadata->last_leaf_node_pos = NOT_EXIST_NODE_POS;

    TreeNode* leaf_node = NULL, *next_leaf_node = NULL;
    int leaf_node_pos, next_leaf_node_pos;
    //将keys中的键值插入到树中
    for (int i=0; i<keys.size()/2; i++) {
        next_leaf_node_pos = bpt->get_a_page();
        if (bpt->metadata->first_leaf_node_pos == NOT_EXIST_NODE_POS) {
            bpt->metadata->first_leaf_node_pos = next_leaf_node_pos;
        }
        bpt->metadata->last_leaf_node_pos = next_leaf_node_pos;

        TreeNode* next_leaf_node = bpt->map(next_leaf_node_pos);
        next_leaf_node->node_type = LEAF_TYPE;
        if (leaf_node == NULL) {
            TreeNode::create_new_node(next_leaf_node, bpt->leaf_node_type,
                                      root_node_pos, next_leaf_node_pos,
                                      NOT_EXIST_NODE_POS, NOT_EXIST_NODE_POS);
        }
        else {
            TreeNode::create_new_node(next_leaf_node, bpt->leaf_node_type,
                                      root_node_pos, next_leaf_node_pos,
                                      NOT_EXIST_NODE_POS, leaf_node_pos);
            leaf_node->next = next_leaf_node_pos;
        }
        leaf_node = next_leaf_node;
        leaf_node_pos = next_leaf_node_pos;
        for(int j=keys[i*2]; j<=keys[i*2+1]; j++) {
            Record& record = generate_record_by_number_and_type(j, bpt->leaf_node_type);
            leaf_node->insert(record);
            delete &record;
        }
        bpt->sync(leaf_node);
        Record* record = new Record(false, bpt->internal_node_type);
        MbtKey key = leaf_node->get_node_key();
        record->key.copy(key, record->metadata->key_type);
        record->set_pos(leaf_node_pos);
        root_node->insert(*record);
        delete record;
    }
    bpt->sync(root_node);
    return bpt;
}