
#include <iostream>
#include <fcntl.h>
#include <errno.h>
#include <map>
#include <gtest/gtest.h>
#include <string.h>
#include <vector>
#include <fstream>

#include "../predefined.h"
#include "test_utils.h"

using namespace std;

void int_and_uchars_test(int x) {
    uchar* ptr = new uchar[4];
    int_to_uchars(x, ptr);
    int y = uchars_to_int(ptr);
    ASSERT_EQ(x, y);
    delete[] ptr;
}

TEST(Predefined, int_and_uchars) {
    for (int i=0; i<100; i++) {
        int_and_uchars_test(i * i);
    }
}

void string_and_uchars_test(std::string str) {
    uchar* ptr = hex_to_uchars(str);
    std::string str2 = uchars_to_hex(ptr, str.length()/2);
    ASSERT_EQ(str, str2);
}

TEST(Predefined, string_and_uchars_test) {
    string_and_uchars_test("aaaaaa");
    string_and_uchars_test("1213244513452345234523");
    string_and_uchars_test("1234567890");
    string_and_uchars_test("237458923456793234abce");
}

void string_split_test(vector<std::string> strs, char delimiter) {
    std::string str;
    if (strs.size() == 0) {
       str = "";
    } else {
        str = strs[0];
        for(int i=1; i<strs.size(); i++) {
            str += " " + strs[i];
        }
    }
    vector<std::string> strs2 = string_split(str, delimiter);
    ASSERT_EQ(strs.size(), strs2.size());
    for(int i=0; i<strs.size(); i++) {
        ASSERT_EQ(strs[i], strs2[i]);
    }
}

TEST(Predefined, string_split_test) {
    vector<std::string> strs;
    string_split_test(strs, ' ');
    strs.push_back("aaaaaa");
    strs.push_back("bbbbbb");
    strs.push_back("cccccc");
    string_split_test(strs, ' ');
}

void load_save_table_txt_test(int table_number) {
    std::vector<Table> opened_tables;
    for (int i=0; i<table_number; i++) {
        Table table;
        table.name = "table" + int_to_string(i);
        table.version = 1;
        table.db_file_dir_path = "/home/haojk/tables";
        table.mkp_files_dir_path = "/home/haojk/mkps";
        opened_tables.push_back(table);
    }
    remove("/tmp/tables.txt");
    save_tables_to_txt("/tmp/tables.txt", opened_tables);
    std::vector<Table> opened_tables2 = load_tables_from_txt("/tmp/tables.txt");
    ASSERT_EQ(opened_tables.size(), opened_tables2.size());
    for (int i=0; i<opened_tables.size(); i++) {
        ASSERT_EQ(opened_tables[i].name, opened_tables2[i].name);
    }
}

TEST(Predefined, load_save_table_txt_test) {
    load_save_table_txt_test(0);
    load_save_table_txt_test(1);
    load_save_table_txt_test(10);
}

void parse_path_test(std::string path, std::vector<std::string> strs) {
    std::vector<std::string> strs2 = parse_path(path);
    ASSERT_EQ(strs.size(), strs2.size());
    for (int i=0; i<strs.size(); i++) {
        ASSERT_EQ(strs[i], strs2[i]);
    }
}

TEST(Predefined, parse_path_test) {
    std::vector<std::string> strs;
    strs.push_back("/");
    strs.push_back("home");
    strs.push_back("haojk");
    strs.push_back("tables");
    parse_path_test("/home/haojk/tables", strs);
    strs.clear();
    strs.push_back("~");
    strs.push_back("mysql");
    strs.push_back("storage");
    strs.push_back("mbtree");
    strs.push_back("bplus_tree.cc");
    parse_path_test("~/mysql/storage/mbtree/bplus_tree.cc", strs);
}

void file_dir_check_create_test(bool is_file, std::string path) {
    if (is_file) {
        remove(path.c_str());
        ASSERT_FALSE(is_file_exist(path));
        ASSERT_TRUE(create_file(path));
        ASSERT_TRUE(is_file_exist(path));
        remove(path.c_str());
    } else {
        remove(path.c_str());
        ASSERT_FALSE(is_dir_exist(path));
        ASSERT_TRUE(create_dir(path));
        ASSERT_TRUE(is_dir_exist(path));
        remove(path.c_str());
    }
}

TEST(Predefined, file_dir_check_create_test) {
    file_dir_check_create_test(true, "/tmp/test_file.txt");
    file_dir_check_create_test(false, "/tmp/test_dir");
//    file_dir_check_create_test(true, "/tmp/test_dir/test_file.txt");
}

void copy_file_test(std::string src_file, std::string dst_file) {
    if(is_file_exist(src_file)){
        remove(src_file.c_str());
    }
    create_file(src_file);
    std::ofstream file(src_file.c_str());
    file << "test123456";
    file.close();
    if(is_file_exist(dst_file))
        remove(dst_file.c_str());
    copy_file(src_file, dst_file);
    std::vector<std::string> strs = parse_path(src_file);
    ASSERT_TRUE(is_file_exist(dst_file));
    ASSERT_TRUE(is_files_equal(src_file, dst_file));
}

TEST(Predefined, copy_file_test) {
    copy_file_test("/tmp/test_file.txt", "/tmp/test_file2.txt");
    if(!is_dir_exist("/tmp/test_dir"))
        create_dir("/tmp/test_dir");
    copy_file_test("/tmp/test_file.txt", "/tmp/test_dir/test_file.txt");
}

TEST(Predefined, get_var_length_test) {
    uchar* data = hex_to_uchars("010000007b030000056162636465");
    ASSERT_EQ(get_var_length(data, INT_TYPE*8*8+INT_TYPE*8+STR20_TYPE), 4+4+1+5);
}

TEST(Predefined, table_manager_test) {
    remove("/tmp/tables.txt");
    TableManager table_manager("/tmp/tables.txt");
    Table table1;
    table1.name = "haojk1";
    table1.version = 1;
    table_manager.import_table(table1);
    int table_index = table_manager.is_imported(table1.name, table1.version);
    ASSERT_NE(table_index, -1);
    ASSERT_TRUE(table_manager.open_table(table1.name, table1.version, NULL));
    ASSERT_TRUE(table_manager.is_opened(table_index));
    ASSERT_FALSE(table_manager.import_table(table1));
    ASSERT_FALSE(table_manager.open_table(table1.name, table1.version, NULL));
    Table table2;
    table2.name = "haojk2";
    table2.version = 1;
    table_index = table_manager.is_imported(table2.name, table2.version);
    ASSERT_EQ(table_index, -1);
    ASSERT_TRUE(table_manager.import_table(table2));
    table_index = table_manager.is_imported(table2.name, table2.version);
    ASSERT_NE(table_index, -1);
    ASSERT_FALSE(table_manager.is_opened(table_index));
}


//TEST(Predefined, parse_toml_config_test) {
//    std::string filePath = "/home/haojk/mysql-5.7.30/storage/mbtree/config.toml";
//    std::map<std::string, std::string> config = parse_toml_config(filePath);
//
//    std::string configPath = config["mbt_store.table_config_path"];
//    std::string dbFileDirPath = config["mbt_store.default_db_file_dir_path"];
//    std::string mkpFileDirPath = config["mbt_store.mkp_file_dir_path"];
//    std::string searchResultFileDirPath = config["mbt_store.default_search_result_file_dir_path"];
//
//    std::cout << "table_config_path: " << configPath << std::endl;
//    std::cout << "default_db_file_dir_path: " << dbFileDirPath << std::endl;
//    std::cout << "mkp_file_dir_path: " << mkpFileDirPath << std::endl;
//    std::cout << "default_search_result_file_dir_path: " << searchResultFileDirPath << std::endl;
//
//
//    std::map<std::string, std::string> config1 = parse_toml_config(filePath);
//
//    std::string configPath1 = config1["mbt_store.table_config_path"];
//    std::string dbFileDirPath1 = config1["mbt_store.default_db_file_dir_path"];
//    std::string mkpFileDirPath1 = config1["mbt_store.mkp_file_dir_path"];
//    std::string searchResultFileDirPath1 = config1["mbt_store.default_search_result_file_dir_path"];
//
//    std::cout << "table_config_path: " << configPath << std::endl;
//    std::cout << "default_db_file_dir_path: " << dbFileDirPath << std::endl;
//    std::cout << "mkp_file_dir_path: " << mkpFileDirPath << std::endl;
//    std::cout << "default_search_result_file_dir_path: " << searchResultFileDirPath << std::endl;
//}


int main(int argc, char **argv) {
    printf("Running main() from %s\n", __FILE__);
    testing::InitGoogleTest(&argc, argv);
//    testing::FLAGS_gtest_filter = "Bitmap.simple_test";
    return RUN_ALL_TESTS();
}