# coding:utf-8

import os
import subprocess
import shutil
import sys
import time

# import toml

base_dir = "/home/haojk/mysql_data"
database_name = "test"
table_name = "user"

client_path = "/home/haojk/mysql-5.7.30/storage/mbtree/cmake-build-debug-remote-host/mbt_store"
table_config_path = "/data/chainmaker_mbt/mbt_store_data/config.txt"
default_db_file_dir_path = "/data/chainmaker_mbt/mbt_store_data/mbt_db_files"
#backend_db_file_dir_path = "/data/chainmaker_mbt/mbt_store_data/mbt_db_files_bak"
default_mkp_file_dir_path = "/data/chainmaker_mbt/mbt_store_data/merkle_proofs"
#config_toml_path = "/home/haojk/mysql-5.7.30/storage/mbtree/config.toml"


def replace_output(output, input):
    placeholder = "__"
    for i in range(len(input)):
        output = output.replace(placeholder, input[i], 1)
    return output

def create_table_cmd(process, table_name, field_names, field_types):
    cmd = f"\n1\n{table_name}\n{field_names}\n{field_types}\n"
    # print("create table cmd: " + cmd)
    process.stdin.write(cmd.encode())


def open_table_cmd(process, table_name, version, table_path):
    cmd = f"\n2\n{table_name}\n{version}\n{table_path}\n"
    # print("open table cmd: " + cmd)
    process.stdin.write(cmd.encode())

def query_cmd(process, table_name, version, left_key, right_key):
    cmd = f"\n4\n{table_name}\n{version}\n{left_key}\n{right_key}\n"
    # print("query cmd: " + cmd)
    process.stdin.write(cmd.encode())

def exit_cmd(process):
    cmd = "\n7\n"
    process.stdin.write(cmd.encode())


def test_create_table(table_name, field_names, field_types):
    """
    测试创建表命令是否正确
    :param table_name: 创建的表的名字
    :expected: 数据库文件目录下有table_number个表，名字也对得上，版本号为0
    """
    process = subprocess.Popen(client_path, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # 发送创建表的命令和数据
    create_table_cmd(process, table_name, field_names, field_types)
    exit_cmd(process)

    # # 获取子进程的输出和错误信息
    stdout_data, stderr_data = process.communicate()

    # 解码输出和错误信息
    output = stdout_data.decode()
    error = stderr_data.decode()

    if error:
        print("error: ")
        print(error)
    process.stdin.close()

    if not os.path.isfile(table_config_path):
        print("error: config file not exist")
    if not os.path.isfile(os.path.join(default_db_file_dir_path, table_name + "-0.mb")):
        print("error: db file not exist")
    print("test_create_table success")

def test_open_empty_table(table_name, version, field_names, field_types):
    # 创建一个新表，然后复制到/tmp目录下
    process = subprocess.Popen(client_path, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    create_table_cmd(process, table_name, field_names, field_types)
    exit_cmd(process)
    process.stdin.close()
    shutil.copy2(os.path.join(default_db_file_dir_path, f"{table_name}-0.mb"),
                     f"/tmp/{table_name}" + ".mb")

    # 删除到和将要复制过来的文件同路径的文件，并清除config文件
    table_name += "x"
    if os.path.isfile(os.path.join(default_db_file_dir_path, table_name + f"-{version}.mb")):
        os.remove(os.path.join(default_db_file_dir_path, table_name + f"-{version}.mb"))
    if os.path.isfile(table_config_path):
        os.remove(table_config_path)

    # 导入表
    process = subprocess.Popen(client_path, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    open_table_cmd(process, table_name, version, f"/tmp/{table_name}" + ".mb")
    exit_cmd(process)

    # # 获取子进程的输出和错误信息
    stdout_data, stderr_data = process.communicate()

    # 解码输出和错误信息
    output = stdout_data.decode()
    error = stderr_data.decode()

    if error:
        print("error: ")
        print(error)
    process.stdin.close()

    if not os.path.isfile(table_config_path):
        print("ERROR: config file not exist")
    if not os.path.isfile(
            os.path.join(default_db_file_dir_path, table_name + f"-{version}.mb")):
        print("ERROR: db file not exist")
    print("test_open_empty_table success")


def test_query(table_name, version, left_key, right_key, old_table_path):
    f"""
    备份+查询
    :param table_name: 表名 
    :param version: 备份过来后的版本号
    :param left_key: 查询的左边界
    :param right_key: 查询的右边界 
    :param old_table_path: 待备份表的路径
    :expected:
        备份成功，复制过来一个{table_name}-{version}-{left_key}-{right_key}.mb的表
        查询成功，打印结果
    """
    process = subprocess.Popen(client_path, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    open_table_cmd(process, table_name, version, old_table_path)
    print("test_query: open table success")
    query_cmd(process, table_name, version, left_key, right_key)
    exit_cmd(process)

    # # 获取子进程的输出和错误信息
    stdout_data, stderr_data = process.communicate()

    # 解码输出和错误信息
    output = stdout_data.decode()
    error = stderr_data.decode()
    print(output)

    if error:
        print("error: ")
        print(error)
    process.stdin.close()

    if not os.path.isfile(
            os.path.join(default_db_file_dir_path, table_name + f"-{version}.mb")):
        print("ERROR: db file not exist")
    print("test_query success")

if __name__ == "__main__":
    # test_create_table("abcd1", "xxxxx", 8*1+2)
    # test_open_empty_table("abc", 3, "xxxxx", 8*1+2)
    # test_query("db", 20, "5", "10", os.path.join(backend_db_file_dir_path, "db.mb"))

    # 集成测试
    # config = toml.load(config_toml_path)
    # base_dir = "/home/haojk/mysql_data"
    # database_name = "test"
    # table_name = "user"
    table_path = os.path.join(base_dir, "data", database_name, table_name + ".mb")
    print("table_path: ", table_path)
    version = time.time()%(10**6)*1000
    args = sys.argv
    if len(args) != 3:
        print("usage: python3 test.py <start> <end>")
        exit(1)
    test_query("user", int(version), args[1], args[2], table_path)
