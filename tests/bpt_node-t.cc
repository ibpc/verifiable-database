/**
 * @file bpt_node-t.cc
 * @brief 用于测试tree_node.h和tree_node.cc文件中定义的记录和树节点
 *
 * Maintainer: haojk
 * Email: jk.hao@pku.edu.cn
 *
 * Dependencies:
 *  - tree_node.h
 *  - test_utils.h
 */

#include <iostream>
#include <fcntl.h>
#include <errno.h>
#include <map>
#include <gtest/gtest.h>
#include <string.h>
#include <vector>

#include <dbug.h>

#include "../bplus_tree.h"
#include "../tree_node.h"
#include "test_utils.h"

using namespace std;


/**
 * @brief 验证记录的深复制功能
 * @param x 用于随机生成记录的索引键
 * @param typ 记录类型
 * @expect 通过
 */
void record_deep_copy_test(int x, int typ) {
    Record& record = generate_record_with_two_int_by_number(x, typ);
    Record *record2 = new Record(false, typ);
    record2->key = MbtKey::from_int(0);
    record2->raw_record = new uchar[INT_SIZE*2];
    record2->copy(record);

    ASSERT_TRUE(record2->equal(record));
}


TEST(TREE_NODE, record_copy_test) {
    int typ = Record::metadatas.size();
    Record::metadatas[typ] = RecordMetadata(typ, INT_TYPE, INT_TYPE * 8 + INT_TYPE, LEAF_TYPE);
    for (int i=0; i<100; i++) {
        record_deep_copy_test(i, typ);
    }
}

/**
 * @brief 向节点中插入和获取记录功能测试，验证插入的记录和计算的记录哈希是否正确
 * @param record_num 节点中的记录数量
 * @param typ 记录类型
 */
void get_and_set_child_in_record(int record_num, int typ) {
    std::vector<Hash*> hashes;

    TreeNode* node = (TreeNode*) malloc(PAGE_SIZE);
    memset((void*) node, 0, PAGE_SIZE);
    TreeNode::create_new_node(node, typ, 0, 0, 0, 0);
    node->n = record_num;
    Record& record1 = generate_record_by_number_and_type(record_num-1+11, typ);
    Hash& hash1 = record1.hash();
    node->set_child(record1, record_num-1);
    delete &record1;
    Record& record2 = node->get_child(record_num-1);
    check_record_by_number_and_type(record2, record_num-1+11, typ);
    Hash& hash2 = node->get_hash(record_num-1);
    ASSERT_EQ(hash1, hash2);
    delete &hash1;
    delete &hash2;
    delete &record2;
}

TEST(TREE_NODE, get_and_set_test) {
    srand(0);
    int typ = Record::metadatas.size();
    Record::metadatas[typ] = RecordMetadata(typ, INT_TYPE, INT_TYPE * 8 + INT_TYPE, LEAF_TYPE);

    get_and_set_child_in_record(10, typ);
    get_and_set_child_in_record(100, typ);
    get_and_set_child_in_record(Record::metadatas[typ].max_record_num, typ);

    typ = Record::metadatas.size();
    Record::metadatas[typ] = RecordMetadata(typ, INT_TYPE, INT_TYPE * 8 + STR20_TYPE, LEAF_TYPE);
    get_and_set_child_in_record(10, typ);
    get_and_set_child_in_record(100, typ);
    get_and_set_child_in_record(Record::metadatas[typ].max_record_num, typ);
}

/**
 * @brief 测试节点记录插入和查询操作，记录插入不会超过节点容量上限
 * @param keys 待插入记录的生成索引值
 * @param not_in_keys 未插入的索引值
 * @param typ 记录类型
 * @expect 通过
 */
void insert_record_into_node_test(vector<int> keys, vector<int> not_in_keys, int typ) {
    TreeNode* node = (TreeNode*) malloc(PAGE_SIZE);

    //插入记录
    TreeNode::create_new_node(node, typ, 0, 0, 0, 0);
    for (int i=0; i<keys.size(); i++) {
        Record record = generate_record_by_number_and_type(keys[i], typ);
        node->insert(record);
    }

    ASSERT_EQ(node->n, keys.size());
    for (int i=0; i<keys.size(); i++) {
        Record *record = new Record(false, typ);
        MbtKey key = MbtKey::from_int(keys[i]);
        bool is_found = node->find(key, *record);
        ASSERT_TRUE(is_found);
        check_record_by_number_and_type(*record, keys[i], typ);
    }
    for (int i=0; i<not_in_keys.size(); i++) {
        Record* record = new Record(false, typ);
        MbtKey key = MbtKey::from_int(not_in_keys[i]);
        bool is_found = node->find(key, *record);
        ASSERT_FALSE(is_found);
        delete record;
    }
}

TEST(TREE_NODE, insert_and_find_test) {
    srand(0);
    int typ = Record::metadatas.size();
    Record::metadatas[typ] = RecordMetadata(typ, INT_TYPE, INT_TYPE * 8 + INT_TYPE, LEAF_TYPE);

    int arr0[] = {1000,1010,3003,1001,3,1};
    vector<int> not_in_keys(arr0, arr0+sizeof(arr0)/sizeof(int)) ;

    int arr1[] = {4,5,6,7,8,9};
    vector<int> in_order_keys(arr1, arr1+sizeof(arr1)/sizeof(int));
    insert_record_into_node_test(in_order_keys, not_in_keys, typ);

    int arr2[] =  {9,8,7,6,5,4};
    vector<int> reverse_order_keys(arr2, arr2+sizeof(arr2)/sizeof(int)) ;
    insert_record_into_node_test(reverse_order_keys, not_in_keys, typ);

    int arr3[] =  {4,9,6,7,5,8};
    vector<int> random_order_keys(arr3, arr3+sizeof(arr3)/sizeof(int));
    insert_record_into_node_test(random_order_keys, not_in_keys, typ);

    vector<int> max_keys;
    for(int i=4; i<Record::metadatas[typ].max_record_num+4; i++) {
        max_keys.push_back(i);
    }
    insert_record_into_node_test(max_keys, not_in_keys, typ);
}

TEST(TREE_NODE, insert_and_find_test2) {
    srand(0);
    int typ = Record::metadatas.size();
    Record::metadatas[typ] = RecordMetadata(typ, INT_TYPE, INT_TYPE*8*8+STR20_TYPE*8+STR20_TYPE, LEAF_TYPE);

    int arr0[] = {1000,1010,3003,1001,3,1};
    vector<int> not_in_keys(arr0, arr0+sizeof(arr0)/sizeof(int)) ;

    int arr1[] = {4,5,6,7,8,9};
    vector<int> in_order_keys(arr1, arr1+sizeof(arr1)/sizeof(int));
    insert_record_into_node_test(in_order_keys, not_in_keys, typ);

    int arr2[] =  {9,8,7,6,5,4};
    vector<int> reverse_order_keys(arr2, arr2+sizeof(arr2)/sizeof(int)) ;
    insert_record_into_node_test(reverse_order_keys, not_in_keys, typ);

    int arr3[] =  {4,9,6,7,5,8};
    vector<int> random_order_keys(arr3, arr3+sizeof(arr3)/sizeof(int));
    insert_record_into_node_test(random_order_keys, not_in_keys, typ);

    vector<int> max_keys;
    for(int i=4; i<Record::metadatas[typ].max_record_num+4; i++) {
        max_keys.push_back(i);
    }
    insert_record_into_node_test(max_keys, not_in_keys, typ);
}


/**
 * @brief 测试节点分裂功能
 * @param keys 节点中插入的记录索引键
 * @param typ 记录类型
 */
void node_split_test(vector<int> keys, int typ) {
    TreeNode* node = (TreeNode*) malloc(PAGE_SIZE);
    TreeNode::create_new_node(node, typ, 0, 0, 0, 0);
    TreeNode* node2 = (TreeNode*) malloc(PAGE_SIZE);
    TreeNode::create_new_node(node2, typ, 0, 0, 0, 0);

    for (int i=0; i<keys.size(); i++) {
        Record record = generate_record_by_number_and_type(keys[i], typ);
        node->insert(record);
    }
    node->split(*node2);
    std::sort(keys.begin(), keys.end());

    ASSERT_EQ(node->n, keys.size()/2);
    for (int i=0; i<node->n; i++) {
        Record *record = new Record(false, typ);
        MbtKey key = MbtKey::from_int(keys[i]);
        bool is_found = node->find(key, *record);
        ASSERT_TRUE(is_found);
        check_record_by_number_and_type(*record, keys[i], typ);
    }
    ASSERT_EQ(node2->n, keys.size()-keys.size()/2);
    for (int i=0; i<node2->n; i++) {
        Record *record = new Record(false, typ);
        MbtKey key = MbtKey::from_int(keys[i+node->n]);
        bool is_found = node2->find(key, *record);
        ASSERT_TRUE(is_found);
        check_record_by_number_and_type(*record, keys[i+node->n],typ);
    }
}

TEST(TREE_NODE, split_test) {
    srand(0);
    int typ = Record::metadatas.size();
    Record::metadatas[typ] = RecordMetadata(typ, INT_TYPE, INT_TYPE * 8 + INT_TYPE, LEAF_TYPE);

    int arr1[] = {1,2,3,4,5,6,7,8,9};
    vector<int> in_order_keys(arr1, arr1 + sizeof(arr1) / sizeof(int));
    node_split_test(in_order_keys, typ);

    int arr2[] = {9,8,7,6,5,4,3,2,1};
    vector<int> reverse_order_keys(arr2, arr2 + sizeof(arr2) / sizeof(int));
    node_split_test(in_order_keys, typ);

    int arr3[] = {2,4,9,1,6,3,7,5,8};
    vector<int> random_order_keys(arr3, arr3 + sizeof(arr3) / sizeof(int));
    node_split_test(in_order_keys, typ);

    vector<int> max_in_order_keys;
    for(int i=0; i<Record::metadatas[typ].max_record_num; i++) {
        max_in_order_keys.push_back(i);
    }
    node_split_test(max_in_order_keys, typ);
}

TEST(TREE_NODE, split_test2) {
    srand(0);
    int typ = Record::metadatas.size();
    Record::metadatas[typ] = RecordMetadata(typ, INT_TYPE, INT_TYPE*8*8+STR20_TYPE*8+INT_TYPE, LEAF_TYPE);

    int arr1[] = {1,2,3,4,5,6,7,8,9};
    vector<int> in_order_keys(arr1, arr1 + sizeof(arr1) / sizeof(int));
    node_split_test(in_order_keys, typ);

    int arr2[] = {9,8,7,6,5,4,3,2,1};
    vector<int> reverse_order_keys(arr2, arr2 + sizeof(arr2) / sizeof(int));
    node_split_test(in_order_keys, typ);

    int arr3[] = {2,4,9,1,6,3,7,5,8};
    vector<int> random_order_keys(arr3, arr3 + sizeof(arr3) / sizeof(int));
    node_split_test(in_order_keys, typ);

    vector<int> max_in_order_keys;
    for(int i=0; i<Record::metadatas[typ].max_record_num; i++) {
        max_in_order_keys.push_back(i);
    }
    node_split_test(max_in_order_keys, typ);
}


/**
 * @brief 验证索引记录读写索引指针的功能
 * @param x 写入的索引指针值
 */
void record_get_set_pos_test(int x, int typ) {
    Record *record = new Record(false, typ);
    record->key.raw_key = new uchar[INT_SIZE];
    record->raw_record = new uchar[INT_SIZE];
    record->set_pos(x);
    ASSERT_EQ(record->get_pos(), x);
}


TEST(TREE_NODE, record_get_set_pos_test) {
    srand(0);
    int typ = Record::metadatas.size();
    Record::metadatas[typ] = RecordMetadata(typ, INT_TYPE, INT_TYPE, INDEX_TYPE);

    for (int i=0; i<1000; i++) {
        record_get_set_pos_test(i, typ);
    }
}



TEST(TREE_NODE, record_to_string_test) {
    int typ = Record::metadatas.size();
    Record::metadatas[typ] = RecordMetadata(typ, INT_TYPE, INT_TYPE, LEAF_TYPE);
    Record *record = new Record(false, typ);
    record->raw_record = new uchar[INT_SIZE];
    memset(record->raw_record, 0, INT_SIZE);
    record->raw_record[0] = 0x77;
    ASSERT_EQ(record->to_string(), "119\n");
    delete record;

    typ = Record::metadatas.size();
    Record::metadatas[typ] = RecordMetadata(typ, INT_TYPE, INT_TYPE*8+INT_TYPE, LEAF_TYPE);
    record = new Record(false, typ);
    record->raw_record = new uchar[INT_SIZE*2];
    memset(record->raw_record, 0, INT_SIZE*2);
    record->raw_record[0] = 0x77;
    record->raw_record[4] = 0x2d;
    record->raw_record[5] = 0x39;
    ASSERT_EQ(record->to_string(), "119\n14637\n");
    delete record;

//    typ = Record::metadatas.size();
//    Record::metadatas[typ] = RecordMetadata(typ, INT_TYPE, INT_TYPE*8+STR20_TYPE, LEAF_TYPE);
//    record = new Record(false, typ);
//    record->raw_record = new uchar[INT_SIZE+STR20_SIZE];
//    int_to_uchars(10, record->raw_record);
//    memset(record->raw_record+INT_SIZE, 'a', STR20_SIZE);
//    ASSERT_EQ(record->to_string(), "10\naaaaaaaaaaaaaaaaaaaa\n");
//    delete record;
}


int main(int argc, char **argv) {
    printf("Running main() from %s\n", __FILE__);
    DBUG_PUSH("d:t:O");
    testing::InitGoogleTest(&argc, argv);
//    testing::FLAGS_gtest_filter = "Bitmap.simple_test";
    return RUN_ALL_TESTS();
}