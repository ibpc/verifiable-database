#!/usr/bin/env bash
#set -x

log_path=/tmp/mysqld.trace

function get_sub_select() {
  sed -n "/>$1/,/<$1/{=;p}" ${log_path} | sed 'N;s/\n/ /' > "$1"_"$2".log
}

get_sub_select "$1" "$2"

