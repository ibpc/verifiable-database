#!/usr/bin/env bash
#set -x

build_path=/home/haojk/mysql-5.7.30/cmake-build-debug-remote-host/
basedir=/home/haojk/mysql_data/
datadir=${basedir}data/
mysqld_path=${build_path}sql/mysqld
plugin_dir=${build_path}storage/mbtree/
mysqladmin_path=${build_path}client/mysqladmin
mysql_path=${build_path}client/mysql

function init() {
  raw_password=$(${mysqld_path}  \
    --initialize \
    --user=haojk \
    --basedir=${basedir} \
    --datadir=${datadir} 2>&1 \
    --lc-messages-dir=${basedir}share | \
    grep "A temporary password is generated for root@localhost")
  password=${raw_password: -12}
  echo "${password}"
}

function run() {
    ${mysqld_path} \
      --debug=d:t:i:o,/tmp/mysqld.trace \
      --plugin_dir=${plugin_dir} \
      --plugin_load=mbtree=ha_mbtree.so \
      --socket=/tmp/mysql.sock \
      --basedir=${basedir} \
      --datadir=${datadir} \
      --lc-messages-dir=${basedir}share > log.txt 2>&1 &
}

function clean() {
   rm -rf ${datadir}*
   echo "clean datadir successfully"
}

function resetpassword() {
    ${mysqladmin_path} \
      -u root \
      -p"${1}" password 111111
    echo "set password successfully"
}

function connect() {
    ${mysql_path} \
      -u root \
      -p111111
}

function kill() {
  killall mysqld
}

function init_and_run() {
    old_password=$(init)
    echo "old_password: ${old_password}"
    run
    sleep 3
    resetpassword "${old_password}"
    echo "init_and_run successfully!"
}

function test2() {
    kill
    sleep 1
    clean
    init_and_run
    echo "----------begin test-----------"
    python3 test_mysql_client.py create
    python3 test_mysql_client.py insert 0 99
    echo "----------end test-----------"
    python3 test_mbtree_client.py 10 20
    python3 test_mysql_client.py insert 100 199
    python3 test_mbtree_client.py 110 120
    kill
    sleep 1
#    clean
    echo "clean successfully!"
}

function test1() {
    kill
    sleep 1
    clean
    init_and_run
    echo "----------begin test-----------"
    python3 test_mysql_client.py create
    python3 test_mysql_client.py insert 0 200
    python3 test_mysql_client.py search 10 20
    echo "run test_mysql_client.py successfully!"
    echo "----------end test-----------"
    kill
    sleep 1
    clean
    echo "clean successfully!"
}

if [ "$1" = help ]
then
  echo "init: init mysql"
  echo "run: run mysql"
  echo "clean: clean datadir"
  echo "resetpassword: reset password"
  echo "connect: connect to mysql"
  echo "kill: kill mysqld"
  echo "stop: kill mysqld and clean datadir"
  echo "test: run test_mysql_client.py"
  echo "test2: run test_mysql_client.py and test_mbtree_client.py"
  echo "init_and_run: init and run mysql"
elif [ $1 = init_and_run ]
then
  init_and_run
elif [ $1 = init ]
then
  init
elif [ "$1" = run ]
then
  run
elif [ "$1" = clean ]
then
  clean
elif [ "$1" = resetpassword ]
then
  resetpassword "$2"
elif [ "$1" = connect ]
then
  connect
elif [ "$1" = kill ]
then
  kill
elif [ "$1" = stop ]
then
  kill
  sleep 1
  clean
elif [ "$1" = test1 ]
then
  test1
elif [ "$1" = test2 ]
then
  test2
else
  echo "wrong args"
fi

