/**
 * @file test_utils.h test_utils.cc
 * @brief 本项目中测试文件的所有依赖函数
 *
 * Maintainer: haojk
 * Email: jk.hao@pku.edu.cn
 *
 * Dependencies:
 *  - tree_node.h
 */

#ifndef MBTREE_TEST_UTILS_H
#define MBTREE_TEST_UTILS_H

#include <gtest/gtest.h>
#include <vector>

#include "../tree_node.h"
#include "../bplus_tree.h"
#include <dbug.h>

///生成一个随机的可读字符
char get_a_char();

/**
 * @brief 以索引键值为种子，生成一个随机的两个int字段的记录
 * 记录类型是(int,int)，索引也是int，是第一个位置
 * @param x 索引键值
 * @param typ 记录类型，索引键是int
 * @return 一个随机记录格式为(x,r)
 */
Record& generate_record_with_two_int_by_number(int x, int typ);

/**
 * @brief 以索引键值为种子，根据typ类型生成一个随机的记录
 * @param x 索引键值种子，目前为索引键值
 * @param typ 记录类型
 * @return 一个随机记录格式为(x,a,b,c...)
 */
Record& generate_record_by_number_and_type(int x, int typ);

/**
 * @brief 生成一个索引记录
 * @param key 传入的索引键
 * @param pos 传入的索引记录值
 * @param typ 索引节点类型，索引键是int
 * @return 一个索引记录，格式为(int,int)，第一个int为索引键值，第二个为索引指针值
 */
Record& generate_index_record(int key, int pos, int typ);

/**
 * @brief 记录是否符合生成的条件
 * @param record 待验证的记录
 * @param x 用于生成记录的索引键值
 */
void check_record_with_two_int_by_number(Record& record, int x);

/**
 * @brief 记录是否符合生成的条件
 * @param record 待验证的记录
 * @param x 用于生成记录的索引键值
 * @param typ 记录类型
 */
void check_record_by_number_and_type(Record& record, int x, int typ);


///生成一个随机的哈希值
Hash& generate_random_hash();

BplusTree* create_a_tree(std::vector<int> keys);


#endif //MBTREE_TEST_UTILS_H
