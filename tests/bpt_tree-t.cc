#include <iostream>
#include <fcntl.h>
#include <errno.h>
#include <map>
#include <gtest/gtest.h>
#include <string.h>
#include <vector>
#include <stdlib.h>

#ifdef is_sole
    #include <dbug.h>
#else
    #include "my_dbug.h"
#endif

#include "../bplus_tree.h"
#include "test_utils.h"

using namespace std;

/**
 * @brief 测试b+tree从空文件构造的情况，应是一个空树
 * @param bpt MBT对象指针
 * @return 如果验证通过返回true，否则返回false
 */
bool check_new_bplus_tree(BplusTree* bpt) {
    if (bpt->metadata->inited != META_ID) {
        //是否初始化
        return false;
    } else if (bpt->metadata->value_size != Record::metadatas[bpt->leaf_node_type].raw_record_size) {
        return false;
    } else if (bpt->metadata->key_size != Record::metadatas[bpt->leaf_node_type].key_size) {
        return false;
    } else if (bpt->metadata->index_type != Record::metadatas[bpt->leaf_node_type].key_type) {
        return false;
    } else if (bpt->metadata->field_types != Record::metadatas[bpt->leaf_node_type].fields_type) {
        return false;
    } else if (bpt->metadata->file_size != PAGE_SIZE*10) {
        //文件大小
        return false;
    } else if (bpt->metadata->height != 0) {
        //树高
        return false;
    } else if (bpt->metadata->root_node_pos != NOT_EXIST_NODE_POS) {
        //根节点不应该存在
        return false;
    } else if (bpt->metadata->first_leaf_node_pos != NOT_EXIST_NODE_POS) {
        //第一个叶子节点不应该存在
        return false;
    } else if (bpt->metadata->last_leaf_node_pos != NOT_EXIST_NODE_POS) {
        //最后一个叶子节点不应该存在
        return false;
    } else if (bpt->internal_node_type == bpt->leaf_node_type) {
        //内部节点类型和叶子节点类型是否初始化
        return false;
    } else if (!bpt->exist_pos(ROOT_FILE_PAGES_MANAGER_POSITION)) {
        //根文件页管理器是否存在
        return false;
    } else if (!bpt->exist_pos(METADATA_POSITION)) {
        //元数据是否存在
        return false;
    } else {
        //根哈希应为0x00...
        for (int i=0; i<SHA256_DIGEST_LENGTH; i++) {
            if (bpt->metadata->root_hash[i] != 0) {
                return false;
            }
        }
    }
    return true;
}

/**
 * @brief 构造一个多节点的树
 * keys: [1,10, 20,30, 40,50, 60,70]
 *                      root node
 * tree node [1,10] [20,30] [40,50] [60,70]
 * @param keys 各个节点的左右边界
 * @return 一棵mbt
 */
BplusTree* create_a_multi_node_tree(vector<int>keys, int index_type, int field_type) {
    BplusTree* bpt = new BplusTree("./db.mb", index_type, index_type, true);
    if (keys.size() != 0) {
        for (int i = 0; i < keys.size()/2; i++) {
            for (int j = keys[i * 2]; j <= keys[i * 2 + 1]; j++) {
                Record &record = generate_record_by_number_and_type(j, bpt->leaf_node_type);
                bpt->insert(record.key, record.raw_record);
            }
        }
    }
    return bpt;
}

/**
 * @brief 构造的一个多节点的树，通过左右边界的查询结果
 * @param keys 树的节点的左右边界
 * @param left_key 查询的左边界
 * @param right_key 查询的右边界
 * @return 查询结果
 */
vector<int> get_search_results(vector<int> keys, int left_key, int right_key) {
    vector<int> result;
    //将keys中符合左右边界的键值放入result中
    for(int i=0; i<keys.size()/2; i++) {
        for(int j=keys[i*2]; j<=keys[i*2+1]; j++) {
            if(right_key != -1 && j>right_key)
                break;
            if(left_key != -1 && j<left_key)
                continue;
            if((left_key==-1 || j>=left_key) && (right_key==-1 || j<=right_key))
                result.push_back(j);
        }
    }
    return result;
}

/**
 * @brief 测试b+tree从空文件构造的情况
 * @param file_path 文件路径
 * @param force_empty 是否强制清空文件
 */
void bplus_tree_constructor_from_empty_file_test(char *file_path, bool force_empty) {
    remove(file_path);

    BplusTree* bpt = new BplusTree(file_path, INT_TYPE, INT_TYPE*8+INT_TYPE, force_empty);
    ASSERT_TRUE(is_file_exist(file_path));
    ASSERT_TRUE(check_new_bplus_tree(bpt));
    delete bpt;
}

/**
 * @brief 测试b+tree从正常文件构造的情况，应是一个空树
 * @param file_path 文件路径
 * @param force_empty 是否强制清空文件
 */
void bplus_tree_constructor_from_file_test(char *file_path, bool force_empty) {
    BplusTree* bpt = new BplusTree(file_path, INT_TYPE, INT_TYPE*8+INT_TYPE, true);
    delete bpt;
    ASSERT_TRUE(is_file_exist(file_path));
    bpt = new BplusTree(file_path, INT_TYPE, INT_TYPE*8+INT_TYPE, force_empty);

    ASSERT_TRUE(check_new_bplus_tree(bpt));
    delete bpt;
}

/**
 * @brief 测试b+tree从错误文件构造的情况
 * @param file_path 文件路径
 * @param force_empty 是否强制清空文件
 * @expect 会置为空文件和空树
 */
void bplus_tree_constructor_from_wrong_file_test(char *file_path, bool force_empty) {
    remove(file_path);
    int fd = open(file_path, O_RDWR | O_CREAT, 0644);
    char *random_str = "asfasdfasdfasdfasdfasd";
    write(fd,  random_str, strlen(random_str));
    close(fd);

    BplusTree* bpt = new BplusTree(file_path, INT_TYPE, INT_TYPE*8+INT_TYPE, force_empty);

    ASSERT_TRUE(check_new_bplus_tree(bpt));
    delete bpt;
}

TEST(bplus_tree_constructor, construct_empty_bpt) {
    try {
        bplus_tree_constructor_from_empty_file_test("./db.mb", false);
        ASSERT_TRUE(false);
    } catch (MyException &e) {
        std::cout << e.what() << std::endl;
    }
    bplus_tree_constructor_from_file_test("./db.mb", false);
    try {
        bplus_tree_constructor_from_wrong_file_test("./db.mb", false);
    } catch (MyException &e) {
        std::cout << e.what() << std::endl;
    }
    bplus_tree_constructor_from_empty_file_test("./db.mb", true);
    bplus_tree_constructor_from_file_test("./db.mb", true);
    bplus_tree_constructor_from_wrong_file_test("./db.mb", true);
}

TEST(FileManager, file_map_test) {
    remove("./file_mapper_test.txt");
    FileMapper *file_mapper = new FileMapper();
    file_mapper->set_file_path_and_fd("./file_mapper_test.txt");
    file_mapper->trunc(10*PAGE_SIZE);
    void* metadata = file_mapper->map(METADATA_POSITION);
    file_mapper->set_metadata((Metadata*) metadata);
    file_mapper->metadata->inited = META_ID;
    delete file_mapper;
    file_mapper = new FileMapper();
    file_mapper->set_file_path_and_fd("./file_mapper_test.txt");
    metadata = file_mapper->map(METADATA_POSITION);
    file_mapper->set_metadata((Metadata*) metadata);
    ASSERT_EQ(file_mapper->metadata->inited, META_ID);
}


TEST(FileManager, bpt_map_test) {
    remove("./db.mb");
    BplusTree* bpt = new BplusTree("./db.mb", INT_TYPE, INT_TYPE*8+INT_TYPE, true);
    delete bpt;

    FileMapper* file_mapper = new FileMapper();
    file_mapper->set_file_path_and_fd("./file_mapper_test.txt");
    Metadata* metadata = (Metadata*) file_mapper->map(METADATA_POSITION);
    file_mapper->set_metadata((Metadata*) metadata);
    ASSERT_EQ(file_mapper->metadata->inited, META_ID);
}


/**
 * @brief 测试b+tree的插入功能
 * 插入完成之后直接遍历叶节点，验证叶节点的键值是否和插入的键值匹配
 * @param keys 向树中插入的键值
 */
void bplus_tree_insert_test(vector<int> keys) {
    try {
        remove("./db.mb");
        BplusTree *bpt = new BplusTree("./db.mb", INT_TYPE, INT_TYPE*8*8+STR20_TYPE+INT_TYPE, true);
        //将keys中的键值插入到树中
        for (int i = 0; i<keys.size(); i++) {
            Record &record = generate_record_by_number_and_type(keys[i], bpt->leaf_node_type);
            bpt->insert(record.key, record.raw_record);
        }

        std::sort(keys.begin(), keys.end());
        TreeNode *current_node = bpt->get_first_leaf();
        int current_node_pos = current_node->current;
        ASSERT_EQ(current_node_pos, bpt->metadata->first_leaf_node_pos);
        //遍历叶节点，验证叶节点的键值是否和插入的键值匹配
        for (int i=0, pos=0; i<keys.size(); i++, pos++) {
            int x = keys[i];
            if (pos == current_node->n) {
                DBUG_PRINT("info", ("current_node_pos: %d, n: %d", current_node_pos, current_node->n));
                //如果当前节点已经遍历完，则遍历下一个节点
                current_node_pos = current_node->next;
                ASSERT_NE(current_node_pos, NOT_EXIST_NODE_POS);
                current_node = bpt->map(current_node_pos);
                pos = 0;
                DBUG_PRINT("info", ("current_node_pos: %d, n: %d", current_node_pos, current_node->n));
            }
            int y = current_node->get_child(pos).key.to_int();
            //验证叶节点的键值是否和插入的键值匹配
            ASSERT_EQ(x, y);
        }
        ASSERT_EQ(current_node_pos, bpt->metadata->last_leaf_node_pos);
    } catch (MyException &e) {
        cout << e.what() << endl;
    }
}


/**
 * @brief 测试b+tree的search_child方法
 *
 * @param keys 向树中插入的键值，两两一组组成一个区间为一个节点
 * @param search_keys 用于查询的键值
 */
void bplus_tree_search_child_test(vector<int>&keys, vector<int>& search_keys) {
    BplusTree* bpt = create_a_tree(keys);
    //使用search_keys中的键值查询
    //i为search_keys的下标，j为keys的下标（也就是第几个节点）
    for(int i=0; i<search_keys.size(); i++) {
        MbtKey key = MbtKey::from_int(search_keys[i]);
        int leaf_node_pos = bpt->search_child(key);
        for (int j=0; j<keys.size()/2; j++) {
            if (keys[j*2] <= search_keys[i] && search_keys[i] <= keys[j*2+1]) {
                //如果该键在某个节点的区间内，则该节点为查询结果
                ASSERT_EQ(leaf_node_pos, j+3);
            }
            else if (j*2+2<keys.size() && keys[j*2+1] < search_keys[i] && search_keys[i] < keys[j*2+2]) {
                //如果该键在两个节点的区间的间隔中，则前边有空则在前节点，前边没有后边有则在后节点
                //如果前后都没有则在前节点
                //暂时全在前节点
                ASSERT_EQ(leaf_node_pos, j+3);
            }
            else {
                if (search_keys[i] < keys[0]) {
                    //如果在第一个节点之前，那么在第一个节点
                    ASSERT_EQ(leaf_node_pos, 3);
                } else if (search_keys[i] > keys[keys.size()-1]) {
                    //如果在最后一个节点之后，那么在最后一个节点
                    ASSERT_EQ(leaf_node_pos, keys.size()/2+2);
                }
            }
        }
    }
}


TEST(bplus_tree, bplus_tree_search_child_test) {
    int arr1[] = {5,10,30,46,50,90};
    vector<int> keys(arr1, arr1 + sizeof(arr1) / sizeof(int));
    int arr2[] = {11, 15, 112, 245, 4, 8};
//    int arr2[] = {100};
    vector<int> search_keys(arr2, arr2 + sizeof(arr2) / sizeof(int));
    bplus_tree_search_child_test(keys, search_keys);
}


/**
 * @brief 测试查询功能
 * 构造一棵树，然后使用左右边界查询对应键值，验证查询结果是否和插入的键值匹配
 * 如果left_key或right_key为-1，则表示不设置左或右边界
 *
 * @param keys 向树中插入的键值
 * @param left_key 用于查询的左边界值
 * @param right_key 用于查询的右边界值
 */
void bplus_tree_search_test(vector<int> keys, int left_key, int right_key) {
    try {
        BplusTree* bpt = create_a_tree(keys);
        vector<int> result;
        //将keys中符合左右边界的键值放入result中
        for(int i=0; i<keys.size()/2; i++) {
            for(int j=keys[i*2]; j<=keys[i*2+1]; j++) {
                if(right_key != -1 && j>right_key)
                    break;
                if(left_key != -1 && j<left_key)
                    continue;
                if((left_key==-1 || j>=left_key) && (right_key==-1 || j<=right_key))
                    result.push_back(j);
            }
        }
        MbtKey *left, *right;
        if (left_key == -1)
            left = NULL;
        else {
            MbtKey tmp_key = MbtKey::from_int(left_key);
            left = new MbtKey;
            left->raw_key = tmp_key.raw_key;
        }
        if (right_key == -1)
            right = NULL;
        else {
            MbtKey tmp_key = MbtKey::from_int(right_key);
            right = new MbtKey;
            right->raw_key = tmp_key.raw_key;
        }
        //使用左右边界查询对应键值
        bpt->init_search(left, right, false);
        if (result.size() != 0)
            ASSERT_EQ(bpt->iter->current_record->key.to_int(), result[0]);
        for (int i=1; i<result.size(); i++) {
            Record *record = bpt->iter->next();
            ASSERT_EQ(record->key.to_int(), result[i]);
        }
        if (left != NULL)
            delete[] left->raw_key;
        if (right != NULL)
            delete[] right->raw_key;
    } catch (MyException& e) {
        cout << e.what() << endl;
        ASSERT_TRUE(false);
    }
}


TEST(bplus_tree, bplus_tree_search_test) {
    vector<int> empty_keys;
    bplus_tree_search_test(empty_keys, -1, -1);

    int arr1[] = {5,10,30,46,68,77};
    vector<int> keys(arr1, arr1 + sizeof(arr1) / sizeof(int));
    bplus_tree_search_test(keys, -1, -1);
    bplus_tree_search_test(keys, 11, 15);
    bplus_tree_search_test(keys, 112,245);
    bplus_tree_search_test(keys, 0, 4);
    bplus_tree_search_test(keys, 8, 56);
    bplus_tree_search_test(keys, 10, -1);
    bplus_tree_search_test(keys, -1, 50);
}


/**
 * @brief 测试插入功能
 * 通过insert向树中插入值，然后使用左右边界查询对应键值，验证查询结果是否和插入的键值匹配
 * 如果left_key或right_key为-1，则表示不设置左或右边界
 *
 * @param keys 向树中插入的键值
 * @param left_key 用于查询的左边界值
 * @param right_key 用于查询的右边界值
 */
void bplus_tree_insert_test(vector<int> keys, int left_key, int right_key) {
    try {
        remove("./db.mb");
        BplusTree* bpt = create_a_multi_node_tree(keys, INT_TYPE, INT_TYPE*8+STR20_TYPE+INT_TYPE);

        vector<int> result = get_search_results(keys, left_key, right_key);

        MbtKey *left, *right;
        if (left_key == -1)
            left = NULL;
        else {
            MbtKey tmp_key = MbtKey::from_int(left_key);
            left = new MbtKey;
            left->raw_key = tmp_key.raw_key;
        }
        if (right_key == -1)
            right = NULL;
        else {
            MbtKey tmp_key = MbtKey::from_int(right_key);
            right = new MbtKey;
            right->raw_key = tmp_key.raw_key;
        }
        //使用左右边界查询对应键值
        bpt->init_search(left, right, false);
        if (result.size() != 0)
            ASSERT_EQ(bpt->iter->current_record->key.to_int(), result[0]);
        for (int i=1; i<result.size(); i++) {
            Record *record = bpt->iter->next();
            ASSERT_EQ(record->key.to_int(), result[i]);
        }
        if (left != NULL)
            delete[] left->raw_key;
        if (right != NULL)
            delete[] right->raw_key;
    } catch (MyException& e) {
        cout << e.what() << endl;
        ASSERT_TRUE(false);
    }
}


TEST(bplus_tree, bplus_tree_insert_test) {
    vector<int> empty_keys;
    bplus_tree_insert_test(empty_keys, -1, -1);

    int arr1[] = {5,10,30,46,68,77};
    vector<int> keys(arr1, arr1 + sizeof(arr1) / sizeof(int));
    bplus_tree_insert_test(keys, -1, -1);
    bplus_tree_insert_test(keys, 11, 15);
    bplus_tree_insert_test(keys, 112,245);
    bplus_tree_insert_test(keys, 0, 4);
    bplus_tree_insert_test(keys, 8, 56);
    bplus_tree_insert_test(keys, 10, -1);
    bplus_tree_insert_test(keys, -1, 50);
}

/**
 * @brief 测试迭代器功能，测试prev和next
 * @param keys 向树中插入的键值
 * @param left_key 用于查询的左边界值
 * @param right_key 用于查询的右边界值
 */
void bplus_tree_iterator_prev_next_test(vector<int> keys, int left_key, int right_key) {
    try {
        remove("./db.mb");
        BplusTree* bpt = create_a_multi_node_tree(keys, INT_TYPE, INT_TYPE*8*8+INT_TYPE*8+STR20_TYPE);
        vector<int> result = get_search_results(keys, left_key, right_key);
        MbtKey *left, *right;
        if (left_key == -1)
            left = NULL;
        else {
            MbtKey tmp_key = MbtKey::from_int(left_key);
            left = new MbtKey;
            left->raw_key = tmp_key.raw_key;
        }
        if (right_key == -1)
            right = NULL;
        else {
            MbtKey tmp_key = MbtKey::from_int(right_key);
            right = new MbtKey;
            right->raw_key = tmp_key.raw_key;
        }
        //使用左右边界查询对应键值
        bpt->init_search(left, right, false);
        Record *null_record = bpt->iter->prev();
        ASSERT_EQ(null_record, (Record*) NULL);
        bpt->iter->next();
        if (keys.size() == 0)
            ASSERT_EQ(bpt->iter->state, EMPTY_TREE);
        else if (result.size() == 0)
            ASSERT_EQ(bpt->iter->state, EMPTY_ITER);
        else {
            ASSERT_EQ(bpt->iter->current_record->key.to_int(), result[0]);
            for (int i=1; i<result.size(); i++) {
                Record *record = bpt->iter->next();
                ASSERT_EQ(record->key.to_int(), result[i]);
            }
            ASSERT_TRUE(bpt->iter->next() == NULL);
            ASSERT_EQ(bpt->iter->state, AFTER_LAST);
            for (int i=result.size()-1; i>=0; i--) {
                Record *record = bpt->iter->prev();
                ASSERT_EQ(record->key.to_int(), result[i]);
            }
            ASSERT_TRUE(bpt->iter->prev() == NULL);
            ASSERT_EQ(bpt->iter->state, BEFORE_FIRST);
        }
        if (left != NULL)
            delete[] left->raw_key;
        if (right != NULL)
            delete[] right->raw_key;
    } catch (MyException& e) {
        cout << e.what() << endl;
        ASSERT_TRUE(false);
    }
}


TEST(bplus_tree, bplus_tree_iterator_prev_next_test) {
    vector<int> empty_keys;
    bplus_tree_iterator_prev_next_test(empty_keys, -1, -1);

    int arr1[] = {5,10,30,46,68,77};
    vector<int> keys(arr1, arr1 + sizeof(arr1) / sizeof(int));
    bplus_tree_iterator_prev_next_test(keys, -1, -1);
    bplus_tree_iterator_prev_next_test(keys, 11, 15);
    bplus_tree_iterator_prev_next_test(keys, 112, 245);
    bplus_tree_iterator_prev_next_test(keys, 0, 4);
    bplus_tree_iterator_prev_next_test(keys, 8, 56);
    bplus_tree_iterator_prev_next_test(keys, 10, -1);
    bplus_tree_iterator_prev_next_test(keys, -1, 50);
}

/**
 * @brief 测试迭代器功能，构造后是否在第一个位置
 * @param keys 向树中插入的键值
 * @param left_key 用于查询的左边界值
 * @param right_key 用于查询的右边界值
 */
void bplus_tree_iterator_first_test(vector<int> keys, int left_key, int right_key) {
    try {
        remove("./db.mb");
        BplusTree* bpt = create_a_multi_node_tree(keys, INT_TYPE, INT_TYPE*8*8*8+STR20_TYPE*8*8+STR20_TYPE*8+INT_TYPE);
        vector<int> result = get_search_results(keys, left_key, right_key);

        MbtKey *left, *right;
        if (left_key == -1)
            left = NULL;
        else {
            MbtKey tmp_key = MbtKey::from_int(left_key);
            left = new MbtKey;
            left->raw_key = tmp_key.raw_key;
        }
        if (right_key == -1)
            right = NULL;
        else {
            MbtKey tmp_key = MbtKey::from_int(right_key);
            right = new MbtKey;
            right->raw_key = tmp_key.raw_key;
        }
        //使用左右边界查询对应键值
        bpt->init_search(left, right, false);
        if (result.size() != 0) {
            int first = bpt->iter->current_record->key.to_int();
            ASSERT_EQ(first, result[0]);
            if (left == NULL) {
                //如果left==NULL，迭代器应当在第一个位置
                ASSERT_EQ(bpt->iter->current_record_pos, 0);
            } else {
                //如果存在，此位置记录应当大于等于left
                ASSERT_GE(bpt->iter->current_record->key.to_int(), left_key);
            }
        } else {
            if (bpt->iter->state == EMPTY_TREE) {
                //如果是空树
                ASSERT_EQ(bpt->iter->current_record, (Record*) NULL);
                ASSERT_EQ(bpt->iter->current_record_pos, -1);
                ASSERT_EQ(bpt->iter->current_node, (TreeNode*) NULL);
            }
            //如果没有查询到记录
            else if(bpt->iter->current_record == NULL) {
                //如果current_record不存在，说明此刻在第一条纪录之前
                ASSERT_GT(left->to_int(), keys[keys.size()-1]);
            } else {
                //如果current_record存在，迭代器所在位置的前一个应当小于left，当前应当大于left
                ASSERT_GT(bpt->iter->current_record->key.to_int(), left_key);
            }
        }
        if (left != NULL)
            delete[] left->raw_key;
        if (right != NULL)
            delete[] right->raw_key;
    } catch (MyException& e) {
        cout << e.what() << endl;
        ASSERT_TRUE(false);
    }
}

TEST(bplus_tree, bplus_tree_iterator_first_test) {
    vector<int> empty_keys;
    bplus_tree_iterator_first_test(empty_keys, -1, -1);

    int arr1[] = {5,10,30,46,68,77};
    vector<int> keys(arr1, arr1 + sizeof(arr1) / sizeof(int));
    bplus_tree_iterator_first_test(keys, -1, -1);
    bplus_tree_iterator_first_test(keys, 11, 15);
    bplus_tree_iterator_first_test(keys, 112, 245);
    bplus_tree_iterator_first_test(keys, 0, 4);
    bplus_tree_iterator_first_test(keys, 8, 56);
    bplus_tree_iterator_first_test(keys, 10, -1);
    bplus_tree_iterator_first_test(keys, -1, 50);
}

/**
 * @brief 测试查找边界值的功能
 * @param keys 向树中插入的键值
 * @param left_key 用于查询的左边界值
 * @param right_key 用于查询的右边界值
 */
void bplus_tree_before_first_first_last_after_last_test(vector<int> keys, int left_key, int right_key) {
    try {
        remove("./db.mb");
        BplusTree* bpt = create_a_tree(keys);
        vector<int> all_keys, lefts, rights;
        for(int i=0; i<keys.size()/2; i++) {
            for(int j=keys[i*2]; j<=keys[i*2+1]; j++) {
                all_keys.push_back(j);
            }
        }
        if (all_keys.size() == 0) {
            lefts.push_back(-1);
            lefts.push_back(-1);
            rights.push_back(-1);
            rights.push_back(-1);
        } else {
            if (left_key == -1) {
                lefts.push_back(-1);
                lefts.push_back(all_keys[0]);
            } else {
                int left_pos = lower_bound(all_keys.begin(), all_keys.end(), left_key) - all_keys.begin();
                if (left_pos == all_keys.size()) {
                    //#为left_pos
                    //12345
                    //     ---
                    //    *#
                    lefts.push_back(all_keys[left_pos - 1]);
                    lefts.push_back(-1);
                } else if (left_pos == 0) {
                    //  34567
                    //--
                    // *#
                    lefts.push_back(-1);
                    lefts.push_back(all_keys[0]);
                } else {
                    //12345
                    // ---
                    //*#
                    lefts.push_back(all_keys[left_pos - 1]);
                    lefts.push_back(all_keys[left_pos]);
                }
            }
            if (right_key == -1) {
                rights.push_back(all_keys[all_keys.size() - 1]);
                rights.push_back(-1);
            } else {
                int right_pos = upper_bound(all_keys.begin(), all_keys.end(), right_key) - all_keys.begin();
                if (right_pos == all_keys.size()) {
                    //12345
                    //     ---
                    //    *#
                    rights.push_back(all_keys[right_pos - 1]);
                    rights.push_back(-1);
                } else if (right_pos == 0) {
                    //  34567
                    //--
                    // *#
                    rights.push_back(-1);
                    rights.push_back(all_keys[0]);
                } else {
                    //12345
                    // ---
                    //   *#
                    rights.push_back(all_keys[right_pos - 1]);
                    rights.push_back(all_keys[right_pos]);
                }
            }
        }

        MbtKey *left, *right;
        if (left_key == -1)
            left = NULL;
        else {
            MbtKey tmp_key = MbtKey::from_int(left_key);
            left = new MbtKey;
            left->raw_key = tmp_key.raw_key;
        }
        if (right_key == -1)
            right = NULL;
        else {
            MbtKey tmp_key = MbtKey::from_int(right_key);
            right = new MbtKey;
            right->raw_key = tmp_key.raw_key;
        }
        //使用左右边界查询对应键值
        bpt->init_search(left, right, false);
        int* before_first_and_first = bpt->iter->get_before_first_and_first_record();
        int* last_and_after_last = bpt->iter->get_last_and_after_last_record();
        if(lefts[0] == -1) {
            //before_first不存在
            ASSERT_EQ(before_first_and_first[0], -1);
        } else {
            TreeNode* node = bpt->map(before_first_and_first[0]);
            Record& record = node->get_child(before_first_and_first[1]);
            ASSERT_EQ(lefts[0], record.key.to_int());
        }
        if(lefts[1] == -1) {
            //first不存在
            ASSERT_EQ(before_first_and_first[2], -1);
        } else {
            TreeNode* node = bpt->map(before_first_and_first[2]);
            Record& record = node->get_child(before_first_and_first[3]);
            ASSERT_EQ(lefts[1], record.key.to_int());
        }
        if(rights[0] == -1) {
            //last不存在
            ASSERT_EQ(last_and_after_last[0], -1);
        } else {
            TreeNode* node = bpt->map(last_and_after_last[0]);
            Record& record = node->get_child(last_and_after_last[1]);
            ASSERT_EQ(rights[0], record.key.to_int());
        }
        if(rights[1] == -1) {
            //after last不存在
            ASSERT_EQ(last_and_after_last[2], -1);
        } else {
            TreeNode* node = bpt->map(last_and_after_last[2]);
            Record& record = node->get_child(last_and_after_last[3]);
            ASSERT_EQ(rights[1], record.key.to_int());
        }

        if (left != NULL)
            delete[] left->raw_key;
        if (right != NULL)
            delete[] right->raw_key;
    } catch (MyException& e) {
        cout << e.what() << endl;
        ASSERT_TRUE(false);
    }
}


TEST(bplus_tree, bplus_tree_before_first_first_last_after_last_test) {
    vector<int> empty_keys;
    bplus_tree_before_first_first_last_after_last_test(empty_keys, -1, -1);
    int arr1[] = {5,10,30,46,68,77};
    vector<int> keys(arr1, arr1 + sizeof(arr1) / sizeof(int));
    bplus_tree_before_first_first_last_after_last_test(keys, -1, -1);
    bplus_tree_before_first_first_last_after_last_test(keys, 11, 15);
    bplus_tree_before_first_first_last_after_last_test(keys, 112, 245);
    bplus_tree_before_first_first_last_after_last_test(keys, 0, 4);
    bplus_tree_before_first_first_last_after_last_test(keys, 8, 56);
    bplus_tree_before_first_first_last_after_last_test(keys, 10, -1);
    bplus_tree_before_first_first_last_after_last_test(keys, -1, 50);
}

void load_exit_tree_and_search_test(std::vector<int> keys, MbtKey* left, MbtKey* right) {
    BplusTree* bpt = create_a_tree(keys);
    ASSERT_EQ(bpt->metadata->inited, META_ID);
    ASSERT_NE(bpt->metadata->root_node_pos, NOT_EXIST_NODE_POS);
    delete bpt;

    bpt = new BplusTree("./db.mb", INT_TYPE, INT_TYPE*8+INT_TYPE, false);
    ASSERT_EQ(bpt->metadata->inited, META_ID);
    ASSERT_NE(bpt->metadata->root_node_pos, NOT_EXIST_NODE_POS);
}

TEST(bplus_tree, load_exit_tree_and_search_test) {
    try {
        int arr1[] = {5,10,30,46,68,77};
        vector<int> keys(arr1, arr1 + sizeof(arr1) / sizeof(int));
        MbtKey left = MbtKey::from_int(6);
        MbtKey right = MbtKey::from_int(8);
        load_exit_tree_and_search_test(keys, &left, &right);
    } catch (MyException& e) {
        cout << e.what() << endl;
        ASSERT_TRUE(false);
    }
}

//TEST(bplus_tree, load_and_query) {
//    try{
//        BplusTree* bpt = new BplusTree("/home/haojk/mbstore_data/mbt_db_files/user-1689663087.mb",
//                                       INT_TYPE, INT_TYPE*8*8+INT_TYPE*8+STR20_TYPE, false);
//        bpt->init_search(NULL, NULL, false);
//        bpt->iter->prev();
//        while(bpt->iter->next()) {
//            std::cout << bpt->iter->current_record->key.to_int() << std::endl;
//            std::cout << bpt->iter->current_record->raw_record+4+4 << std::endl;
//        }
//    } catch (MyException& e) {
//        cout << e.what() << endl;
//        ASSERT_TRUE(false);
//    }
//}

int main(int argc, char **argv) {
    printf("Running main() from %s\n", __FILE__);
    testing::InitGoogleTest(&argc, argv);
    DBUG_PUSH("d:t:O");
    DBUG_PROCESS("bpt_tree-t");
//    testing::FLAGS_gtest_filter = "bplus_tree.bplus_tree_insert_test";
    return RUN_ALL_TESTS();
}