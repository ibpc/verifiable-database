import sys

import pymysql
import random
import string
import time
import functools
import inspect


def line():
    return inspect.currentframe().f_back.f_lineno

def log(func):
    @functools.wraps(func)
    def wrapper(*args, **kw):
        print('start executing %s()' % func.__name__)
        s = time.perf_counter()
        res = func(*args, **kw)
        elapsed = time.perf_counter() - s
        print('%s() executed for %fs' % (func.__name__, elapsed))
        return res
    return wrapper


def random_string(len):
    letters = string.ascii_letters
    return ''.join([random.choice(letters) for i in range(random.randint(5, len))])


@log
def generate_data(data_len: int):
    s = set()
    data = []
    for i in range(data_len):
        s.add(i)
        age = i * 123
        # age = random.randint(0, 100)
        # name = random_string(20)
        data.append((i, age))
    return data


@log
def connect():
    return pymysql.connect(
        host='localhost',
        port=3306,
        user='root',
        password='111111',
    )

@log
def create_database(con: pymysql.Connection):
    cur = con.cursor()
    cur.execute("DROP DATABASE IF EXISTS test")
    cur.execute("CREATE DATABASE test")
    cur.close()

@log
def create_table(con: pymysql.Connection):
    cur = con.cursor()
    cur.execute('use test')
    cur.execute('drop table if exists user')
    cur.execute(
        '''create table user (
            id int not null,
            age int not null,
            # num int not null,
            name varchar(20) not null,
            primary key (id)
        ) engine=mbtree''')
    # cur.execute("DESCRIBE user")
    table_info = cur.fetchall()
    print(table_info)
    cur.close()

@log
def install_plugin(con: pymysql.Connection):
    cur = con.cursor()
    try:
        cur.execute('uninstall plugin mbtree')
    except Exception as e:
        print("uninstall plugin error: ", e)
    cur.execute('install plugin mbtree soname "ha_mbtree.so"')
    cur.close()



@log
def insert_by_range(con: pymysql.Connection, data: list, start: int, end: int):
    cur = con.cursor()
    cur.execute('use test')
    try:
        for u in data:
            if u[0] < start or u[0] > end:
                continue
            # cur.execute('insert into user values (%s, %s)' % u)
            cur.execute('insert into user values (%s, %s, \'abcdefg\')' % u)
            # cur.execute('insert into user values (%s, %s, 1234, \'abcdefg\')' % u)
            assert 1 == cur.rowcount, 'insert error'
    except Exception as e:
        print('insert error: ', e)
    finally:
        cur.close()

@log
def query_by_range(con: pymysql.Connection, start: int, end: int):
    cur = con.cursor()
    cur.execute('use test')
    try:
        cur.execute('select * from user where id >= %d and id <= %d' % (start, end))
        rows = cur.fetchall()
        rows = [i for i in rows]
        rows.sort()
        print(f"query rows: {rows}")
        print("query rows len: %d" % len(rows))
    except Exception as e:
        print('query error: ', e)
    finally:
        cur.close()




if __name__ == "__main__":
    con = connect()
    data_len = 1000

    args = sys.argv
    if len(args) < 2:
        print("Usage: python test_mysql_client.py <action> <start> <end>")
        exit(1)
    action = args[1]
    if action == "create":
        install_plugin(con)
        create_database(con)
        create_table(con)
    elif action == "insert":
        if len(args) != 4:
            print(args)
            print("Usage: python test_mysql_client.py <action> <start> <end>")
            exit(1)
        start = int(args[2])
        end = int(args[3])
        data = generate_data(data_len)
        insert_by_range(con, data, start, end)
    elif action == "search":
        if len(args) != 4:
            print(args)
            print("Usage: python test_mysql_client.py <action> <start> <end>")
            exit(1)
        start = int(args[2])
        end = int(args[3])
        query_by_range(con, start, end)
    else:
        print(args)
        print("Usage: python test_mysql_client.py <action> <start> <end>")
        exit(1)
    con.close()