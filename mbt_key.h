/*
  Copyright (C) BABEC. All rights reserved.
  Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
  and Privacy Computing. All rights reserved.
  
  SPDX-License-Identifier: Apache-2.0
*/

#ifndef MBT_KEY_H
#define MBT_KEY_H

#include "predefined.h"

class MbtKeyException: public MyException {
public:
    std::string err_msg;
    MbtKeyException(std::string err_msg) {
        this->err_msg = "[mbt_key] error: " + err_msg;
    }
    virtual const char* what() const throw() {
        return this->err_msg.c_str();
    }
};


/**
 * @brief this class is a key for comparation and search, with method for comparation and search
 * @method
 *  1. methods of less , great and equal 
 *  2. method of tranlating key into corresponding type
 *
 * @note MbtKey raw_key should use delete to free
 */
class MbtKey {
public:

    /**
     * @brief construction function
     */
    MbtKey() {
        raw_key = NULL;
    }

    /**
     * @brief comparation method
     * @param key key for compare
     * @param typ compare key type
     * @return key1<key2 returns -1，> returns 1，= returns0
     */
    int cmp(const MbtKey& key, int typ) const;
    bool lt (const MbtKey& key, int typ) const;
    bool lte (const MbtKey& key, int typ) const;
    bool gt (const MbtKey& key, int typ) const;
    bool gte (const MbtKey& key, int typ) const;
    bool eq (const MbtKey& key, int typ) const;
    bool neq (const MbtKey& key, int typ) const;

    /**
     * @brief interpret key as int
     * @return key interpreted as int
     */
    int to_int() const ;

    /**
     * @brief from int construct key
     * @param x index key
     * @return constructed key
     */
    static MbtKey from_int(int x);

    /**
     * @brief deep copy
     * @param key target 
     * @param typ key type
     */
    void copy(MbtKey& key, int typ);

    uchar* raw_key;
};



#endif //MBT_KEY_H
