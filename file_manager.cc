/*
  Copyright (C) BABEC. All rights reserved.
  Copyright (C) Beijing Advanced Innovation Center for Future Blockchain
  and Privacy Computing. All rights reserved.
  
  SPDX-License-Identifier: Apache-2.0
*/
#include "file_manager.h"

void FilePagesManager::set_file_pages_manager_root(uchar* file_pages_manager_root) {
    this->file_pages_manager_root = file_pages_manager_root;
}

int FilePagesManager::get_a_page() {
    for (int i=0; i<PAGE_SIZE; i++) {
        if (file_pages_manager_root[i] != 0xff) {
            int j = get_zero_pos_in_byte(file_pages_manager_root[i]);
            int pos = j + i*8;
            unsigned char mask = get_bit_exist_mask_in_pos(j);
            file_pages_manager_root[i] = file_pages_manager_root[i] | mask;
            return pos;
        }
    }
    return -1;
}
bool FilePagesManager::reback_a_page(int pos) {
    int i = pos / 8;
    int j = pos % 8;
    unsigned char mask = get_bit_exist_mask_in_pos(j);
    if (file_pages_manager_root[i] & mask) {
        // if page not returned
        file_pages_manager_root[i] &= !mask;
        return true;
    }
    //empty page
    return false;
}

int FilePagesManager::get_zero_pos_in_byte(uchar x) {
    if ((~x) & 0x80) {
        return 0;
    }
    if ((~x) & 0x40) {
        return 1;
    }
    if ((~x) & 0x20) {
        return 2;
    }
    if ((~x) & 0x10) {
        return 3;
    }
    if ((~x )& 0x8) {
        return 4;
    }
    if ((~x) & 0x4) {
        return 5;
    }
    if ((~x) & 0x2) {
        return 6;
    }
    if ((~x) & 0x1) {
        return 7;
    }
    std::string err_msg = "get_zero_pos_in_byte error";
    throw FileManagerException(err_msg);
}

uchar FilePagesManager::get_bit_exist_mask_in_pos(int x) {
    if (x == 0) {
        return 0x80;
    }
    if (x == 1) {
        return 0x40;
    }
    if (x == 2) {
        return 0x20;
    }
    if (x == 3) {
        return 0x10;
    }
    if (x == 4) {
        return 0x8;
    }
    if (x == 5) {
        return 0x4;
    }
    if (x == 6) {
        return 0x2;
    }
    if (x == 7) {
        return 0x1;
    }
    std::string err_msg = "get_bit_mask error";
    throw FileManagerException(err_msg);
}

bool FilePagesManager::exist_pos(int x) {
    int i = x / 8;
    int j = x % 8;
    int flag = file_pages_manager_root[i] & (1 << (7-j));

    return flag;
}

void FilePagesManager::set_pos(int x, bool b) {
    int i = x / 8;
    int j = x % 8;
    if (b) {
        file_pages_manager_root[i] |= (1 << (7-j));
    } else {
        file_pages_manager_root[i] &= (~(1 << (7-j)));
    }
}

